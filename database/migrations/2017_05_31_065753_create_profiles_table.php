<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->text('status')->nullable();
            $table->date('date_of_birth');
            $table->enum('gender',array('M','F','Unspecified'))->default('Unspecified');
            $table->string('work_place_name')->nullable();
            $table->string('position_at_work')->nullable();
            // $table->date('start_date')->nullable();
            $table->text('about_work')->nullable();
            $table->integer('user_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
