<?php

use Illuminate\Database\Seeder;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('social_media')->insert([
            'url' => 'facebook.com',
            'media' => 'facebook',
            'restaurant_id' => 1,
        ]);

         DB::table('social_media')->insert([
            'url' => 'twitter.com',
            'media' => 'twitter',
            'restaurant_id' => 1,
        ]);

         DB::table('social_media')->insert([
            'url' => 'whatsapp.com',
            'media' => 'whatsapp',
            'restaurant_id' => 1,
        ]);

         DB::table('social_media')->insert([
            'url' => 'facebook.com',
            'media' => 'google+',
            'restaurant_id' => 1,
        ]);

         DB::table('social_media')->insert([
            'url' => 'facebook.com',
            'media' => 'Other',
            'restaurant_id' => 1,
        ]);
    }
}
