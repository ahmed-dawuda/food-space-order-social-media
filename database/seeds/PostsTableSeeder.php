<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('posts')->insert([
            'title' =>'free to organize your',
            'body' => 'The default Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 1,
        ]);

         DB::table('posts')->insert([
            'title' =>'To forgive is divine',
            'body' => 'The default Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 1,
        ]);

         DB::table('posts')->insert([
            'title' =>'large and small applications',
            'body' => 'The default Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 1,
        ]);

         DB::table('posts')->insert([
            'title' =>'Of course, you are free',
            'body' => 'starting point for both large and small applications.  to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 3,
        ]);

         DB::table('posts')->insert([
            'title' =>'almost no',
            'body' => 'The default Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 3,
        ]);
         DB::table('posts')->insert([
            'title' =>'long as Composer can autoload',
            'body' => ' great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as  the class.',
            'user_id' => 3,
        ]);

         DB::table('posts')->insert([
            'title' =>'no restrictions on where any',
            'body' => 'The default Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost  given class is located - as long as Composer can autoload the class.',
            'user_id' => 4,
        ]);

         DB::table('posts')->insert([
            'title' =>'The default Laravel application structure',
            'body' => ' is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 4,
        ]);

         DB::table('posts')->insert([
            'title' =>'Android application',
            'body' => 'application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 5,
        ]);

         DB::table('posts')->insert([
            'title' =>'however you are, just becarefull',
            'body' => 'however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 5,
        ]);

         DB::table('posts')->insert([
            'title' =>'How to start it',
            'body' => 'starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 5,
        ]);

         DB::table('posts')->insert([
            'title' =>'Laravel as your breakfast',
            'body' => 'Laravel application structure is intended to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 5,
        ]);

         DB::table('posts')->insert([
            'title' =>'The frontier',
            'body' => 'to provide a great starting point for both large and small applications. Of course, you are free to organize your application however you like. Laravel imposes almost no restrictions on where any given class is located - as long as Composer can autoload the class.',
            'user_id' => 6,
        ]);
    }
}
