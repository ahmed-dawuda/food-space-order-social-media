<?php

use Illuminate\Database\Seeder;

class ChefsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('chefs')->insert([
            'full_name' => 'Ahmed Dawuda',
            'role' => 'Chief Chef',
            // 'about' => 'The cache configuration file also contains various other options, which are documented within the file, so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized, cached objects in the filesystem. For larger applications',
            'restaurant_id'=>1,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Kofi Operon',
            'role' => 'Asst. Chef',
            // 'about' => 'so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized, cached objects in the filesystem. For larger applications',
            'restaurant_id'=>1,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Araba Coder',
            'role' => 'Newest Chef',
            // 'about' => 'various other options, which are documented within the file, so make sure to read over these options. By default, Laravel is configured to ',
            'restaurant_id'=>1,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Ikeda Cripps',
            'role' => 'Normal Chef',
            // 'about' => 'documented within the file, so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized',
            'restaurant_id'=>1,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Izzy Arius',
            'role' => 'Chief Chef',
            // 'about' => 'The cache configuration file also contains various other options, which are documented within the file, so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized, cached objects in the filesystem. For larger applications',
            'restaurant_id'=>2,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Selasi Micheal',
            'role' => 'Chief Chef',
            // 'about' => 'file, so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized, cached objects in the filesystem. For larger applications',
            'restaurant_id'=>2,
        ]);

         DB::table('chefs')->insert([
            'full_name' => 'Nudoji Hillary',
            'role' => 'Chief Chef',
            // 'about' => 'The cache configuration file also contains various other options, which are documented within the file, so make sure to read over these options. By default, Laravel is configured to use the file cache driver, which stores the serialized, cached objects in the filesystem. For larger applications',
            'restaurant_id'=>3,
        ]);


    }
}
