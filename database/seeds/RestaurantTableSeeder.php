<?php

use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('restaurants')->insert([
            'name' => 'akuzi',
            'home_message' => 'home of quality foods for you and friends',
            'about_us' => 'Laravel already makes it easy to perform authentication via traditional login forms, but what about_us APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport',
            // 'minimun_order_time'=>'1 hour 25 mins',
            'user_id'=>2,
        ]);
         DB::table('restaurants')->insert([
            'name' => 'Brunei Cafetaria',
            'home_message' => 'Welcome To the Best Meal Service Providers In Kumasi',
            'about_us' => 'Laravel already makes it easy to perform authentication via traditional login forms, but what about_us APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport',
            // 'minimun_order_time'=>'1 hour',
            'user_id'=>4,
        ]);
         DB::table('restaurants')->insert([
            'name' => 'akuzi',
            'home_message' => 'Call Us, We make the delivery',
            'about_us' => 'Laravel already makes it easy to perform authentication via traditional login forms, but what about_us APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport',
            // 'minimun_order_time'=>'30 mins',
            'user_id'=>6,
        ]);
         DB::table('restaurants')->insert([
            'name' => 'akuzi',
            'home_message' => 'Hello Dear, you are welcome to Maa U restaurant',
            'about_us' => 'Laravel already makes it easy to perform authentication via traditional login forms, but what about_us APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport',
            // 'minimun_order_time'=>'30 mins',
            'user_id'=>7,
        ]);
         DB::table('restaurants')->insert([
            'name' => 'Fried Rice Delivery',
            'home_message' => 'home of quality foods for you and friends',
            'about_us' => 'Laravel already makes it easy to perform authentication via traditional login forms, but what about_us APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport',
            // 'minimun_order_time'=>'25 mins',
            'user_id'=>10,
        ]);
    }
}
