<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//user 1
        DB::table('users')->insert([
            'phone' => '0547406820',
            'username' => 'Kofi',
            'password' => bcrypt('12345'),
            'admin'=>true,
            'assit_admin'=>false,
            'profile_updated'=>true,
        ]);
//user 2
        DB::table('users')->insert([
            'phone' => '0547406821',
            'username' => 'Kojo',
            'password' => bcrypt('12345'),
            'admin'=>false,
            'assit_admin'=>false,
            'profile_updated'=>true,
        ]);
// //user 3
//         DB::table('users')->insert([
//             'phone' => '0547406822',
//             'username' => 'Ama',
//             'password' => bcrypt('12345'),
//             'admin'=>false,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //user 4
//         DB::table('users')->insert([
//             'phone' => '0547406823',
//             'username' => 'Kwame',
//             'password' => bcrypt('12345'),
//             'admin'=>true,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //user 5
//         DB::table('users')->insert([
//             'phone' => '0547406824',
//             'username' => 'Joe',
//             'password' => bcrypt('12345'),
//             'admin'=>false,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //user 6
//         DB::table('users')->insert([
//             'phone' => '0547406825',
//             'username' => 'John',
//             'password' => bcrypt('12345'),
//             'admin'=>true,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);

// //has a restaurant id = 7
//         DB::table('users')->insert([
//             'phone' => '0547406826',
//             'username' => 'Jonny',
//             'password' => bcrypt('12345'),
//             'admin'=>true,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //user 8
//         DB::table('users')->insert([
//             'phone' => '0547406827',
//             'username' => 'Mansah',
//             'password' => bcrypt('12345'),
//             'admin'=>false,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //user 9
//         DB::table('users')->insert([
//             'phone' => '0547406828',
//             'username' => 'Messi',
//             'password' => bcrypt('12345'),
//             'admin'=>false,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
// //has a restaurant id = 10
//         DB::table('users')->insert([
//             'phone' => '0547406829',
//             'username' => 'Neymar',
//             'password' => bcrypt('12345'),
//             'admin'=>true,
//             'assit_admin'=>false,
//             'profile_updated'=>true,
//         ]);
    }
}
