<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            'country'=>'ghana',
            'city'=>'tema',
            'state'=>'Accra',
            'Street'=>'Com. 6',
            'zip_code'=>'00233',
            'user_id'=>1,
            
        ]);

        // DB::table('locations')->insert([
        //     'country'=>'USA',
        //     'city'=>'Alobam',
        //     'state'=>'Texas',
        //     'Street'=>'line. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>2,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'ghana',
        //     'city'=>'tema',
        //     'state'=>'Madrid',
        //     'Street'=>'Com. 56',
        //     'zip_code'=>'00233',
        //     'user_id'=>3,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Spain',
        //     'city'=>'Barcelona',
        //     'state'=>'Catalan',
        //     'Street'=>'MLasf',
        //     'zip_code'=>'00233',
        //     'user_id'=>4,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Mali',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>5,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Benin',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>6,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Malaysia',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>7,
    
        // ]);
        // DB::table('locations')->insert([
        //     'country'=>'China',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>8,
            
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Senegal',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>9,
           
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Brazil',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'user_id'=>10,
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Brazil',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'restaurant_id'=>1,
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Brazil',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'restaurant_id'=>2,
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Germany',
        //     'city'=>'Bayern',
        //     'state'=>'Munich',
        //     'Street'=>'Mun. 6',
        //     'zip_code'=>'00333',
        //     'restaurant_id'=>3,
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Netherlands',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'restaurant_id'=>4,
        // ]);

        // DB::table('locations')->insert([
        //     'country'=>'Brazil',
        //     'city'=>'tema',
        //     'state'=>'Accra',
        //     'Street'=>'Com. 6',
        //     'zip_code'=>'00233',
        //     'restaurant_id'=>5,
        // ]);
    }
}
