<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('messages')->insert([
            'user_id' => 1,
            'reciever_id' => 2,
            'body'=>'Please reply me',
            'chat_id'=>1,
            'created_at'=> '2017-07-07 08:38:57',
        ]);

        

        DB::table('messages')->insert([
            'user_id' => 1,
            'reciever_id' => 2,
            'body'=>'Sorry... I thought you were Ahmed',
            'chat_id'=>1,
            'created_at'=> '2017-07-07 09:36:57',
        ]);

        DB::table('messages')->insert([
            'user_id' => 1,
            'reciever_id' => 2,
            'body'=>'Hello, How are you?',
            'chat_id'=>1,
            'created_at'=> '2017-07-07 08:36:57',
        ]);

        DB::table('messages')->insert([
            'user_id' => 2,
            'reciever_id' => 1,
            'body'=>'Ok',
            'chat_id'=>2,
            'created_at'=> '2017-07-07 09:56:57',
        ]);

        DB::table('messages')->insert([
            'user_id' => 2,
            'reciever_id' => 1,
            'body'=>'What do you want from me?',
            'chat_id'=>2,
            'created_at'=> '2017-07-07 09:16:57',
        ]);

        DB::table('messages')->insert([
            'user_id' => 1,
            'reciever_id' => 2,
            'body'=>'test message',
            'chat_id'=>1,
            'created_at'=> '2017-07-07 09:16:57',
        ]);
    }
}
