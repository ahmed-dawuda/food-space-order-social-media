<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('orders')->insert([
            'user_id' => 1,
            'food_id'=>1,
            'orderID' => '78970',
            'restaurant_id'=> 1,
            // 'order_time'=>'21:00',
            'delivery_time'=>'2.45',
            'quantity'=>3,
            'price'=> 12,
            'status'=>'pending',
            'destination'=>'Tema Comm. 4 hse 34/hn',
            'created_at'=>'2017-07-07 08:38:57',
        ]);

         DB::table('orders')->insert([
            'user_id' => 1,
            'food_id'=>1,
            'orderID' => '56770',
            'restaurant_id'=> 1,
            // 'order_time'=>'21:00',
            'delivery_time'=>'4',
            'quantity'=>34,
            'price'=> 12,
            'status'=>'Delivered',
            'destination'=>'Tema Comm. 4 hse 34/hn',
            'created_at'=>'2017-04-07 08:38:57',
        ]);

         DB::table('orders')->insert([
            'user_id' => 1,
            'food_id'=>3,
            'orderID' => '98970',
            'restaurant_id'=> 1,
            // 'order_time'=>'21:00',
            'delivery_time'=>'2',
            'quantity'=>34,
            'price'=> 12,
            'status'=>'Rejected',
            'destination'=>'Tema Comm. 4 hse 34/hn',
            'created_at'=>'2017-07-10 12:38:27',
        ]);
    }
}
