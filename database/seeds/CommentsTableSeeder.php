<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'body' => 'jScroll is a jQuery plugin allowing you to effortlessly implement infinite scrolling, lazy loading, or whatever catchy phrase you may know it as, within a web page or web/HTML5 application.',
            'user_id' => 1,
            'post_id' => 1,
        ]);

        DB::table('comments')->insert([
            'body' => 'jScroll is a jQuery plugin allowing you to effortlessly implement infinite scrolling, lazy loading, or whatever catchy phrase you may know it as, within a web page or web/HTML5 application.',
            'user_id' => 1,
            'post_id' => 1,
        ]);

        DB::table('comments')->insert([
            'body' => 'jScroll is a jQuery plugin allowing you to effortlessly implement infinite scrolling, lazy loading, or whatever catchy phrase you may know it as, within a web page or web/HTML5 application.',
            'user_id' => 1,
            'post_id' => 2,
        ]);

        DB::table('comments')->insert([
            'body' => 'jScroll is a jQuery plugin allowing you to effortlessly implement infinite scrolling, lazy loading, or whatever catchy phrase you may know it as, within a web page or web/HTML5 application.',
            'user_id' => 1,
            'post_id' => 2,
        ]);
    }
}
