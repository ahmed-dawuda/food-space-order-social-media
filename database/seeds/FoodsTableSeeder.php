<?php

use Illuminate\Database\Seeder;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('foods')->insert([
            'name' => 'Banku and Tilapia',
            'period' => 'Supper',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>20,
            'available'=>true,
            'pricing' => '3/5.25/12/43',
        ]);

         DB::table('foods')->insert([
            'name' => 'Banku and Tilapia',
            'period' => 'Supper',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>20,
            'pricing'=>'23/12',
            'available'=>true,
        ]);

         DB::table('foods')->insert([
            'name' => 'Banku and Tilapia',
            'period' => 'Supper',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>20,
            'available'=>true,
            'pricing'=>'2/3/4',
        ]);

         DB::table('foods')->insert([
            'name' => 'Tuo Zaafi',
            'period' => 'lunch',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>10,
            'available'=>true,
            'pricing'=>'12',
        ]);

         DB::table('foods')->insert([
            'name' => 'Fufu and light soup',
            'period' => 'lunch',
            // 'about' => 'You and your family wants something spicy? then look  no further, we have the best fufu and light soup for you. Call now',
            'restaurant_id'=>1,
            // 'quantity'=>50,
            'available'=>true,
            'pricing'=>'10',
        ]);

         DB::table('foods')->insert([
            'name' => 'Rice and Stew',
            'period' => 'BreakFast',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>70,
            'available'=>true,
            'pricing'=>'21.50',
        ]);

         DB::table('foods')->insert([
            'name' => 'Banku and Tilapia',
            'period' => 'Supper',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>0,
            'available'=>false,
            'pricing'=>'21.50',
        ]);

         DB::table('foods')->insert([
            'name' => 'Tea and Bread',
            'period' => 'BreakFast',
            // 'about' => 'Come and get it If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>2,
            'available'=>true,
            'pricing'=>'21.50',
        ]);

         DB::table('foods')->insert([
            'name' => 'Chicken',
            'period' => 'BreakFast',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>20,
            'available'=>true,
            'pricing'=>'21.50',
        ]);

         DB::table('foods')->insert([
            'name' => 'Aprepresah and palm nut soup',
            'period' => 'lunch',
            // 'about' => 'If youre a Ghanaian, then this food is the best for you every afternoon',
            'restaurant_id'=>1,
            // 'quantity'=>20,
            'available'=>true,
            'pricing'=>'21.50',
        ]);
    }
}
