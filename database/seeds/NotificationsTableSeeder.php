<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('notifications')->insert([
            'user_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'user_id' => 1,
            'note_type'=>'TAG',
        ]);

         DB::table('notifications')->insert([
            'user_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'user_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'user_id' => 1,
            'note_type'=>'TAG',
        ]);

         DB::table('notifications')->insert([
            'restaurant_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'restaurant_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'restaurant_id' => 1,
            'note_type'=>'ORDER',
        ]);

         DB::table('notifications')->insert([
            'restaurant_id' => 1,
            'note_type'=>'ORDER',
        ]);
    }
}
