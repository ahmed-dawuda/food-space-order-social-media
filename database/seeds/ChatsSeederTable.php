<?php

use Illuminate\Database\Seeder;

class ChatsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chats')->insert([
            'user_1' => 1,
            'user_2' => 2,
            'created_at'=> '2017-07-07 08:38:57',
        ]);

        DB::table('chats')->insert([
            'user_1' => 2,
            'user_2' => 1,
            'created_at'=> '2017-07-07 08:38:56',
        ]);

        // DB::table('chats')->insert([
        //     'user_1' => 1,
        //     'user_2' => 3,
        //     'created_at'=> '2017-07-07 08:38:55',
        // ]);

        // DB::table('chats')->insert([
        //     'user_1' => 3,
        //     'user_2' => 1,
        //     'created_at'=> '2017-07-07 08:38:54',
        // ]);
    }
}
