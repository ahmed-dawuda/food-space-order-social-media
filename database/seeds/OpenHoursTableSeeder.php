<?php

use Illuminate\Database\Seeder;

class OpenHoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('open_hours')->insert([
            'restaurant_id' => 1,
            'start_time' =>'12:20:00' ,
            'close_time' => '20:00:00',
            'day'=>'sun',
        ]);

         DB::table('open_hours')->insert([
            'restaurant_id' => 1,
            'start_time' =>'09:20:00' ,
            'close_time' => '20:00:00',
            'day'=>'sat',
        ]);

         DB::table('open_hours')->insert([
            'restaurant_id' => 2,
            'start_time' =>'09:20:00' ,
            'close_time' => '20:00:00',
            'day'=>'tue',
        ]);

         DB::table('open_hours')->insert([
            'restaurant_id' => 3,
            'start_time' =>'08:00:00' ,
            'close_time' => '20:00:00',
            'day'=>'sat',
        ]);

         DB::table('open_hours')->insert([
            'restaurant_id' => 4,
            'start_time' =>'09:20:00' ,
            'close_time' => '21:00:00',
            'day'=>'Sun',
        ]);
    }
}
