<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // $this->call(UsersTableSeeder::class);
        // $this->call(PostsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        // $this->call(RestaurantTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        // $this->call(ChefsTableSeeder::class);
        // $this->call(FoodsTableSeeder::class);
        // $this->call(OrdersTableSeeder::class);
        // $this->call(PricesTableSeeder::class);
        // $this->call(OpenHoursTableSeeder::class);
        // $this->call(SocialMediaTableSeeder::class);
        // $this->call(ChatsSeederTable::class);
        // $this->call(MessagesTableSeeder::class);
        // $this->call(NotificationsTableSeeder::class);
    }
}
