<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('prices')->insert([
            'food_id' => 1,
            'amount' => 10.00,
        ]);

         DB::table('prices')->insert([
            'food_id' => 1,
            'amount' => 1.00,
        ]);

         DB::table('prices')->insert([
            'food_id' => 2,
            'amount' => 0.90,
        ]);

         DB::table('prices')->insert([
            'food_id' => 3,
            'amount' => 3.44,
        ]);

         DB::table('prices')->insert([
            'food_id' => 4,
            'amount' => 12.12,
        ]);

         DB::table('prices')->insert([
            'food_id' => 4,
            'amount' => 7.25,
        ]);

         DB::table('prices')->insert([
            'food_id' => 4,
            'amount' => 18.50,
        ]);

         DB::table('prices')->insert([
            'food_id' => 5,
            'amount' => 2.29,
        ]);

         DB::table('prices')->insert([
            'food_id' => 6,
            'amount' => 7.00,
        ]);

         DB::table('prices')->insert([
            'food_id' => 7,
            'amount' => 3.25,
        ]);

         DB::table('prices')->insert([
            'food_id' => 8,
            'amount' => 2.00,
        ]);

         DB::table('prices')->insert([
            'food_id' => 9,
            'amount' => 3.12,
        ]);

         DB::table('prices')->insert([
            'food_id' => 10,
            'amount' => 13.12,
        ]);
    }
}
