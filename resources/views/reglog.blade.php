<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Login / Register with US </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="timer/css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="timer/css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="timer/css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="timer/css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="timer/css/owl.carousel.css">
        <link rel="stylesheet" href="timer/css/owl.theme.css">
        <link rel="stylesheet" href="timer/css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="timer/css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="timer/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="zentro/css/font-awesome.min.css">
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="timer/js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="timer/js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="timer/js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="timer/js/wow.min.js"></script>
        <!-- slider js -->
        <script src="timer/js/slider.js"></script>
        <script src="timer/js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="timer/js/main.js"></script>
        <style type="text/css">
            .profile-nav{
                background: white;
                box-shadow: 0px 0px 1px;
                position: sticky;
            }
            .centa{
                text-align: center;
                padding: 10px;
                color: white;
                box-shadow: 5px;
            }

            .green{
                background: #3CB371;
            }

            .black{
                background: #000000;
                color: white;
            }
            .blue{
                background: #578EBE;
                color: white;
            }
           
        </style>
    </head>
    <body>

<div class="container-fluid profile-nav navbar-fixed-top">
    <!-- <div class="navbar"> -->
        <div class="row">
            <div class="col-sm-5"></div>

            <a href="#blog-full-width" title="Go Home">
                <div class="col-sm-1 centa green"><i class="fa fa-home fa-2x"></i></div>
            </a>

             <a href="#about" title="About Us">
                <div class="col-sm-1 centa black"><i class="fa fa-info-circle fa-2x"></i></div>
            </a>

            <div class="col-sm-5"></div>
        </div>
    <!-- </div> -->
</div>


<section id="blog-full-width">


<div class="container" id="home">
    <div class="row" >

<!-- SIDE BAR -->
        <div class="col-md-4" >
        <!-- <div class="affix"> --><br>
            <div class="sidebar">
                
                    <div class="author widget">
                        <!-- <img class="img-responsive" src="timer/images/author/author-bg.jpg"> -->
                        <div class="author-body text-center">
                            <div class="author-img">
                                <!-- <img src="timer/images/author/author.jpg"> -->
                                <img src="{{asset('avartar.png')}}">
                            </div>
                            <div class="author-bio">
                            @if(Session::has('failed'))
                            <p class="alert alert-danger">{{ session('failed')}}</p>
                            @endif
                            @if (count($errors) > 0 && old('login') == 1)
                         
                                    @foreach ($errors->all() as $error)
                                        <p class="alert alert-danger">{{ $error }}<p>
                                    @endforeach
                            
                            @endif
                                <h3>Login</h3>
                                <hr>
                                <!-- <p> -->
                                    <!-- <div class="search widget"> -->
                        <form action="{{ route('user-login') }}" method="POST" class="searchform" role="search">
                        <label>Mobile Number</label>
                            <div class="input-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                                <input type="number" class="form-control" placeholder="Mobile Number" name="mobile" value="{{ old('mobile') }}">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"> <i class="ion-android-call"></i> </button>
                                </span>
                            </div><br>
                            <label>Password</label>
                            <div class="input-group {{ $errors->has('pasword') ? 'has-error' : '' }}">
                                <input type="password" class="form-control" placeholder="{{ $errors->has('pasword') ? 'Enter Password Again' : 'Password' }}" name="pasword">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"> <i class="ion-android-lock"></i> </button>
                                </span>
                            </div><br>
                            <div class="input-group">
                                <label style="text-align: left; margin-right: 10px; color: #578EBE;">Remember me</label>
                                  <input type="checkbox" name="remember" value="1">
                            </div><br>
                            <!-- <div class="input-group"> -->
                            <input type="hidden" name="login" value="1">
                            <br>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-dafault btn-details blue" >Resume</button>
                            <!-- </div> -->

                                        </form>
                                    <!-- </div> -->
                                <!-- </p> -->
                            </div>
                        </div>
                    </div>
                </div>

            <!-- </div> -->
            </div>

<!-- END SIDE BAR -->

<!-- BLOG BODY -->
            <div class="col-md-8">
                <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                    <div class="sidebar">
                    <div class="author widget">
                        <div class="author-body text-center">
                            <div class="author-img">
                                <img src="hand.jpg">
                            </div>
                            <div class="author-bio">
                            @if (count($errors) > 0 && old('register') == 1)
                         
                                    @foreach ($errors->all() as $error)
                                        <p class="alert alert-danger">{{ $error }}<p>
                                    @endforeach
                            
                            @endif
                                <h3>Register And Rent for Free</h3>
                                <hr>
                                <!-- <p> -->
                                    <!-- <div class="search widget"> -->
                <form action="{{ route('user-register') }}" method="POST" class="searchform" role="search">
                <label>Username</label>
                    <div class="input-group {{ $errors->has('username') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="ion-android-contact"></i> </button>
                        </span>
                    </div><br>
                    <label>Mobile Number</label>
                    <div class="input-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <input type="number" class="form-control" placeholder="Mobile Number" name="phone" value="{{old('phone')}}">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="ion-android-call"></i> </button>
                        </span>
                    </div><br>
                    <label>Password</label>
                    <div class="input-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="ion-android-lock"></i> </button>
                        </span>
                    </div>
                    <br>
                    <label>Password Confirmation</label>
                    <div class="input-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Confirm your password" name="password_confirmation">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="ion-android-lock"></i> </button>
                        </span>
                    </div>
                    <br>
                    <input type="hidden" name="register" value="1">
                    <div class="input-group">
                        <label style="text-align: left; margin-right: 10px; color: #578EBE;">Create Restaurant</label>
                         <input type="checkbox" name="restaurant" value="1"> | 
                         <label style="text-align: left; margin-left: 10px; margin-right: 10px; color: #578EBE;">Remember me</label>
                         <input type="checkbox" name="remember" value="1">
                    </div><br>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-dafault btn-details blue" >Register</button>
                </form>
                                  
                            </div>
                        </div>
                    </div>
                </div>
                   
                </article>
                <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                    <div class="blog-post-image">
                        <a href="#about"><img class="img-responsive" src="connect.png" alt="" /></a>
                    </div>
                    <div class="blog-content">
                        <h2 class="blogpost-title">
                        <a id="about">STAY CONNECTED WITH YOUR FOODS AND FRIENDS</a>
                        </h2>
                        <div class="blog-meta">
                            <span>Dec 11, 2020</span>
                            <span> <i class="fa fa-user"></i> <a href="">Admin</a></span>
                            <span><a href="">business</a>,<a href="">people</a></span>
                        </div>
                        <p>Ultrices posuere cubilia curae curabitur sit amet tortor ut massa commodo. Vestibulum consectetur euismod malesuada tincidunt cum. Sed ullamcorper dignissim consectetur ut tincidunt eros sed sapien consectetur dictum. Pellentesques sed volutpat ante, cursus port. Praesent mi magna, penatibus et magniseget dis parturient montes sed quia consequuntur magni dolores eos qui ratione.
                        </p>
                    </div>
                </article>
            </div>
        </div>
        

    </section>

    <footer id="footer">
        <div class="container">
            <div class="col-md-8">
            </div>
            <div class="col-md-4">
                <!-- Social Media -->
                <ul class="social">
                    <li>
                        <a href="#" class="Facebook">
                            <i class="ion-social-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Twitter">
                            <i class="ion-social-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Linkedin">
                            <i class="ion-social-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Google Plus">
                            <i class="ion-social-googleplus"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer> <!-- /#footer -->
                </body>
            </html>
        </html>