@extends('layouts.dashboard')
@section('content')
	
                <div class="row" id="orders">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">DAILY ORDERS <span class="ti-angle-right"></span> {{explode(' ',Carbon\Carbon::now())[0]}}</h4>
                                <p class="category">Here are the orders within today. Sort By<i class="fa fa-sort"></i> 
                                    <select style="padding: 3px;" id="sort">
                                        <option value="{{ route('sort-order',['orderID']) }}">Order ID</option>
                                        <option value="{{ route('sort-order',['status']) }}">Status</option>
                                        <option value="{{ route('sort-order',['delivery_time']) }}">Delivery Time</option>
                                        <option value="{{ route('sort-order',['price']) }}">Price Asc</option>
                                        <option value="{{ route('sort-order',['quantity']) }}">Quantity</option>
                                    </select> |
                                    Show only 
                                    <select id="show_only" style="padding: 3px;">
                                        <option value="{{ route('view-group',['all']) }}">-- Select --</option>
                                        <option value="{{ route('view-group',['pending']) }}">Pending Orders</option>
                                        <option value="{{ route('view-group',['Rejected']) }}">Rejected Orders</option>
                                        <option value="{{ route('view-group',['delivered']) }}">Delivered order</option>
                                    </select>
                                </p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                               
                                    <thead>
                                        <th>ID</th>
                                    	<th>From</th>
                                    	<th>Delivery time</th>
                                    	<th>Food</th>
                                    	<th>Quantity</th>
                                    	<th>Price(GHS)</th>
                                    	<th>Status</th>
                                    	<th>Action(s)</th>
                                    </thead>
                                    <tbody>
    @include('admins.order_tmpl')
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

 
@endsection
@section('modals')

<div id="orderModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius: 0px;background: #e7e7e7;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: : black;">Order Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-6">
        	<dl>
			  <dt><i class="fa fa-chevron-circle-right"></i> Order ID</dt>
			  <dd style="margin-left: 30px;" id="order_id">89486</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> Food</dt>
			  <dd style="margin-left: 30px;" id="order_food">Rice malon</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> Delivery time</dt>
			  <dd style="margin-left: 30px;" id="order_del_time">23:34</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> From</dt>
			  <dd style="margin-left: 30px;" class="user_name">John Doe</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> Quantity</dt>
			  <dd style="margin-left: 30px;" id="order_quantity">2</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> Price(GHS)</dt>
			  <dd style="margin-left: 30px;" id="order_price">23.45</dd>

			  <dt><i class="fa fa-chevron-circle-right"></i> Ordered at</dt>
			  <dd style="margin-left: 30px;" id="order_at">12/3/2017 12:45 PM
			  
			</dl>
        	</div>
        	<div class="col-md-6">
        	<dl>
              <dt><i class="fa fa-chevron-circle-right"></i> Full name</dt>
              <dd style="margin-left: 30px;" class="user_name">Ahmed Dawuda</dd>

              <dt><i class="fa fa-chevron-circle-right"></i> FoodSpace handle</dt>
              <dd style="margin-left: 30px;" id="handle">Operon</dd>

              <dt><i class="fa fa-chevron-circle-right"></i> Address</dt>
              <dd style="margin-left: 30px;">
                  <ul>
                      <li id="country">Country - Ghana</li>
                      <li id="state">State - Greater Accra</li>
                      <li id="city">City - Tema</li>
                      <li id="district">District - Tema North</li>
                      <li id="street">Street - Tema Community 5, TDC residence</li>
                      <li id="hseno">House no. - H/B69</li>
                  </ul>
              </dd>

              <dt><i class="fa fa-chevron-circle-right"></i> Current location</dt>
              <dd style="margin-left: 30px;" id="current_loc">Tema comm. 4 chemu street</dd>

              {{-- <dt><i class="fa fa-chevron-circle-right"></i> From</dt>
              <dd style="margin-left: 30px;">John Doe</dd>

              <dt><i class="fa fa-chevron-circle-right"></i> Quantity</dt>
              <dd style="margin-left: 30px;">2</dd>

              <dt><i class="fa fa-chevron-circle-right"></i> Price(GHS)</dt>
              <dd style="margin-left: 30px;">23.45</dd>

              <dt><i class="fa fa-chevron-circle-right"></i> Ordered at</dt>
              <dd style="margin-left: 30px;">12/3/2017 12:45 PM
               --}}
            </dl>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      	<a  class="btn btn-info" style="padding: 2px;" href="#" data-toggle="tooltip" title="Delivered"><i class="fa fa-check"></i></a>
      	<a  class="btn btn-info" style="padding: 2px;" href="#" data-toggle="tooltip" title="Reject"><i class="fa fa-times"></i></a>
      	<a class="btn btn-info" style="padding: 2px;" href="#" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>
        <button type="button" class="btn btn-info" data-dismiss="modal" style="padding: 4px;" data-toggle="tooltip" title="Close">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/admin/admin_order.js')}}"></script>
@endsection