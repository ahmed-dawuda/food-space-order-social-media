@foreach($orders as $order)
        <tr>
            <td>
            <a href="{{ route('view-order',[$order['id']]) }}" data-toggle="tooltip" title="View order" class="view"><i class="fa fa-eye"></i></a>{{ $order['orderID'] }}</td>
            <td>{{ $order['user_name'] }}</td>
            <td>12:00 PM</td>
            <td>{{ $order['food_name'] }}</td>
            <td>{{ $order['quantity'] }}</td>
            <td>GHS {{ round($order['price'],2) }}</td>
            <td class="text-{{ $order['status_color'] }} status">{{ $order['status'] }} 
                @if($order['status'] == 'Pending')
                    <i class="fa fa-spinner fa-spin"></i>
                @elseif($order['status'] == 'Rejected')
                    <i class="fa fa-pause"></i>
                @else
                    <i class="fa fa-check"></i>
                @endif
            </td>

            <td class="dropdown">

            <a {{ $order['status'] == 'Delivered' || $order['status'] == 'Rejected'?'disabled':'' }} class="btn btn-info delivered" style="padding: 2px;" href="#" data-toggle="tooltip" title="Delivered"><i class="fa fa-check"></i></a> | 

            <a {{ $order['status'] == 'Delivered' || $order['status'] == 'Rejected'?'disabled':'' }} class="btn btn-info dropdown-toggle reject" data-toggle="dropdown" style="padding: 2px;"  href="#" data-toggle="tooltip" title="Reject"><i class="fa fa-times"></i></a > | 

            <a {{ $order['status'] == 'Pending'?'disabled':'' }} class="btn btn-info delete" style="padding: 2px;" href="#" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>
            <ul class="dropdown-menu">
                <li class="text-warning" style="margin: 10px;">Please Select a reason</li>
                <li><a href="#">Food not available</a></li>
                <li><a href="#">Time not possible</a></li>
                <li><a href="#">Destination can not be located</a></li>
                <li><a href="#">Other reason(s)</a></li>
              </ul>
            </td>
        </tr>
@endforeach