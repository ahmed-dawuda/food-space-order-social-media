@extends('layouts.dashboard')
{{-- @php
	$hasRestaurant = Auth::user()->restaurant;
	$not_set = is_null($hasRestaurant);
	$true = !$not_set;
	// dd($true)
@endphp --}}
@section('content')
<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card card-user">
            <div class="image">
                <img src="{{asset('assets/img/background.jpg')}}" alt="..." id="cover_here" />
            </div>
            <div class="content">
                <div class="author">
                  <img class="avatar border-white" src="{{asset('assets/img/faces/face-2.jpg')}}" alt="..." id="logo_here" />
                  <h4 class="title">{{$not_set?'Your name':ucfirst(Auth::user()->restaurant->name)}} <br />
                     <a href="#"><small>@FoodSpace</small></a>
                  </h4>
                </div>
                <p class="description text-center">
                    {{$not_set?'Your status comes here...':ucfirst(Auth::user()->restaurant->home_message)}}
                </p>
            </div>
            <hr>
            <div class="text-center">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h5><span class="label label-success">Available</span><br /><small>Food</small></h5>
                    </div>
                    <div class="col-md-4">
                        <h5><span class="label label-success">Open</span><br /><small>Working</small></h5>
                    </div>
                    <div class="col-md-3">
                        <h5>24,6$<br /><small>Spent</small></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h4 class="title">{{$not_set?'Your name':ucfirst(Auth::user()->restaurant->name)}}'s Top 3 Chefs</h4>
            </div>
            <div class="content">
                <ul class="list-unstyled team-members">


                    <form method="get" id="chef_form" action="{{route('admin-save')}}">        
                    <li>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="avatar">
                                            <img src="{{asset('assets/img/faces/face-0.jpg')}}" alt="Circle Image" class="img-circle img-no-padding img-responsive" id="chef1_here">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control border-input" placeholder="DJ Khaled - Manager" data-validate="required" data-validate-value="any" name="chef1_name" {{$true?'disabled=disabled':''}}
                                        value="{{$chef_is_null?'':$chefs[0]->full_name.' - '.$chefs[0]->role}}">
                                        <br />
                                        <span class="text-muted"><small>Name - Role</small></span>
                                    </div>

                                    <div class="col-xs-3 text-right">
                                        <label class="btn btn-sm btn-info btn-icon" for="chef1"><i class="ti-gallery"></i></label>
                                        
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="avatar">
                                            <img src="{{asset('assets/img/faces/face-1.jpg')}}" alt="Circle Image" class="img-circle img-no-padding img-responsive" id="chef2_here">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control border-input" placeholder="DJ Khaled - Manager" data-validate="required" data-validate-value="any" name="chef2_name" {{$true?'disabled=disabled':''}}
                                        value="{{$chef_is_null?'':$chefs[1]->full_name.' - '.$chefs[1]->role}}">
                                        <br />
                                        {{-- <span class="text-success"><small>Available</small></span> --}}
                                        <span class="text-muted"><small>Name - Role</small></span>
                                    </div>

                                    <div class="col-xs-3 text-right">
                                        <label class="btn btn-sm btn-info btn-icon" for="chef2"><i class="ti-gallery" ></i></label>
                                        
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="avatar">
                                            <img src="{{asset('assets/img/faces/face-3.jpg')}}" alt="Circle Image" class="img-circle img-no-padding img-responsive" id="chef3_here">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control border-input" placeholder="DJ Khaled - Manager" data-validate="required" data-validate-value="any" name="chef3_name" {{$true?'disabled=disabled':''}}
                                        value="{{$chef_is_null?'':$chefs[2]->full_name.' - '.$chefs[2]->role}}">
                                        <br />
                                        {{-- <span class="text-danger"><small>Busy</small></span> --}}
                                        <span class="text-muted"><small>Name - Role</small></span>
                                    </div>

                                    <div class="col-xs-3 text-right">
                                        <label class="btn btn-sm btn-info btn-icon" for="chef3"><i class="ti-gallery" ></i></label>
                                        
                                    </div>
                                </div>
                            </li>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                  </form>
                        </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <div class="header">
                <h4 class="title">About {{$not_set?'Your Company':ucfirst(Auth::user()->restaurant->name)}}</h4>
            </div>
            <div class="content">
                <form method="post" id="main_form" action="{{route('admin-save')}}">
                {{-- chef section --}}
                	<input type="hidden" name="chef_1">
                	<input type="hidden" name="chef_2">
                	<input type="hidden" name="chef_3">
                	<input type="file" id="chef1" name="chef1_photo" style="display: none;" {{$true?'disabled=disabled':''}}>
                	<input type="file" name="chef2_photo" style="display: none;" id="chef2" {{$true?'disabled=disabled':''}}>
                	<input type="file" name="chef3_photo" id="chef3" style="display: none;" {{$true?'disabled=disabled':''}}>
                {{-- end chef section --}}
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Change Company's name</label>
                                <input type="text" class="form-control border-input" placeholder="Company" value="{{$not_set?'':ucfirst(Auth::user()->restaurant->name)}}" name="name" data-validate="required" data-validate-value="name" {{$true?'disabled=disabled':''}}>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Manager/Administrator full name</label>
                                <input type="text" class="form-control border-input" placeholder="full name" value="{{$not_set?'':ucfirst(Auth::user()->restaurant->managers_name)}}" name="username" data-validate="required" data-validate-value="name" {{$true?'disabled=disabled':''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">{{$not_set?'Your company':ucfirst(Auth::user()->restaurant->name)}}'s Email address</label>
                                <input type="email" class="form-control border-input" placeholder="Email" name="email" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}} id="email"
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->email)}}">
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control border-input" placeholder="Home Address" value="" name="address" data-validate="required" data-validate-value="any">
                            </div>
                        </div>
                    </div> --}}

                    <div class="row">
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control border-input" placeholder="Country" name="country" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->country)}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>State/Region</label>
                                <input type="text" class="form-control border-input" placeholder="State / Region" name="state" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->state)}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control border-input" placeholder="City" name="city" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->city)}}">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                    <div class="col-md-4">
                            <div class="form-group">
                                <label>City Area/District</label>
                                <input type="text" class="form-control border-input" placeholder="District" name="district" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->district)}}">
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Street</label>
                                <input type="text" class="form-control border-input" placeholder="Street" name="street" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->street)}}">
                            </div>
                        </div>

                        

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Postal Address</label>
                                <input type="number" class="form-control border-input" placeholder="Postal Address" name="postal" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->postal)}}">
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cover" class="btn btn-info"><span class="ti-gallery"></span> Cover</label>
                                <input type="file" id="cover" style="display: none;" name="cover" {{$true?'disabled=disabled':''}}><br>
                                <label>Upload Cover Photo</label>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="logo" class="btn btn-info"><span class="ti-gallery"></span> Logo</label>
                                <input type="file" id="logo" style="display: none;" name="logo" {{$true?'disabled=disabled':''}}><br>
                                <label>Upload Profile Photo</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                        	<div class="form-group">
                                <label>Some message to be shown on cover</label>
                                <input type="text" class="form-control border-input" placeholder="Some message to be shown on cover" name="home_message" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}
                                value="{{$not_set?'':ucfirst(Auth::user()->restaurant->home_message)}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>About Us</label>
                                <textarea rows="3" class="form-control border-input" placeholder="Here can be your description" name="about_us" {{$true?'disabled=disabled':''}}>{{$not_set?'':ucfirst(Auth::user()->restaurant->about_us)}}</textarea >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Why Choose Us</label>
                                <textarea rows="3" class="form-control border-input" placeholder="Tell your customer why they should work with you" name="why_us" {{$true?'disabled=disabled':''}}>{{$not_set?'':ucfirst(Auth::user()->restaurant->why_us)}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Others You should know about Us</label>
                                <textarea rows="3" class="form-control border-input" placeholder="Tell them some other information" name="other_us" {{$true?'disabled=disabled':''}}>{{$not_set?'':ucfirst(Auth::user()->restaurant->other_us)}}</textarea >
                            </div>
                        </div>
                    </div>
<div id="open_hours">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>General Working hours</label>
                                <input type="text" class="form-control border-input" placeholder=".e.g Monday - Friday" name="general" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Start</label>
                                <input type="time" class="form-control border-input" name="start" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>End</label>
                                <input type="time" class="form-control border-input" name="end" data-validate="required" data-validate-value="any" {{$true?'disabled=disabled':''}}>
                            </div>
                        </div>
                        <div class="col-md-2">
                        	<div class="form-group">
                                <label>Click to add exceptions</label>
                                <button class="btn btn-info" href="#" id="except" {{$true?'disabled=disabled':''}}>Exception(s)</button>
                            </div>
                        </div>
                    </div>
				</div>				
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                    <div class="text-center">
                    	<button type="button" class="btn btn-info btn-fill btn-wd" {{$true?'':'disabled=disabled'}} id="editInfo">Change Info</button>
                        <button type="submit" class="btn btn-info btn-fill btn-wd" {{$true?'disabled=disabled':''}} id="saveInfo">Save Info</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>


</div>
@endsection
@section('js')
<script src="{{asset('js/admin/admin_profile.js')}}"></script>
@endsection