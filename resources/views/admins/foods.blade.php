@extends('layouts.dashboard')
@section('content')
<div class="row" id="food-details">
	<div class="col-md-3">
		<p >
			<img src="{{asset('assets/img/faces/face-2.jpg')}}" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
		</p>
		<p>Some info about food</p>
	</div>
	<div class="col-md-3">
		<p>
			<img src="{{asset('assets/img/faces/face-2.jpg')}}" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
		</p>
		<p>Some info about food</p>
	</div>
	<div class="col-md-3">
		<p>
			<img src="{{asset('assets/img/faces/face-2.jpg')}}" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
		</p>
		<p>Some info about food</p>
	</div>
	<div class="col-md-3">
		<p>
			<img src="{{asset('assets/img/faces/face-2.jpg')}}" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
		</p>
		<p>Some info about food</p>
	</div>
</div>
<button class="btn btn-info" id="add_more"><i class="ti-plus"></i> Add More</button>

<form id="add_food_form" method="get" style="display: none;" action="{{route('admin-add-food')}}">
<div class="col-md-1"></div>
<div class="col-md-10">
<div class="row">
	<div class="col-lg-3 col-md-4">
        <div>
            
                <img id="logo_here" style="width: 100%;  height: 100%;" src="{{asset('assets/img/faces/face-2.jpg')}}" alt="..."/>
           
        </div>
    </div>
	<div class="col-lg-9 col-md-8">
	   
	        
	        <div class="content">
	           
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="row">
	                        	<div class="col-md-6">
	                        		<div class="form-group">
			                            <label>food name</label>
			                            <input type="text" class="form-control border-input" placeholder="Food name" name="food_name">
			                        </div>
	                        	</div>
	                        	<div class="col-md-6">
	                        		<div class="form-group">
			                            <label>food type</label>
			                            <select class="form-control border-input" name="type">
			                            	<option value="break fast">Break fast</option>
			                            	<option value="snack">Snack</option>
			                            	<option value="lunch">Lunch</option>
			                            	<option value="supper">Supper</option>
			                            </select>
			                        </div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                            <label>Ingredients [Please separet multiple ingredients by / character]</label>
	                            <input type="text" class="form-control border-input" placeholder="Type important Ingredients separated by /" name="ingredients">
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                            <label>Price(s) GHS [Please separet multiple prices by / character]</label>
	                            <input type="text" class="form-control border-input" placeholder="Enter the prices" name="prices">
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                <div class="col-md-4 text-center">
	                	<div class="form-group">
                            <label for="logo" class="btn btn-info"><span class="ti-gallery"></span> Photo</label>
                            <input type="file" id="logo" style="display: none;" name="photo"><br>
                            <label>Upload Photo for Food</label>
                        </div>
	                </div>
	                <div class="col-md-8 text-center">
	                    <button type="submit" class="btn btn-info btn-wd" id="submit"><i class="ti-plus"></i> Add</button>
	                </div>
	                </div>
	                <div class="clearfix"></div>
	            
	        </div>
	   
	</div>
	</div>
	</div>
	<div class="col-md-1"></div>
</form>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/admin/admin_food.js')}}"></script>
@endsection