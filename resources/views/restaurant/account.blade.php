<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    {{ ucfirst($restaurant->name) }} | Home
  </title>

  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
<!--

Template 2076 Zentro

http://www.tooplate.com/view/2076-zentro

-->
  <link rel="stylesheet" href="{{asset('zentro/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('zentro/css/animate.min.css')}}">
  <link rel="stylesheet" href="{{asset('zentro/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('zentro/css/nivo-lightbox.css')}}">
  <link rel="stylesheet" href="{{asset('zentro/css/nivo_themes/default/default.css')}}">
  <link rel="stylesheet" href="{{asset('zentro/css/style.css')}}">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>

  <style type="text/css">
  .red{
    color: #ff9999;
  }
  .alignl{
    text-align: right;
  }
  .btlr{
    border-radius: 50px;
    border-top-left-radius: 0px;
    padding: 0;
    margin: 0;
  }
  .btrr{
    border-radius: 50px;
    border-top-right-radius: 0px;
  }
  .pl{
    padding-left: 100px; 
  }
    .blue{
      color: #00BFFF;
    }
    .modblue{
      background: #00BFFF;
      color: white;
      /*border: #00BFFF;*/
    }
    .right{
      align-content: right;
    }

    .myspecs{
    border-radius: 0px;
    vertical-align: center;
  }
  .chat{
    margin-top: 2px;
  }
  .live{
    color: #7FFF00;
  }
  select,option{
                color: #000111;
                border-radius: 5px;
                padding: 6px;
            }
    .modal .modal-body {
    max-height: 420px;
    overflow-y: auto;
}
  </style>
</head>
<body>

<!-- preloader section -->
<section class="preloader">
  <div class="sk-spinner sk-spinner-pulse"></div>
</section>

<!-- navigation section -->
<section class="navbar navbar-default navbar-fixed-top blue" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span>
      </button>
      <span class="navbar-brand"><a href="{{ route('customer-home') }}" class="blue" title="Go Home"><i class="fa fa-home"></i></a> <i class="fa fa-angle-left"></i> {{ ucfirst($restaurant->name) }}|
      <a href="{{ route('previous-restaurant',[$restaurant->id]) }}" title="Previous Restaurant"><i class="fa fa-arrow-left blue"></i></a>
      <a href="{{ route('next-restaurant',[$restaurant->id]) }}" title="Next Restaurant"><i class="fa fa-arrow-right blue"></i></a>
      </span>
      <!-- <span class="navbar-brand">
      <a href="#"><i class="fa fa-arrow-right blue"></i></a>
      </span> -->
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#home" class="smoothScroll" title="[restaurant] Home" ><i class="fa fa-home fa-2x blue"></i></a></li>
        <li><a href="#gallery" class="smoothScroll" title="Our Foods"><i class="fa fa-cutlery fa-2x blue"></i></a></li>
        <li><a href="#menu" class="smoothScroll" title="Our Special Menu"><i class="fa fa-bars fa-2x blue"></i></a></li>
        <li><a href="#team" class="smoothScroll" title="Our Chefs"><i class="fa fa-users fa-2x blue"></i></a></li>
        <li><a href="#" class="smoothScroll" title="Live Chat" data-toggle="modal" data-target="#chatModal"><i class="fa fa-comments fa-2x blue"></i></a></li>
        {{-- <li><a href="#" class="smoothScroll" title="Subscribe To Our NewsLetter" data-toggle="modal" data-target="#subscribeModal"><i class="fa fa-internet-explorer fa-2x blue"></i></a></li> --}}
        <li><a href="#contact" class="smoothScroll" title="Make Suggestion"><i class="fa fa-phone fa-2x blue"></i></a></li>

        <li id="thumbsup"><a href="#" class="smoothScroll blue" title="Follow Us" id="follow"><i class="fa fa-thumbs-up fa-2x blue"></i></a></li>

        <li id="thumbsdown"><a href="#" class="smoothScroll blue" title="Unfollow Us" id="unfollow"><i class="fa fa-thumbs-down fa-2x blue"></i></a></li>

        <li><a href="#" class="smoothScroll blue" title="Suggest Us To A Friend" id="tag"><i class="fa fa-tag fa-2x blue"></i></a></li>
      </ul>
    </div>
  </div>
</section>


<!-- home section -->
<section id="home" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <h1>{{ ucfirst($restaurant->name) }}</h1>
        <h2>{{ ucfirst($restaurant->home_message) }}</h2>
        <a href="#about" class="smoothScroll btn btn-default">ABOUT US</a>
      </div>
    </div>
  </div>    
</section>


<!-- About us section -->
<section id="about" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
        <h1 class="heading">About Us</h1>
        <hr>
      </div>
      <div class="col-md-4 col-sm-4">
      <h1 class="heading">About {{ ucfirst($restaurant->name) }}</h1>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

 Ipsum is simply dummy
 When an unknown
 The printing and typesetting
 Lorem Ipsum has been
      </div>
      <div class="col-md-4 col-sm-4">
      <h1 class="heading">Why Choose Akuzi{{ ucfirst($restaurant->name) }}</h1>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

 Ipsum is simply dummy
 When an unknown
 The printing and typesetting
 Lorem Ipsum has been
      </div>
      <div class="col-md-4 col-sm-4">
      <h1 class="heading">Statistics</h1>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

 Ipsum is simply dummy
 When an unknown
 The printing and typesetting
 Lorem Ipsum has been
      </div>
    </div>
  </div>
</section>

<section id="gallery" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
        <h1 class="heading">Food Gallery</h1>
        <hr>
      </div>

      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
        <a href="{{asset('zentro/images/gallery-img1.jpg')}}" data-lightbox-gallery="zenda-gallery"><img src="{{asset('zentro/images/gallery-img1.jpg')}}" alt="gallery img"></a>
        <div>
          <h3>Lemon-Rosemary Prawn</h3>
          <span>Seafood / Shrimp / Lemon</span>
        </div>
        <a href="{{asset('zentro/images/gallery-img2.jpg')}}" data-lightbox-gallery="zenda-gallery"><img src="{{asset('zentro/images/gallery-img2.jpg')}}" alt="gallery img"></a>
        <div>
          <h3>Lemon-Rosemary Vegetables</h3>
          <span>Tomato / Rosemary / Lemon </span>
        </div>
      </div>

      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
        <a href="{{asset('zentro/images/gallery-img3.jpg')}}" data-lightbox-gallery="zenda-gallery"><img src="{{asset('zentro/images/gallery-img3.jpg')}}" alt="gallery img"></a>
        <div>
          <h3>Lemon-Rosemary Bakery</h3>
          <span>Bread / Rosemary / Orange</span>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.9s">
        <a href="{{asset('zentro/images/gallery-img4.jpg')}}" data-lightbox-gallery="zenda-gallery"><img src="{{asset('zentro/images/gallery-img4.jpg')}}" alt="gallery img"></a>
        <div>
          <h3>Lemon-Rosemary Salad</h3>
          <span>Chicken / Rosemary / Green</span>
        </div>
        <a href="{{asset('zentro/images/gallery-img5.jpg')}}" data-lightbox-gallery="zenda-gallery"><img src="{{asset('zentro/images/gallery-img5.jpg')}}" alt="gallery img"></a>
        <div>
          <h3>Lemon-Rosemary Pizza</h3>
          <span>Pasta / Rosemary / Green</span>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- menu section -->
<section id="menu" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
        <h1 class="heading">Special Menu</h1>
        <hr>
      </div>

@foreach($restaurant->foods as $food)
      <div class="col-md-6 col-sm-6">
        <h4>{{ $food->name }} ................ <span>
        <ul>
          @foreach(explode("/",$food->pricing) as $price)
            <li>Ghc {{round($price,2)}}</li>
          @endforeach
        </ul>
        </span></h4>
        <h5>Type <i class="fa fa-angle-right"></i> <i class="fa fa-clock-o"> {{$food->period}}</i></h5>
        <a href="{{$food->id}}"><i class="fa fa-shopping-cart"> Order</i></a>
      </div>
@endforeach

    </div>
  </div>
</section>    


<!-- team section -->
<section id="team" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
        <h1 class="heading">Meet {{ ucfirst($restaurant->name) }} chefs</h1>
        <hr>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
        <img src="{{asset('zentro/images/team1.jpg')}}" class="img-responsive center-block" alt="team img">
        <h4>Thanya</h4>
        <h3>Main Chef</h3>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
        <img src="{{asset('zentro/images/team2.jpg')}}" class="img-responsive center-block" alt="team img">
        <h4>Lynda</h4>
        <h3>Pizza Specialist</h3>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.9s">
        <img src="{{asset('zentro/images/team3.jpg')}}" class="img-responsive center-block" alt="team img">
        <h4>Jenny Ko</h4>
        <h3>New Baker</h3>
      </div>
    </div>
  </div>
</section>


<!-- contact section -->
<section id="contact" class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-1 col-md-10 col-sm-12 text-center">
        <h1 class="heading">Make Suggestion</h1>
        <hr>
      </div>
      <div class="col-md-offset-1 col-md-10 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
        <form action="{{route('suggest')}}" method="post" id="contactForm">
        <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
        <input type="hidden" name="_token" value="{{Session::token()}}">
          <div class="col-md-12 col-sm-12">
            <input name="title" type="text" class="form-control" id="email" placeholder="Suggestion Title">
          </div>

          <div class="col-md-12 col-sm-12">
            <textarea name="suggestion" rows="8" class="form-control" id="message" placeholder="Suggestion Here"></textarea>
          </div>

          <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
            <input name="submit" type="submit" class="form-control" id="submit" value="Suggest">
          </div>

        </form>
      </div>
      <div class="col-md-2 col-sm-1"></div>
    </div>
  </div>
</section>


<!-- footer section -->
<footer class="parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
        <h2 class="heading">Contact Info.</h2>
        <div class="ph">
          <p><i class="fa fa-phone"></i> Phone</p>
          <h4>[Phone]</h4>
        </div>
        <div class="address">
          <p><i class="fa fa-map-marker"></i> Our Location</p>
          <h4>[Location of restaurant]</h4>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
        <h2 class="heading">Open Hours</h2>
          <p>Sunday <span>10:30 AM - 10:00 PM</span></p>
          <p>Mon-Fri <span>9:00 AM - 8:00 PM</span></p>
          <p>Saturday <span>11:30 AM - 10:00 PM</span></p>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
        <h2 class="heading">Follow Us</h2>
        <ul class="social-icon">
          <li><a href="#" class="fa fa-facebook wow bounceIn" data-wow-delay="0.3s"></a></li>
          <li><a href="#" class="fa fa-twitter wow bounceIn" data-wow-delay="0.6s"></a></li>
          <li><a href="#" class="fa fa-behance wow bounceIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fa fa-dribbble wow bounceIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fa fa-github wow bounceIn" data-wow-delay="0.9s"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>


<!-- copyright section -->
<section id="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <h3>{{ ucfirst($restaurant->name) }}</h3>
        <p>Copyright © foodbook.com
                
                | Design: Ahmed Dawuda
        </p>
      </div>
    </div>
  </div>
</section>
<div>
  @include('includes.chat')
</div>
<!-- JAVASCRIPT JS FILES -->  
<script src="{{asset('zentro/js/jquery.js')}}"></script>
<script src="{{asset('zentro/js/bootstrap.min.js')}}"></script>
<script src="{{asset('zentro/js/jquery.parallax.js')}}"></script>
<script src="{{asset('zentro/js/smoothscroll.js')}}"></script>
<script src="{{asset('zentro/js/nivo-lightbox.min.js')}}"></script>
<script src="{{asset('zentro/js/wow.min.js')}}"></script>
<script src="{{asset('zentro/js/custom.js')}}"></script>
{{-- <script src="{{asset('js/chat.js')}}"></script> --}}
<script src="{{asset('js/jquery.tmpl.js')}}"></script>
<script src="{{asset('js/bootstrap-tokenfield.js')}}"></script>
<script src="{{asset('js/restaurant.js')}}"></script>

</body>
</html>