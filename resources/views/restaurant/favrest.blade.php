@extends('layouts.users')
@section('url')
	<i class="icon-angle-right"></i>
	Favuorite Resturants
@endsection
@section('content')
<div class="row-fluid sortable">

	<div class="box span3">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white picture"></i><span class="break"></span> [Restaurant name]</h2>
		</div>
		<div class="box-content">
			<div class="masonry-gallery">
					<div id="image-1" class="masonry-thumb">
					<img class="grayscale" src="img/gallery/photo1.jpg" alt="Sample Image 1">
				</div>
						
			</div>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at dapibus ex. Praesent sollicitudin eget enim id posuere. Duis sed mollis tellus, ut sodales augue. Aliquam eleifend magna ac ipsum maximus semper. Sed sodales interdum nunc, id dictum massa luctus vel. Duis nisl sapien, maximus ut lacinia eu, vulputate sit amet risus. Ut vitae tempus lorem.</p>
		<a href="#" class="btn btn-primary" >View</a>
	</div>

	<div class="box span3">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white picture"></i><span class="break"></span> [Restaurant name]</h2>
		</div>
		<div class="box-content">
			<div class="masonry-gallery">
					<div id="image-1" class="masonry-thumb">
					<img class="grayscale" src="img/gallery/photo1.jpg" alt="Sample Image 1">
				</div>
						
			</div>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at dapibus ex. Praesent sollicitudin eget enim id posuere. Duis sed mollis tellus, ut sodales augue. Aliquam eleifend magna ac ipsum maximus semper. Sed sodales interdum nunc, id dictum massa luctus vel. Duis nisl sapien, maximus ut lacinia eu, vulputate sit amet risus. Ut vitae tempus lorem.</p>
		<a href="#" class="btn btn-primary" >View</a>
	</div>

	<div class="box span3">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white picture"></i><span class="break"></span> [Restaurant name]</h2>
		</div>
		<div class="box-content">
			<div class="masonry-gallery">
					<div id="image-1" class="masonry-thumb">
					<img class="grayscale" src="img/gallery/photo1.jpg" alt="Sample Image 1">
				</div>
						
			</div>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at dapibus ex. Praesent sollicitudin eget enim id posuere. Duis sed mollis tellus, ut sodales augue. Aliquam eleifend magna ac ipsum maximus semper. Sed sodales interdum nunc, id dictum massa luctus vel. Duis nisl sapien, maximus ut lacinia eu, vulputate sit amet risus. Ut vitae tempus lorem.</p>
		<a href="#" class="btn btn-primary" >View</a>
	</div>

	<div class="box span3">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white picture"></i><span class="break"></span> [Restaurant name]</h2>
		</div>
		<div class="box-content">
			<div class="masonry-gallery">
					<div id="image-1" class="masonry-thumb">
					<img class="grayscale" src="img/gallery/photo1.jpg" alt="Sample Image 1">
				</div>
						
			</div>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at dapibus ex. Praesent sollicitudin eget enim id posuere. Duis sed mollis tellus, ut sodales augue. Aliquam eleifend magna ac ipsum maximus semper. Sed sodales interdum nunc, id dictum massa luctus vel. Duis nisl sapien, maximus ut lacinia eu, vulputate sit amet risus. Ut vitae tempus lorem.</p>
		<a href="#" class="btn btn-primary" >View</a>
	</div>


</div>
@endsection