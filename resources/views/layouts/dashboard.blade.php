<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FoodSpace |
     @if(url()->current() == route('admin-home'))
        Welcome to Dashboard
     @elseif(url()->current() == route('admin-foods'))
        Foods
     @elseif(url()->current() == route('admin-orders'))
        Orders
     @elseif(url()->current() == route('admin-messages'))
        Messages
     @elseif(url()->current() == route('admin-notifications'))
        Notifications
     @elseif(url()->current() == route('admin-profile'))
        Profile
     @endif
     </title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{asset('assets/css/paper-dashboard.css')}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    {{-- <link href="{{asset('assets/css/demo.css')}}" rel="stylesheet" /> --}}


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/css/themify-icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/nprogress.css')}}">
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    FoodSpace
                </a>
            </div>

            <ul class="nav">
                <li class="{{ url()->current() == route('admin-home')? 'active':'' }}">
                    <a href="{{route('admin-home')}}">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="{{ url()->current() == route('admin-profile')? 'active':'' }}">
                    <a href="{{route('admin-profile')}}">
                        <i class="ti-write"></i>
                        <p>Edit Info</p>
                    </a>
                </li>
                <li class="{{ url()->current() == route('admin-orders')? 'active':'' }}">
                    <a href="{{route('admin-orders')}}">
                        <i class="ti-shopping-cart"></i>
                        <p>Orders</p>
                    </a>
                </li>

                <li class="{{ url()->current() == route('admin-foods')? 'active':'' }}">
                    <a href="{{route('admin-foods')}}">
                        <i class="ti-pie-chart"></i>
                        <p>Our Foods</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="ti-map"></i>
                        <p>Maps</p>
                    </a>
                </li>

                <li class="{{ url()->current() == route('admin-messages')? 'active':'' }}">
                    <a href="{{route('admin-messages')}}">
                        <i class="ti-email"></i>
                        <p>Messages</p>
                    </a>
                </li>
                
                <li class="{{ url()->current() == route('admin-notifications')? 'active':'' }}">
                    <a href="{{route('admin-notifications')}}">
                        <i class="ti-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>
                {{-- <li class="{{url()->current() == route('admin-settings')?'active':''}}">
                    <a href="{{route('admin-settings')}}">
                        <i class="ti-settings"></i>
                        <p>Settings</p>
                    </a>
                </li> --}}
				<li class="active-pro">
                    <a href="{{route('user-logout')}}" id="logout">
                        <i class="ti-power-off"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{is_null(Auth::user()->restaurant)?'Your Restaurants Name':ucfirst(Auth::user()->restaurant->name)}}</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-settings"></i>
                                    <p>Settings</p>
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu" id="setting">
                                <li><a href="{{route('admin-settings',['go-premium',1])}}" class="dash"><i class="ti-star"></i> Go Premium</a></li>
                                <li><a href="{{route('admin-settings',['enable-sms',1])}}" class="dash"><i class="ti-bell"></i> Enable SMS notification</a></li>
                                <li><a href="{{route('admin-settings',['scope',1])}}" class="dash"><i class="ti-target"></i> Set Your Scope</a></li>
                                @if(!is_null(Auth::user()->restaurant))
                                    @if(Auth::user()->restaurant->working)
                                        <li><a href="{{route('admin-working',['close'])}}" id="close"><i class="ti-alarm-clock" style="color: green;"></i> Currently Opened - Close Work</a></li>
                                    @else
                                        <li><a href="{{route('admin-working')}}" id="open"><i class="ti-alarm-clock"></i> Currently Closed - Open Work </a></li>
                                    @endif
                                @endif
                                
                                <li><a href="{{route('user-logout')}}"><i class="ti-power-off"></i> Logout </a></li>
                              </ul>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>


        <div class="content" id="mainBody">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-signal"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Popularity</p>
                                            <i class="ti-star"></i>
                                            <i class="ti-star"></i>
                                            <i class="ti-star" style="color: #CD853F;"></i>
                                            <i class="ti-star" style="color: #CD853F;"></i>
                                            <i class="ti-star" style="color: #CD853F;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-stats-up"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Sales</p>
                                            <i class="ti-arrow-up"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-calendar"></i> Last day
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-shopping-cart"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Order(s)</p>
                                            23
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-timer"></i> In the last hour
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Customers(Visitors)</p>
                                            45(162)
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @yield('content')        
                
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="#">
                                Dimension Foundation
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Visit Us
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Privacy, Terms & Conditions
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
                </div>
            </div>
        </footer>

    </div>
</div>

@yield('modals')
<div id="dashboardModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius: 0px;background: #e7e7e7;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="border-bottom: 1px solid grey;">[LINK DETAIL]</h4>
      </div>
      <div class="modal-body" id="settingBody">
        
      </div>
      <div class="modal-footer">
        <a  class="btn btn-info" style="padding: 2px;" id="proceed" href="#" data-toggle="tooltip" title="Proceed"><i class="ti-check" ></i></a>
        <button type="button" class="btn btn-info" data-dismiss="modal" style="padding: 4px;" data-toggle="tooltip" title="Close">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
    
    <!--   Core JS Files   -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{asset('assets/js/bootstrap-checkbox-radio.js')}}"></script>

	<!--  Charts Plugin -->
	<script src="{{asset('assets/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{asset('assets/js/bootstrap-notify.js')}}"></script>

    <!--  Google Maps Plugin    -->
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> --}}

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="{{asset('assets/js/paper-dashboard.js')}}"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{asset('assets/js/demo.js')}}"></script>

    <script src="{{asset('js/nprogress.js')}}"></script>
    <script src="{{asset('js/admin/dashboard.js')}}"></script>
    @yield('js')

	<script type="text/javascript">
    	$(document).ready(function(){
            NProgress.start();
            NProgress.inc()
            NProgress.done();
            $('[data-toggle="tooltip"]').tooltip(); 
        	// demo.initChartist();
@if(is_null(Auth::user()->restaurant))
    $('.nav a').not('#logout').click(function(event){
        event.preventDefault()
        $.notify({
                icon: 'ti-lock',
                message: " Please the account <b>is locked</b> until you update your profile"

            },{
                type: 'danger',
                timer: 100
            });
    })
        	
@endif


    	});
	</script>

</html>
