@php
 $set = isset($profile) ? true : false;
@endphp
<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}">
        <title>Edit Profile | {{ucfirst(Auth::user()->username)}} </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="{{asset('timer/css/bootstrap.min.css')}}">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{asset('timer/css/ionicons.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{asset('timer/css/animate.css')}}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{asset('timer/css/slider.css')}}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{asset('timer/css/owl.carousel.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/owl.theme.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/jquery.fancybox.css')}}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{asset('timer/css/main.css')}}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{asset('timer/css/responsive.css')}}">

        <link rel="stylesheet" type="text/css" href="{{asset('zentro/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/nprogress.css')}}">
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="{{asset('timer/js/vendor/modernizr-2.6.2.min.js')}}"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- jquery template -->
        <script src="{{asset('js/jquery.tmpl.js')}}"></script>
        <!-- owl carouserl js -->
        <script src="{{asset('timer/js/owl.carousel.min.js')}}"></script>
        <!-- bootstrap js -->

        <script src="{{asset('timer/js/bootstrap.min.js')}}"></script>
        <!-- wow js -->
        <script src="{{asset('timer/js/wow.min.js')}}"></script>
        <!-- slider js -->
        <script src="{{asset('timer/js/slider.js')}}"></script>

        <script src="{{asset('timer/js/jquery.fancybox.js')}}"></script>
        <!-- template main js -->
        <script src="{{asset('timer/js/main.js')}}"></script>
        <script src="{{asset('js/sweetalert2.js')}}"></script>
        <script src="{{asset('js/validateForm.js')}}"></script>
        <!-- custom js -->
        <script src="{{asset('js/nprogress.js')}}"></script>
        <script src="{{asset('js/edit_profile.js')}}"></script>
        <style type="text/css">
           
            .profile-nav{
                background: white;
                box-shadow: 0px 0px 6px;
                /*position: sticky;*/
            }
            .centa{
                text-align: center;
                padding: 15px;
                color: white;
                box-shadow: 5px;
            }

            .green{
                background: #3CB371;
            }

            .black{
                background: #000000;
                color: white;
            }
            .red{
                color: #ff9999;
              }
            .redback{
                background: #ff9999;
            }
            .modblue{
                  background: #00BFFF;
                  color: white;
                  /*border: #00BFFF;*/
                }
            .myspecs{
                border-radius: 0px;
                vertical-align: center;
              }
              .live{
                color: #7FFF00;
              }
              .alignl{
                text-align: right;
              }
              .btlr{
                border-radius: 50px;
                border-top-left-radius: 0px;
              }
              .btrr{
                border-radius: 50px;
                border-top-right-radius: 0px;
              }
              .pl{
                padding-left: 100px; 
              }
            .bordra{
                border-radius: 0px;
            }  
            select,option{
                color: grey;
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
                padding: 5px;
            }

            .modal .modal-body {
                max-height: 420px;
                overflow-y: auto;
            }

            #change_button{
                display: none;
                /*box-shadow: 0px 0px 6px;*/
            }
            .box_shadow{
                box-shadow: 0px 0px 6px;
            }
            .header{
              color: white;
              padding: 200px;
              background: url({{asset('timer/images/profilebackground.jpg')}});
              background-size: 100% 100%;
              background-repeat: no-repeat;

            }
        </style>
    </head>
    <body>

<div class="container-fluid profile-nav navbar-fixed-top">
    
        <div class="row">
            <div class="col-sm-5"></div>

            <a href="{{route('customer-home')}}" title="Home"><div class="col-sm-1 centa green"><i class="fa fa-home"></i> Home</div></a>  
            @if(Auth::user()->profile_updated)
              <a href="{{ route('profile',[Auth::user()->id]) }}" title="Timeline"><div class="col-sm-1 centa black"><i class="fa fa-user"></i> Timeline</div></a>  
            @else
                <a href="{{ route('user-logout') }}" title="logout"><div class="col-sm-1 centa black"><i class="fa fa-sign-out"></i>SignOut</div></a>
            @endif
            <div class="col-sm-5"></div>
        </div>
    <!-- </div> -->
</div>


<section id="blog-full-width">


<div class="container">
    <div class="row" style="margin-top: 40px; box-shadow: 0px 0px 1px; border-radius: 5px;">
    
    <p class="container header"><h2 style="text-align: center; margin-bottom: 0px;" id="edit-head">Edit Your Profile
    @if(URL::current() == route('customer-update-profile'))
        First, It's required
    @endif
    </h2></p>
<!-- SIDE BAR -->
<div class="col-md-4" >
        <!-- <div class="affix"> -->
            <div class="sidebar">
                <div class="search widget">
                    <form action="" method="get" class="searchform" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Type e.g. 'fullname' for criteria...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"> <i class="ion-search"></i> </button>
                            </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                    <div class="author widget">
                        <img class="img-responsive" src="{{asset('timer/images/author/author-bg.jpg')}}" id="cover_picture_here">
                        <div class="author-body text-center">
                            <div class="author-img">
                                <img src="{{asset('timer/images/author/author.jpg')}}" id="profile_picture_here" alt="Profile Picture" width="84.028" height="83.75">
                            </div>
                            <div class="author-bio">
                                <h3 id="username_here">{{ucfirst(Auth::user()->username)}}</h3>
                                <p id="status_here">{{$set?$profile->status:'Say something... Update your status'}}</p>
                            </div>
                        </div>
                    </div>
                <div class="categories widget" id="profile_preview">

                </div>
                </div>

                

            <!-- </div> -->
</div>

<!-- END SIDE BAR -->

<!-- BLOG BODY -->

            <div class="col-md-8"><br><br><br>
                <form method="post" id="profile_edit_form"  action="{{route('create-profile')}}" enctype="multipart/form-data">
            @if(URL::current()==route('customer-update-profile',['edit']))
                <input type="hidden" name="edit" value="1">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            @endif

<div class="input-group input-group-sm">
          <span class="input-group-addon">Full Name</span>
        <input type="text" name="full_name" class="form-control" placeholder="full name" id="full_name" value="{{$set?$profile->full_name:''}}" data-validate="required" data-validate-value="alpha"> 
</div>
        <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Date Of Birth</span>
       <input type="date" name="date_of_birth" class="form-control" placeholder="Date Of Birth" id="date_of_birth" value="{{$set?$profile->date_of_birth:''}}" data-validate="required" data-validate-value="date:"> 
</div>
       <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Gender</span>
       <select name="gender" id="gender" >
           <option value="0" {{$set? $profile->gender == 'Unspecified'?'selected': '':''}}>Unspecified</option>
           <option value="1" {{$set? $profile->gender == 'M'?'selected': '':''}}>Male</option>
           <option value="2" {{$set? $profile->gender == 'F'?'selected': '':''}}>Female</option>
       </select>
</div>
       <hr>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Country</span>
       <input type="text" name="country" class="form-control" placeholder="Country" id="country" value="{{$set?$location->country:''}}" 
       data-validate="required" data-validate-value="alpha:"> 
</div>
       <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">City</span>
       <input type="text" name="city" class="form-control" placeholder="City" id="city" value="{{$set?$location->city:''}}" 
       data-validate="required" data-validate-value="alphanum:"> 
</div>
       <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">State</span>
       <input type="text" name="state" class="form-control" placeholder="State" id="state" value="{{$set?$location->state:''}}"
       data-validate="required" data-validate-value="alpha:">
</div>
       <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Street</span>
       <input type="text" name="street" class="form-control" placeholder="Street" id="street" value="{{$set?$location->street:''}}"
       data-validate="required" data-validate-value="alphanum:"> 
</div>
       <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Zip Code</span>
       <input type="number" name="zip" class="form-control" placeholder="Zip Code" id="zip" value="{{$set?$location->zip_code:''}}"
       data-validate="required" data-validate-value="num:"> 
</div>
       <hr>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Work</span>
       <input type="text" name="work" class="form-control" placeholder="The Place You Currently Work" id="work" value="{{$set?$profile->work_place_name:''}}" data-validate="text">
</div>
        <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon">Position</span>
       <input type="text" name="position" class="form-control" placeholder="Your Position At Work" id="position" value="{{$set?$profile->position_at_work:''}}" data-validate="text"> 
</div>
{{--        <br>
<div class="input-group input-group-sm">
          <span class="input-group-addon" >Start Date</span>
       <input type="date" name="start_date" class="form-control" placeholder="What Date Did You Started?" id="start_date" value="{{$set?$profile->start_date:''}}" > 
</div> --}}
       <br>

       
<div class="input-group input-group-sm">
          <span class="input-group-addon">About Work</span>
        <textarea class="form-control" placeholder="What Do You Do At Work?" name="about_work" id="about_work">{{$set?$profile->about_work:''}}</textarea>
</div>
        <br> 
<div class="input-group input-group-sm">
          <span class="input-group-addon">Status</span>
        <textarea rows="4" cols="50" class="form-control" placeholder="Status: Say Something..." name="status" id="status">{{$set?$profile->status:''}}</textarea>
</div><br>

<div class="input-group input-group-sm">
          <span class="input-group-addon">Upload profile picture</span>
       <input type="file" name="profile_picture" class="form-control" id="profile_picture" > 
</div>
       <br>
       <div class="input-group input-group-sm">
          <span class="input-group-addon">Upload Cover picture</span>
       <input type="file" name="cover_picture" class="form-control" id="cover_picture"> 
        </div>
       <br>

         {{ csrf_field() }}
        <button type="submit" class="btn modblue chat box_shadow" id="preview_button"><i class="fa fa-eye" id="proceed_icon"></i> Preview</button>
        <button type="button" class="btn modblue chat box_shadow" id="change_button"><i class="fa fa-edit"></i> change</button>
        </form>
        <br>
            </div>
<!-- END BLOG -->

        </div>
        </div>
    </section>


    <script id="profileTemplate" type="text/x-jquery-tmpl">
        
                        <h3 class="widget-head">Profile Summary</h3>
                        <ul>
                            <li>
                                Full Name <span class="badge">${full_name}</span> 
                            </li>
                            <li>
                                Date Of Birth <span class="badge">${date_of_birth}</span> 
                            </li>
                            <li>
                                Gender <span class="badge">${gender}</span> 
                            </li>

                            <li>
                                Country <span class="badge">${country}</span>
                            </li>
                            <li>
                                City <span class="badge">${city}</span> 
                            </li>
                            <li>
                                State <span class="badge">${state}</span> 
                            </li>
                            <li>
                                Street <span class="badge">${street}</span> 
                            </li>
                            <li>
                                Works At <span class="badge">${work}</span> 
                            </li>
                            <li>
                                Position At Work <span class="badge">${position}</span> 
                            </li>
                            
                            <li>
                                <a href="">Status Updates</a> <span class="badge">1</span>
                            </li>
                            <li>
                                <a href="">Photo Uploads</a> <span class="badge">2</span>
                            </li>
                            <li>
                                <a href="">Video Uploads</a> <span class="badge">0</span>
                            </li>
                            
                        </ul>
                    
    </script>
  <script type="text/javascript">
  NProgress.start();
   NProgress.inc()
  NProgress.done();
</script>
<a href="{{URL::to('/')}}" id="domain"></a>
     </body>    
</html>