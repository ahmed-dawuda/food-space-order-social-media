<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}">
        <title>Timeline | {{ ucfirst($user_profile->username) }}</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="{{asset('timer/css/bootstrap.min.css')}}">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{asset('timer/css/ionicons.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{asset('timer/css/animate.css')}}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{asset('timer/css/slider.css')}}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{asset('timer/css/owl.carousel.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/owl.theme.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/jquery.fancybox.css')}}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{asset('timer/css/main.css')}}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{asset('timer/css/responsive.css')}}">

        <link rel="stylesheet" type="text/css" href="{{asset('zentro/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/nprogress.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.css')}}">

        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="{{asset('timer/js/vendor/modernizr-2.6.2.min.js')}}"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="{{asset('timer/js/owl.carousel.min.js')}}"></script>
        <!-- bootstrap js -->

        <script src="{{asset('timer/js/bootstrap.min.js')}}"></script>
        <!-- wow js -->
        <script src="{{asset('timer/js/wow.min.js')}}"></script>
        <!-- slider js -->
        <script src="{{asset('timer/js/slider.js')}}"></script>

        <script src="{{asset('timer/js/jquery.fancybox.js')}}"></script>
        <!-- template main js -->
        <script src="{{asset('timer/js/main.js')}}"></script>
        <script src="{{asset('js/nprogress.js')}}"></script>

        <style type="text/css">
            .profile-nav{
                background: white;
                box-shadow: 0px 0px 6px;
                /*position: sticky;*/
            }
            .centa{
                text-align: center;
                padding: 20px;
                color: white;
                box-shadow: 5px;
            }

            .green{
                background: #3CB371;
            }

            .black{
                background: #000000;
                color: white;
            }
            .red{
                color: #ff9999;
              }
            .redback{
                background: #ff9999;
            }
            .modblue{
                  background: #00BFFF;
                  color: white;
                  /*border: #00BFFF;*/
                }
            .myspecs{
                border-radius: 0px;
                vertical-align: center;
              }
              .live{
                color: #7FFF00;
              }
              .alignl{
                text-align: right;
              }
              .btlr{
                border-radius: 50px;
                border-top-left-radius: 0px;
                padding: 5px;
              }
              .btrr{
                border-radius: 10px;
                border-top-right-radius: 0px;
                padding: 5px;
              }
              .pl{
                padding-left: 100px; 
              }
            .bordra{
                border-radius: 0px;
            }  
            select,option{
                color: #00BFFF;
                border-radius: 5px;
                padding: 6px;
            }

            .modal .modal-body {
                max-height: 420px;
                overflow-y: auto;
            }

            .dropdown-menu > li{
                border-radius: 0px;  
                padding: 10px;
            }
            #messages li{
                padding: 20px;
                margin-top: 2px;
                color: white;
            }

            .offline{
                background: #f27171;
            }
            .online{
                background: #3CB371;
            }
            .underline{
                border-bottom: 1px dashed white;
            }

            .onlineform{
                background: #3CB371;
                border: 1px black;
                width: 100%;
                height: 100%;
                color: white;
            }
            .formHolder{
                margin-bottom: 2px;
            }

            #notifications li{
                padding: 15px;
                margin-top: 2px;
                color: white;
                /*background: #00BFFF;*/
                /*border-radius: 5px;*/
            }
            /*#somenews{
                overflow-y:auto;
                height: 480px;
                width: 100%;
            }*/

        </style>
    </head>
    <body>

<div class="container-fluid profile-nav navbar-fixed-top">
    <!-- <div class="navbar"> -->
        <div class="row">
            <div class="col-sm-5"></div>

            <a href="{{ route('customer-home') }}" title="Go Home" id="home">
                <div class="col-sm-1 centa green"><i class="fa fa-home"></i> Home</div>
            </a>
        @if(Auth::user()->id == $user_profile->id)
            <a href="{{route('customer-update-profile',['edit'])}}" title="Edit your profile">
                <div class="col-sm-1 centa modblue"><i class="fa fa-edit"></i> Edit</div>
            </a>
        @else
            <a href="{{route('getChats',[Auth::user()->id,$user_profile->id])}}" title="Message {{ucfirst($user_profile->username)}}" id="chatFriend">
                <div class="col-sm-1 centa modblue"><i class="fa fa-comment"></i> Chat</div>
            </a>
        @endif
             
            <div class="col-sm-5"></div>
        </div>
    <!-- </div> -->
</div>


<section id="blog-full-width">


<div class="container">
    <div class="row" id="mainBody">

<!-- SIDE BAR -->
{{-- <div class="col-md-1"></div> --}}
        <div class="col-md-4" >
        <!-- <div class="affix"> -->
            <div class="sidebar">
                <div class="search widget">
                    <div class="searchform" role="search">
                        <div class="input-group">
                            {{-- @if(Auth::user()->id == $user->id) --}}
                            <div class="dropdown">Account Settings
                                
                                <a class="btn btn-default dropdown-toggle" style="margin-left: 144px;" data-toggle="dropdown">
                                 <i class="fa fa-cogs"></i> </a>
                                <ul class="dropdown-menu">
                                @if(Auth::user()->id == $user_profile->id)
                                    <li><a href="{{route('trending')}}">Trending</a></li>
                                    <li><a href="{{route('top')}}">Top Restaurants</a></li>
                                    <li><a href="{{route('notifications')}}" id="totrigger">Notifications</a></li>
                                    <li><a href="{{route('my-messages')}}">New Messages</a></li>
                                    <li><a href="{{route('my-posts')}}">My Posts</a></li>
                                    <li><a href="{{route('news-feed')}}">News Feeds</a></li><hr>
                                @endif
                                    <li><a href="{{route('profile',[Auth::user()->id])}}" class="not">Timeline</a></li>
                                    <li><a href="{{route('customer-update-profile',['edit'])}}" class="not">Edit My Profile</a></li>
                                    <li><a href="{{ route('user-logout') }}" class="not">Logout</a></li>
                                    <li><a href="{{route('delete-account')}}" id="delete-account" class="not">Delete My Account</a></li>
                                  </ul>
                            </div>
                            {{-- @endif --}}
                            </div><!-- /input-group -->
                        </div>
                    </div>
                    <div class="author widget">
                        <img class="img-responsive" src="{{asset('timer/images/author/author-bg.jpg')}}">
                        <div class="author-body text-center">
                            <div class="author-img">
                                <img src="{{asset('timer/images/author/author.jpg')}}">
                            </div>
                            <div class="author-bio">
                                <h3>{{ ucfirst($user_profile->username) }}</h3>
                                <p>{{ ucfirst($user_profile->profile->status) }}</p>
                               {{--  @php
                                    $profile = $user->profile;
                                @endphp
                                <p>{{ $profile->status }}</p> --}}
                            </div>
                        </div>
                    </div>

                    <div class="categories widget">
                        <h3 class="widget-head">Profile Summary
                        
                        </h3>
                        <ul>
                            @if(Auth::user()->id == $user_profile->id)
                                <li>
                                Hide Profile Summary <span class="badge"><a href="#" title="Hide Profile Summary"><i class="fa fa-toggle-on"></i></a></span> 
                            </li>
                            @else
                                <li>
                                Show Profile Summary <span class="badge"><a href="#" title="Hide Profile Summary"><i class="fa fa-toggle-on"></i></a></span> 
                            </li>
                            @endif
                            
                            <li>
                                Full Name <span class="badge">{{ $user_profile->profile->full_name }}</span> 
                            </li>
                            <li>
                                Date Of Birth <span class="badge">{{ $user_profile->profile->date_of_birth }}</span> 
                            </li>
                            <li>
                                Gender <span class="badge">{{ $user_profile->profile->gender == 'M' ? 'Male' : 'Female' }}</span> 
                            </li>

                            <li>
                                Country <span class="badge">{{ $user_profile->location->country }}</span>
                            </li>
                            <li>
                                City <span class="badge">{{ ucfirst($user_profile->location->city) }}</span> 
                            </li>
                            <li>
                                State <span class="badge">{{ ucfirst($user_profile->location->state) }}</span> 
                            </li>
                            <li>
                                Street <span class="badge">{{ ucfirst($user_profile->location->street) }}</span> 
                            </li>
                            <li>
                                Works At <span class="badge">{{ ucfirst($user_profile->profile->work_place_name) }}</span> 
                            </li>
                            <li>
                                Position At Work <span class="badge">{{ ucfirst($user_profile->profile->position_at_work) }}</span> 
                            </li>
                            {{-- <li>
                                Interests <span class="badge">3</span>
                                <ol>
                                    <li>Football</li>
                                    <li>Praying</li>
                                    <li>Partying</li>
                                </ol>
                            </li> --}}
                            <li>
                                <a href="">Status Updates</a> <span class="badge">3</span>
                            </li>
                            <li>
                                <a href="">Photo Uploads</a> <span class="badge">33</span>
                            </li>
                            <li>
                                <a href="">Video Uploads</a> <span class="badge">7</span>
                            </li>
                            @if(Auth::user()->id != $user_profile->id)
                            <li id="whetherfollowing">
                                <a href="">You're Following</a> 
                                @if(Auth::user()->friendOf($user_profile->id))
                                <a href="{{route('customer-follow',[$user_profile->id,'stop'])}}" class="unfollow" id="profile_unfollow">
                                    <span class="badge">Unfollow <i class="fa fa-user-times"></i></span>
                                </a>
                                    <span class="badge">Yes</span>
                                @else
                                <a href="{{route('customer-follow',[$user_profile->id])}}" class="follow" id="profile_follow">
                                    <span class="badge">Follow <i class="fa fa-user-plus"></i></span>
                                </a>
                                   <span class="badge">No</span>
                                @endif
                            </li>
                            @endif
                        </ul>
                    </div>
                    
                    <div class="recent-post widget">
                        <h3>Recent Posts</h3>
                        <ul>
                        @foreach($user_profile->posts as $post)
                            <li>
                                <a href="#">{{ ucfirst($post->title) }}</a><br>
                                <time>{{ $post->created_at }}</time>
                            </li>
                        @endforeach
                            
                        </ul>
                    </div>
                </div>

            <!-- </div> -->
            </div>

<!-- END SIDE BAR -->

<!-- BLOG BODY -->
            <div class="col-md-6" id="posts">
            
            @foreach($user_profile->friendsPosts() as $post)
                <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                    <div class="blog-post-image">
                        <a href="#"><img class="img-responsive" src="{{asset('timer/images/blog/post-1.jpg')}}" alt="" /></a>
                    </div>
                    <div class="blog-content">
                        <h2 class="blogpost-title">
                        <a href="#">{{ ucfirst($post->title) }}</a>
                        </h2>
                        <?php
                            $author = App\User::find($post->user_id);
                        ?>
                       {{--  {{Auth::user()->friendOf($author->id)}} --}}
                        <div class="blog-meta">
                            <span>{{$post->created_at?$post->created_at:'7th Jul 20:21'}}</span>
                            <span> <i class="fa fa-user"></i> 
                            <a href="{{route('profile',[$author->id])}}" style="color: #00BFFF;  border-radius: 10px;padding: 5px;"> 
                                @if(Auth::user()->id == $author->id)
                                    You
                                @else
                                    {{ ucfirst($author->username) }}
                                @endif
                             </a></span>

                             @if(Auth::user()->id == $author->id)
                                <span>
                                <a href="{{route('customer-delete-post',[$post->id])}}" class="deletePost" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                Delete <i class="fa fa-times"></i></a></span>
                             @endif

                            @if(Auth::user()->friendOf($author->id))
                                <span><a href="{{route('customer-follow',[$author->id,'stop'])}}" style="color: #00BFFF;  border-radius: 10px;padding: 5px;" class="unfollow">Unfollow</a></span>
                            @elseif(Auth::user()->id != $author->id)
                                <span><a class="follow" href="{{route('customer-follow',[$author->id])}}" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">Follow</a></span>
                            @endif

                            <span><a href="">{{$author->admin?'Restaurant':'Customer'}}</a></span>
                        @if(Auth::user()->voted($post->id))
                            <span>
                                <a href="{{route('vote',[$post->id,'votedown'])}}" class="votedown" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                        VoteDown <i class="fa fa-level-down"></i>
                                </a>
                                <i class="post_votes">{{$user_profile->votes($post->id)}}</i>
                            </span>
                        @else  
                            <span>
                                <a href="{{route('vote',[$post->id])}}" class="voteup" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                        VoteUp <i class="fa fa-level-up"></i>
                                </a>
                                <i class="post_votes">{{$user_profile->votes($post->id)}}</i>
                            </span>
                        @endif

                        </div>
                        <p>
                            {{ ucfirst($post->body) }} 
                        </p>
                       {{--  <a href="#" class="btn btn-dafault btn-details ">Continue Reading</a> --}}
                       <a href="{{URL::to('/').'/post/'.$post->id}}" style="color: #00BFFF; border-radius: 10px;padding: 5px;" class="comment">
                            Comment <i class="fa fa-comment-o"></i></a>
                    </div>
                </article>
            @endforeach
            </div>
            {{-- <div class="col-md-1"></div> --}}
            <div class="col-md-2"><br><br><br><br><div id="somenews">Some news heome news herome news herome news herome news herre
            </div></div>
<!-- END BLOG -->

        </div>


    </section>
    <!--
    ==================================================
    Call To Action Section Start
    ================================================== -->
    
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
    <footer id="footer">
        <div class="container">
            <div class="col-md-8">
            </div>
            <div class="col-md-4">
                <!-- Social Media -->
                <ul class="social">
                    <li>
                        <a href="#" class="Facebook">
                            <i class="ion-social-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Twitter">
                            <i class="ion-social-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Linkedin">
                            <i class="ion-social-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="Google Plus">
                            <i class="ion-social-googleplus"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer> <!-- /#footer -->

<div id="chatModal" class="modal fade" role="dialog">
  <div class="modal-dialog modblue">

    <!-- Modal content-->
    <div class="modal-content myspecs">
      <div class="modal-header myspecs modblue">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"> <i class="fa fa-rss live"></i> <img src="{{asset('timer/images/author/author.jpg')}}" width="40" height="40" class="img-rounded"> {{ $user_profile->profile->full_name }}</h4>
      </div>
      <div class="modal-body" id="chat">
        
      </div>
      <div class="modal-footer" id="last">
        <form method="get" id="chatForm" action="{{route('live-chat')}}">
          <textarea class="form-control" placeholder="Message" name="textbody" id="tbody"></textarea>
          <button type="submit" class="btn modblue chat"><i class="fa fa-paper-plane-o"></i></button>
          <input type="hidden" value="{{Session::token()}}" name="_token">
          <input type="hidden" name="domain" id="domain" value="{{URL::to('/')}}">
          <input type="hidden" name="user_id" value="{{$user_profile->id}}">
        </form>
      </div>
    </div>

  </div>
</div>


<script id="sendTemplate" type="text/x-jquery-tmpl">
    <div class="well well-large alert alert-info alert-dismissable ${style}">
        <div class="alignl"><i class="fa fa-user"> ${time.date}</i> <a href="#" data-dismiss="alert" aria-label="close"> <i class="fa fa-times red"></i></a></div>
          ${message}
      </div>
  </script>

  <script id="recieveTemplate" type="text/x-jquery-tmpl">
     <div class="well well-large alert alert-info alert-dismissable btlr">
        <div> <a href="#" data-dismiss="alert" aria-label="close"> <i class="fa fa-times red"></i></a> | <i class="fa fa-user"> | ${time} |</i></div>
         ${message}
        </div>
  </script>



<div id="commentModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modblue">

    <!-- Modal content-->
    <div class="modal-content myspecs">
      <div class="modal-header myspecs modblue">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"><i class="fa fa-comment-o"></i> <a href="#" style="color: white;"> View all Comments(8)</a></h4>
      </div>
      <div class="modal-body">
        <ul class="comments_here">
            
        </ul>
      </div>
      <div class="modal-footer">
        {{-- <form>
          <textarea class="form-control" placeholder="Message"></textarea><br>
          <button type="button" class="btn modblue chat" data-dismiss="modal"><i class="fa fa-times"></i></button>
          <button type="button" class="btn modblue chat"><i class="fa fa-paper-plane-o"></i></button>
        </form> --}}

        <form method="get" id="commentForm">
          <textarea class="form-control" placeholder="Message" name="textbody" id="textbody"></textarea>
          <button type="submit" class="btn modblue chat"><i class="fa fa-paper-plane-o"></i></button>
          <input type="hidden" value="{{Session::token()}}" name="_token">
          {{-- <input type="hidden" name="post_id" value="{{$post->id}}"> --}}
        </form>

      </div>
    </div>

  </div>
</div>

<a href="{{URL::to('/')}}" id="friendship"></a>

<script id="commentTemplate" type="text/x-jquery-tmpl">
<li>
    <strong>${user} | ${created}</strong>
</li>
</script>



<script id="followTemplate" type="text/x-jquery-tmpl">
    
    <a href="">Youre Following</a>
    <a href="${follow}" class="${clas}" id="${iden}">
    <span class="badge">${followORnot} <i class="fa fa-user-${icon}"></i></span>
    </a>
    <span class="badge">${yesORno}</span>
    
</script>



{{-- THIS SCRIPT WILL MAKE THE CHAT MODAL APPEAR IF THE AUTH::USER WANTS TO MESSAGE THE USER GIVEN IN AS THE ROUTE ARGUMANET --}}
<script type="text/javascript">
    $(document).ready(function(){
        // make ajax call and get the messages
        //put display the messages on the modal
        var userID = {{ Auth::user()->id == $user_profile->id ? 1 : 0 }};
        var isMessage = {{ $message }} ;
        if(isMessage == 1 && userID == 0){
            $.get($('#chatFriend').attr('href'), function(result){
                console.log(result)
                $( "#sendTemplate" ).tmpl( result ).appendTo( "#chat" )
                $('#chatModal').modal({
                    backdrop: 'static' ,
                    keyboard: false
                });
            })
            
        }

        
    });
</script>
<script src="{{asset('js/jquery.tmpl.js')}}"></script>
<script src="{{asset('js/sweetalert2.js')}}"></script>
<script src="{{asset('js/profile.js')}}"></script>
<script type="text/javascript">
    NProgress.start();
     NProgress.inc()
    NProgress.done();
</script>
                </body>
            </html>
        </html>