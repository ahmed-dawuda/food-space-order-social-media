<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}">
        <title>Timeline | </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="{{asset('timer/css/bootstrap.min.css')}}">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{asset('timer/css/ionicons.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{asset('timer/css/animate.css')}}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{asset('timer/css/slider.css')}}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{asset('timer/css/owl.carousel.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/owl.theme.css')}}">

        <link rel="stylesheet" href="{{asset('timer/css/jquery.fancybox.css')}}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{asset('timer/css/main.css')}}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{asset('timer/css/responsive.css')}}">

        <link rel="stylesheet" type="text/css" href="{{asset('zentro/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/nprogress.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.css')}}">

        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="{{asset('timer/js/vendor/modernizr-2.6.2.min.js')}}"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="{{asset('timer/js/owl.carousel.min.js')}}"></script>
        <!-- bootstrap js -->

        <script src="{{asset('timer/js/bootstrap.min.js')}}"></script>
        <!-- wow js -->
        <script src="{{asset('timer/js/wow.min.js')}}"></script>
        <!-- slider js -->
        <script src="{{asset('timer/js/slider.js')}}"></script>

        <script src="{{asset('timer/js/jquery.fancybox.js')}}"></script>
        <!-- template main js -->
        <script src="{{asset('timer/js/main.js')}}"></script>
        <script src="{{asset('js/nprogress.js')}}"></script>

        <style type="text/css">
            .profile-nav{
                background: white;
                box-shadow: 0px 0px 6px;
                /*position: sticky;*/
            }
            .centa{
                text-align: center;
                padding: 20px;
                color: white;
                box-shadow: 5px;
            }

            .green{
                background: #3CB371;
            }

            .black{
                background: #000000;
                color: white;
            }
            .red{
                color: #ff9999;
              }
            .redback{
                background: #ff9999;
            }
            .modblue{
                  background: #00BFFF;
                  color: white;
                  /*border: #00BFFF;*/
                }
            .myspecs{
                border-radius: 0px;
                vertical-align: center;
              }
              .live{
                color: #7FFF00;
              }
              .alignl{
                text-align: right;
              }
              .btlr{
                border-radius: 50px;
                border-top-left-radius: 0px;
              }
              .btrr{
                border-radius: 50px;
                border-top-right-radius: 0px;
              }
              .pl{
                padding-left: 100px; 
              }
            .bordra{
                border-radius: 0px;
            }  
            select,option{
                color: #00BFFF;
                border-radius: 5px;
                padding: 6px;
            }

            .modal .modal-body {
                max-height: 420px;
                overflow-y: auto;
            }

            .dropdown-menu > li{
                border-radius: 0px;  
                padding: 10px;
            }
            #messages li{
                padding: 7px;
                margin-top: 2px;
                color: white;
            }

            .offline{
                background: #f27171;
            }
            .online{
                background: #3CB371;
            }
            .underline{
                border-bottom: 1px dashed white;
            }

            .onlineform{
                background: #3CB371;
                border: 1px black;
                width: 100%;
                height: 100%;
                color: white;
            }
            .formHolder{
                margin-bottom: 2px;
            }

            #notifications li{
                padding: 15px;
                margin-top: 2px;
                color: white;
                /*background: #00BFFF;*/
                /*border-radius: 5px;*/
            }
            .scrollable{
                overflow-y:auto;
                height: 840px;
                width: 100%;
            }

        </style>
    </head>
    <body>

<div class="container-fluid profile-nav navbar-fixed-top">
    <!-- <div class="navbar"> -->
        <div class="row">
            <div class="col-sm-5"></div>

            <a href="{{ route('customer-home') }}" title="Go Home" id="home">
                <div class="col-sm-1 centa green"><i class="fa fa-home"></i> Home</div>
            </a>
       
            <a href="{{route('customer-update-profile',['edit'])}}" title="Edit your profile">
                <div class="col-sm-1 centa modblue"><i class="fa fa-edit"></i> Edit</div>
            </a>
        
             
            <div class="col-sm-5"></div>
        </div>
    <!-- </div> -->
</div>


<section id="blog-full-width">


<div class="container">
    <div class="row" id="mainBody">

<!-- SIDE BAR -->

        <div class="col-md-4" >
        <!-- <div class="affix"> -->
            <div class="sidebar">
            
                    

                    <!-- <div class="categories widget"> -->
                        <p>Chat(s)</p>
                        <ul id="messages" class="scrollable">
                            <a href="#message1" class="mes">
                                <li class="online">
                                    <p class="underline">Ahmed Dawuda <i class="fa fa-arrow-circle-right"></i> Tue 8:25 pm</p>
                                    <p>Hello how are you</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>

                            <a href="#message2" class="mes">
                                <li class="online">
                                    <p class="underline">Kofi Operon <i class="fa fa-arrow-circle-right"></i> Mon 10:03 am</p>
                                    <p>Hahahahaha, I thought you knew</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>


                            <a href="#message3" class="mes">
                                <li class="offline">
                                    <p class="underline">Justice Arthur <i class="fa fa-arrow-circle-right"></i> Tue 11:23 pm</p>
                                    <p>I'll visit you this evening. Hope you will be around?</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>


                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>
                        </ul>

                    <!-- </div> -->
                    
                    
                </div>

            <!-- </div> -->
            </div>

<!-- END SIDE BAR -->

<!-- BLOG BODY -->
            <div class="col-md-7 container">
            <br><br><br><br><p>chat here</p>
            <div class="scrollable">

                <ul id="messages">
                            <a href="#message1" class="mes">
                                <li class="online">
                                    <p class="underline">Ahmed Dawuda <i class="fa fa-arrow-circle-right"></i> Tue 8:25 pm</p>
                                    <p>Hello how are you</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>

                            <a href="#message2" class="mes">
                                <li class="online">
                                    <p class="underline">Kofi Operon <i class="fa fa-arrow-circle-right"></i> Mon 10:03 am</p>
                                    <p>Hahahahaha, I thought you knew</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>


                            <a href="#message3" class="mes">
                                <li class="offline">
                                    <p class="underline">Justice Arthur <i class="fa fa-arrow-circle-right"></i> Tue 11:23 pm</p>
                                    <p>I'll visit you this evening. Hope you will be around?</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>


                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>

                            <a href="#message4" class="mes">
                                <li class="online">
                                    <p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
                                    <p>I love you dear.. how are you?</p>
                                </li>
                            </a>
                            <p class="formHolder online form-group"></p>




                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>



                            <a href="#message5" class="mes">
                                <li class="offline">
                                    <p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
                                    <p>Ebi steez thick.</p>
                                </li>
                            </a>
                            <p class="formHolder offline form-group"></p>
                        </ul>

                        
            </div>
            
            
            </div>
            
            <div class="col-md-1"></div>
<!-- END BLOG -->

        </div>


    </section>
  






<script src="{{asset('js/jquery.tmpl.js')}}"></script>
<script src="{{asset('js/sweetalert2.js')}}"></script>
<!-- <script src="{{asset('js/profile.js')}}"></script> -->
<script type="text/javascript">
    NProgress.start();
     NProgress.inc()
    NProgress.done();
</script>
                </body>
            </html>
        </html>