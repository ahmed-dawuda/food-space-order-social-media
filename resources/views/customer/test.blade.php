<!DOCTYPE html>
<html lang="en">
<head>	

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>
		Home | {{ ucfirst(Auth::user()->username) }}
	</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link id="bootstrap-style" href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
	<link id="base-style" href="{{asset('css/style.css')}}" rel="stylesheet">
	<link id="base-style-responsive" href="{{asset('css/style-responsive.css')}}" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/nprogress.css')}}">

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
	<!-- end: Favicon -->
	
	<style type="text/css">
	.span10 a:hover{
		padding: 10px;
		border-top-right-radius: 20px;
	}
	
	.myspecs{
		/*height: 100%;*/
		/*width: 980px;*/
		border-radius: 0px;
		/*margin-right: 10px;*/
		vertical-align: center;
	}

	/*.box{
		box-shadow: 0 1px 3px;
	}*/
	.star{
		color: #CCCC00;
	}
	.hov:hover{
		background: #43B5AD;
	}
	</style>	
		
		
</head>

<body>

		<!-- start: Header -->

	<!-- start: Header -->
	
		<div class="container-fluid-full" id="test-links">
			<div class="row-fluid">
				{{-- <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="#">Brand</a>
</div> --}}
			<div class="box">
					<div class="box-header">
						<h2>
						<i class="halflings-icon white food"></i><span class="break"></span>Welcome to foodbook.com
						<span class="break"></span><span style="color: white;"><i class="icon-home"></i> Quick Access Panel</span>
						
						</h2>
					</div>
			</div>
			
				<div class="span1"></div>

				<div class="span10">
					<div class="row-fluid">
					
					<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
						<div class="boxchart">5,6,7,2,0,4,2,4,8,2,3,3,2</div>
						<div class="number">854<i class="icon-arrow-up"></i></div>
						<div class="title">Visits</div>
						{{-- <div class="footer">
							<a href="#"> Read full report</a>
						</div>	 --}}
					</div>

					<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
						<div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
						<div class="number">123<i class="icon-arrow-up"></i></div>
						<div class="title">Sales</div>
						{{-- <div class="footer">
							<a href="#"> Read full report</a>
						</div> --}}
					</div>

					<div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
						<div class="boxchart">5,6,7,2,0,-4,-2,4,8,2,3,3,2</div>
						<div class="number">982<i class="icon-arrow-up"></i></div>
						<div class="title">All Orders</div>
						{{-- <div class="footer">
							<a href="#"> Read full report</a>
						</div> --}}
					</div>

					<div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
						<div class="boxchart">7,2,2,2,1,-4,-2,4,8,,0,3,3,5</div>
						<div class="number">678<i class="icon-arrow-down"></i></div>
						<div class="title">Visits</div>
						{{-- <div class="footer">
							<a href="#"> Read full report</a>
						</div> --}}
					</div>	
					
				</div>	
{{-- it starts here --}}
		
				<div class="row-fluid">	

					<a class="quick-button metro yellow span4" href="#" id="fav">
						<i class="icon-thumbs-up"></i>
						<p>My Favourite Restaurants</p>
						<span class="badge blue">237</span>
					</a>

					<a class="quick-button metro red span4" href="#" id="top">
						<i class="icon-rss"></i>
						<p>Top Restaurants</p>
						<span class="badge black">46</span>
					</a>

					<a class="quick-button metro blue span4" href="#" data-toggle="modal" data-target="#searchModal" id="search">
						<i class="icon-zoom-in"></i>
						<p>Search</p>
						<!-- <span class="badge">13</span> -->
					</a>

				
					<div class="clearfix"></div>
									
				</div><!--/row--><br><br>

				
				<div class="row-fluid">	

					<a class="quick-button metro yellow span3" href="#" id="friends">
						<i class="icon-group"></i>
						<p>Friends</p>
						<span class="badge pink">237</span>
					</a>
					<!-- <a class="quick-button metro red span2">
						<i class="icon-road"></i>
						<p>Competitors</p>
						<span class="badge">46</span>
					</a> -->
					<a class="quick-button metro blue span2" href="#" id="orders">
						<i class="icon-shopping-cart"></i>
						<p> My Orders</p>
						<span class="badge green">13</span>
					</a>
					<a class="quick-button metro green span3" href="#" id="trends">
						<i class="icon-refresh"></i>
						<p>Trends</p>
					</a>
					<a class="quick-button metro pink span4" id="suggestions">
						<i class="icon-envelope"></i>
						<p>Make Suggestions</p>
						<span class="badge yellow">88</span>
					</a>
					<!-- <a class="quick-button metro black span2">
						<i class="icon-calendar"></i>
						<p>Calendar</p>
					</a> -->
					
					<div class="clearfix"></div>
									
				</div><!--/row--><br><br>

				<div class="row-fluid">	

					<a class="quick-button metro blue span3" href="#" id="posts">
						<i class="icon-comment"></i>
						<p>Posts</p>
						<span class="badge green">{{ Auth::user()->posts->count() }}</span>
					</a>
					<a class="quick-button metro green span3" href="#" id="messages">
						<i class="icon-envelope"></i>
						<p>Messages</p>
						<span class="badge red">{{Auth::user()->messages->count()}}</span>
					</a>
					<a class="quick-button metro pink span3" id="notifications">
						<i class="icon-bell"></i>
						<p>Notifications</p>
						<span class="badge yellow">{{Auth::user()->notifications->count()}}</span>
					</a>
					<a class="quick-button metro black span3" href="{{ route('profile',[Auth::user()->id]) }}" id="timeline">
						<i class="icon-user"></i>
						<p>Timeline</p>
						<span class="badge {{ Auth::user()->profile_updated? 'green':'red' }}">{{ Auth::user()->profile_updated ? 'Updated':'Required' }}</span>
					</a>
					
					<div class="clearfix"></div>
									
				</div><!--/row--><br><br>

				<div class="row-fluid">	

					<a class="quick-button metro yellow span2" href="#" id="faq">
						<i class="icon-lightbulb"></i>
						<p>Frequently Asked Questions</p>
						<span class="badge pink">237</span>
					</a>
					
					<a class="quick-button metro green span6" href="#" id="foods">
						<i class="icon-barcode"></i>
						<p>Foods</p>
						<span class="badge red">32</span>
					</a>
					<a class="quick-button metro pink span2" id="about">
						<i class="icon-road"></i>
						<p>About foodbook</p>
						<span class="badge yellow">88</span>
					</a>
					<a class="quick-button metro black span2" href="{{ route('user-logout') }}" id="logout">
						<i class="icon-lock"></i>
						<p>logout</p>
					</a>
					
					<div class="clearfix"></div>
									
				</div><!--/row-->

				</div>

				<div class="span1"></div>

			</div>
		</div><!--/.fluid-container-->
	
	<div class="clearfix"></div>
	
<div id="searchModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header modal-header" data-original-title>
            <h2><i class="icon-zoom-in white"></i><span class="break"></span>Filter Foods/ Restaurants</h2>
          </div>
      {{-- </div> --}}
      <div class="box-content modal-body">
            <form class="form-horizontal" id="searchForm" action="{{URL::to('/')}}" method="post">

              <fieldset>
                <div class="control-group">
                <label class="control-label" for="selectError">Search By:</label>
                <div class="controls">
                  <select id="selectError" data-rel="chosen">
                  <option value="1">Restaurant</option>
                  <option value="2">Foods</option>
                  <option value="3">Popular Foods</option>
                  <option value="4">Popular Restaurants</option>
                  <option value="5">Top Restaurants</option>
                  <option value="6">Top Foods</option>
                  </select>
                </div>
                </div>

                  <div class="control-group">
                <label class="control-label" for="selectError">Search By Food Type:</label>
                <div class="controls">
                  <select id="selectError1" data-rel="chosen">
                  <option value="0">Any</option>
                  <option value="1">Break Fast</option>
                  <option value="2">Lunch</option>
                  <option value="3">Supper</option>
                  </select>
                </div>
                </div>


                 <div class="control-group">
                <label class="control-label" for="prependedInput">Search By Price</label>
                <div class="controls">
                  <div class="input-prepend">
                  <span class="add-on">Ghc</span><input id="prependedInput" size="16" type="text" name="price" placeholder="20.25" value="Any">
                  </div>
                <input type="hidden" name="_token" value="{{Session::token()}}">
                  <input type="checkbox" id="inlineCheckbox1" value="yes">Above
                  
                </div>
                </div>

              </fieldset>
              </form>
          
          </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Search</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
	<div id="testModal" class="modal fade myspecs" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content" id="modal_content">

	        <div class="box-header modal-header" data-original-title id="headerColor">
	            <h2>Header</h2>
	          </div>
	            <div class="box-content modal-body">
	                 <ul class="dashboard-list">
             
            		</ul>
	            </div>
	      <div class="modal-footer">
	      {{-- <button type="submit" class="btn btn-primary">Search</button> --}}
	        <button type="button" class="btn btn-default" data-dismiss="modal" id="buttonColor">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="postModal" class="modal fade myspecs" role="dialog" >
	  <div class="modal-dialog">
	  <form style="margin: 0px;" action="{{route('submit-post')}}" method="post" id="post_form" enctype="multipart/form-data">
	    <!-- Modal content-->
	    <div class="modal-content" id="modal_content">

	        <div class="box-header modal-header blue" data-original-title>
	            <h2>Post Something</h2>
	          </div>
	            <div class="box-content modal-body">
	                <div style="margin: 0px; width: 100%; display: none;">
	                	<img src="" id="postPhoto" style="width: 100%;">
	                </div>
             		<div style="margin: 0px;">
             			<textarea id="text_input" name="post_text"></textarea>
             		</div>
            		
	            </div>
	       </div>
	      <div class="modal-footer" style="text-align: left;">
		    	Photo: <input class="btn btn-primary"  id="file-input" type="file" name="post_photo" style="float: left;" >
		    	
			<div style="text-align: right;">
				      <button type="submit" class="btn btn-primary" style="border-radius: 20px;"> Post</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal" style="border-radius: 20px;">Close</button>
				        <input type="hidden" name="_token" value="{{Session::token()}}">
			</div>
			</div>	        
	        </form>
	      </div>
	    </div>

	  </div>
	</div>



	@include('includes.test_tmpls')


	<!-- start: JavaScript-->
<a href="{{URL::to('/')}}" id="domain"></a>
		<script src="{{asset('js/jquery-1.9.1.min.js')}}"></script>
	<script src="{{asset('js/jquery-migrate-1.0.0.min.js')}}"></script>
	
		<script src="{{asset('js/jquery-ui-1.10.0.custom.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.ui.touch-punch.js')}}"></script>
	
		<script src="{{asset('js/modernizr.js')}}"></script>
	
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.cookie.js')}}"></script>
	
		<script src="{{asset('js/fullcalendar.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

		<script src="{{asset('js/excanvas.js')}}"></script>
	<script src="{{asset('js/jquery.flot.js')}}"></script>
	<script src="{{asset('js/jquery.flot.pie.js')}}"></script>
	<script src="{{asset('js/jquery.flot.stack.js')}}"></script>
	<script src="{{asset('js/jquery.flot.resize.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.chosen.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.uniform.min.js')}}"></script>
		
		<script src="{{asset('js/jquery.cleditor.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.noty.js')}}"></script>
	
		<script src="{{asset('js/jquery.elfinder.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.raty.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.iphone.toggle.js')}}"></script>
	
		<script src="{{asset('js/jquery.uploadify-3.1.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.gritter.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.imagesloaded.js')}}"></script>
	
		<script src="{{asset('js/jquery.masonry.min.js')}}"></script>
	
		<script src="{{asset('js/jquery.knob.modified.js')}}"></script>
	
		<script src="{{asset('js/jquery.sparkline.min.js')}}"></script>
	
		<script src="{{asset('js/counter.js')}}"></script>
	
		<script src="{{asset('js/retina.js')}}"></script>

		<script src="{{asset('js/custom.js')}}"></script>
		<script src="{{asset('js/jquery.tmpl.js')}}"></script>
		<script src="{{asset('js/nprogress.js')}}"></script>
		<script src="{{ asset('js/test.js') }}"></script>
	<!-- end: JavaScript-->
<script type="text/javascript">
	NProgress.start();
	 NProgress.inc()
	NProgress.done();
</script>
</body>
</html>
