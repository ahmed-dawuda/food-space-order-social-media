<br><br><br>
Mark Notifications to delete
<form id="mark_form" action="{{route('clear-notif')}}" method="get">
<ul id="notifications">
		<li style="background: #00BFFF;"><input type="checkbox" value="1" name="not1">
		<i class="fa fa-flag"></i> 
			<a href="#" >Ahmed Dawuda</a> is following You.  <a href="#"><i class="fa fa-street-view"></i> Follow Back</a>
		</li>

		<li style="background: #3CB371;"><input type="checkbox" value="2" name="not2">
		<i class="fa fa-flag"></i> 
			<a href="#" >Akuzi</a> accepted your suggestion 	
		</li>

		<li style="background: #f9a4a9;"><input type="checkbox" value="3" name="not3">
		<i class="fa fa-flag"></i> 
			<a href="#" >Brunei Cafetaria</a> rejected your order because the time you gave wouldn't be possible. 	
		</li>

		<li style="background: #f9a4a9;"><input type="checkbox" value="4" name="not4">
		<i class="fa fa-flag"></i> 
			<a href="#" >Maa U</a> Rejected your order with ID: 5223242 because your location is unknown. sorry
		</li>
		<li style="color: blue;">
			<a href="#" id="clearSelected"><i class="fa fa-trash" style="color: red; margin-top: 5px;"></i> Clear Selected</a>	
		</li>
		<input type="hidden" name="_token" value="{{Session::token()}}">
</ul>
<form>
