<br><br><br>
Inbox <a href="{{route('chat_screen')}}" style="color: #00BFFF;"><i class="fa fa-long-arrow-right"> Goto Chat Screen</i></a>
<ul id="messages">
	<a href="#message1" class="mes">
		<li class="online">
			<p class="underline">Ahmed Dawuda <i class="fa fa-arrow-circle-right"></i> Tue 8:25 pm</p>
			<p>Hello how are you</p>
		</li>
	</a>
	<p class="formHolder online form-group"></p>

	<a href="#message2" class="mes">
		<li class="online">
			<p class="underline">Kofi Operon <i class="fa fa-arrow-circle-right"></i> Mon 10:03 am</p>
			<p>Hahahahaha, I thought you knew</p>
		</li>
	</a>
	<p class="formHolder online form-group"></p>


	<a href="#message3" class="mes">
		<li class="offline">
			<p class="underline">Justice Arthur <i class="fa fa-arrow-circle-right"></i> Tue 11:23 pm</p>
			<p>I'll visit you this evening. Hope you will be around?</p>
		</li>
	</a>
	<p class="formHolder offline form-group"></p>


	<a href="#message4" class="mes">
		<li class="online">
			<p class="underline">Solomon Dankwa <i class="fa fa-arrow-circle-right"></i> Fri 9:28 am</p>
			<p>I love you dear.. how are you?</p>
		</li>
	</a>
	<p class="formHolder online form-group"></p>


	<a href="#message5" class="mes">
		<li class="offline">
			<p class="underline">Kingsley Tagoe <i class="fa fa-arrow-circle-right"></i> Sat 12:17 pm</p>
			<p>Ebi steez thick.</p>
		</li>
	</a>
	<p class="formHolder offline form-group"></p>
</ul>
