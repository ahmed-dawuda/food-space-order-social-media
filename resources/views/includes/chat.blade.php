<div id="chatModal" class="modal fade" role="dialog">
  <div class="modal-dialog modblue">

    <!-- Modal content-->
    <div class="modal-content myspecs">
      <div class="modal-header myspecs modblue">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"> <i class="fa fa-rss live"></i> Live Chat With {{ ucfirst($restaurant->name) }}</h4>
      </div>
      <div class="modal-body" id="chat">
        
      </div>
      <h5 id="last">here</h5>
      <div class="modal-footer">
        <form method="get" id="chatForm">
          <textarea class="form-control" placeholder="Message" name="textbody" id="textbody"></textarea>
          <button type="submit" class="btn modblue chat"><i class="fa fa-paper-plane-o"></i></button>
          <input type="hidden" value="{{Session::token()}}" name="_token">
          <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
        </form>
      </div>
    </div>

  </div>
</div>


<div id="orderModal" class="modal fade" role="dialog">
  <div class="modal-dialog modblue">

    <!-- Modal content-->
    <div class="modal-content myspecs">
      <div class="modal-header myspecs modblue">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"> <i class="fa  fa-thumbs-o-up"></i> Place Order - You will only pay on delivery</h4>
      </div>
      <div class="modal-body">
          <form id="orderForm" method="post">
          <input type="hidden" value="{{ $restaurant->id }}" name="restaurant_id" id="rest_id">
          <input type="hidden" name="food_id" id="food_id">
              <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">Name</span>
                     <input type="text" name="name" class="form-control" disabled="disabled" id="name"> 
              </div><br>

              <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">Type</span>
                     <input type="text" name="type" class="form-control" disabled="disabled" id="type"> 
              </div><br>

              <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">Time</span>
                     <input type="text" name="current_time" class="form-control" disabled="disabled" id="current_time"> 
              </div><br>

              <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">Quantity</span>
                     <input type="number" name="quantity" class="form-control" placeholder="Enter The Quantity" id="quantity"> 
              </div><br>

              {{-- <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">Price</span>
                     <input type="number" name="price" class="form-control" placeholder="Enter the price" id="price"> 
              </div> --}}
              <div class="input-group input-group-sm">
                        <span class="input-group-addon" id="sizing-addon3">Select Price GHS</span>
                     {{-- <input type="text" name="gender" class="form-control" placeholder="gender"> --}}
                     <select id="price" name="price">
                         {{-- <option value="1">Male</option>
                         <option value="2">Female</option> --}}
                     </select>
              </div>
              <br>

              <div class="input-group input-group-sm">
                      <span class="input-group-addon" id="sizing-addon3">In How Many Hours Time?</span>
                     <input type="text" name="delivery_time" class="form-control" placeholder="e.g. 3:25" id="delivery_time"> 
              </div><br>

              <input type="hidden" value="{{Session::token()}}" name="_token">
          <button type="submit" class="btn modblue chat"><i class="fa fa-shopping-cart fa-2x" ></i></button>
        </form>
      </div>
    </div>

  </div>
</div>

<script id="sendTemplate" type="text/x-jquery-tmpl">
    <div class="well well-large alert alert-info alert-dismissable btrr">
        <div class="alignl"><i class="fa fa-user"> ${time}</i> <a href="#" data-dismiss="alert" aria-label="close"> <i class="fa fa-times red"></i></a></div>
          ${message}
      </div>
  </script>

  <script id="recieveTemplate" type="text/x-jquery-tmpl">
     <div class="well well-large alert alert-info alert-dismissable btlr">
        <div> <a href="#" data-dismiss="alert" aria-label="close"> <i class="fa fa-times red"></i></a>{{ ucfirst($restaurant->name) }} | <i class="fa fa-cutlery"> | ${time} |</i></div>
         ${message}
        </div>
  </script>
  <a href="{{URL::to('/')}}" id="domain"></a>

  <div id="tagModal" class="modal fade" role="dialog">
  <div class="modal-dialog modblue">

    <!-- Modal content-->
    <div class="modal-content myspecs">
      <div class="modal-header myspecs modblue">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title">Tag friends with {{ucfirst($restaurant->name)}}</h4>
      </div>
      <div class="modal-body">
         <form method="get" id="multiselectForm">
         <div class="input-group input-group-sm">
          <span class="input-group-addon" id="sizing-addon3">Type Friend's username</span>
          <input type="text" name="usernames" class="form-control" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput"> 
        </div>
        </form>
      </div>
      <div class="modal-footer">
       
      </div>
    </div>

  </div>
</div>