@foreach($posts as $post)
                <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                    <div class="blog-post-image">
                        <a href="#"><img class="img-responsive" src="{{asset('timer/images/blog/post-1.jpg')}}" alt="" /></a>
                    </div>
                    <div class="blog-content">
                        <h2 class="blogpost-title">
                        <a href="#">{{ ucfirst($post->title) }}</a>
                        </h2>
                       
                        <div class="blog-meta">
                            <span>{{$post->created_at?$post->created_at:'7th Jul 20:21'}}</span>
                            <span> <i class="fa fa-user"></i> 
                            <a href="{{route('profile',[Auth::user()->id])}}" style="color: #00BFFF;  border-radius: 10px;padding: 5px;"> 
                                    You
                             </a></span>
                                <span>
                                <a href="{{route('customer-delete-post',[$post->id])}}" class="deletePost" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                Delete <i class="fa fa-times"></i></a></span>

                            <span><a href="">Customer</a></span>
                        @if(Auth::user()->voted($post->id))
                            <span>
                                <a href="{{route('vote',[$post->id,'votedown'])}}" class="votedown" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                        VoteDown <i class="fa fa-level-down"></i>
                                </a>
                                <i class="post_votes">{{Auth::user()->votes($post->id)}}</i>
                            </span>
                        @else  
                            <span>
                                <a href="{{route('vote',[$post->id])}}" class="voteup" style="color: #00BFFF;  border-radius: 10px;padding: 5px;">
                                        VoteUp <i class="fa fa-level-up"></i>
                                </a>
                                <i class="post_votes">{{Auth::user()->votes($post->id)}}</i>
                            </span>
                        @endif

                        </div>
                        <p>
                            {{ ucfirst($post->body) }} 
                        </p>
                       {{--  <a href="#" class="btn btn-dafault btn-details ">Continue Reading</a> --}}
                       <a href="{{URL::to('/').'/post/'.$post->id}}" style="color: #00BFFF; border-radius: 10px;padding: 5px;" class="comment">
                            Comment <i class="fa fa-comment-o"></i></a>
                    </div>
                </article>
@endforeach