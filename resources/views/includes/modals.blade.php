<!-- Modal fav resta-->
<div id="myOrdersModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header modal-header" data-original-title>
            <h2><i class="icon-shopping-cart white"></i><span class="break"></span>All Orders</h2>
          </div>
      {{-- </div> --}}
     <div class="box-content modal-body">
            <ul class="dashboard-list">
              <li>
                <strong>Restuarant:</strong> <a href="#">ID: 2345, Name: Maa U</a><br>
                <strong>Order ID:</strong> <a href="#">2345</a><br>
                <strong>Order Type:</strong> <a href="#">Breakfast</a><br>
                <strong>Time Ordered:</strong> Jul 25, 2012 11:09<br>
                <strong>Delivery Time:</strong> Jul 25, 2012 11:09<br>
                <strong>Status:</strong> <span class="label label-success">Delivered</span><br><br>
                <span>
                <a class="btn btn-success" href="#">
                    <i class="halflings-icon white ok"></i>                                            
                  </a>
                  <a class="btn btn-info" href="#">
                    <i class="halflings-icon white question-sign"></i>                                            
                  </a>
                  <a class="btn btn-danger" href="#">
                    <i class="halflings-icon white trash"></i> 
                  </a>
                  </span>
              </li>


              <li>
                <strong>Restuarant:</strong> <a href="#">ID: 2345, Name: Maa U</a><br>
                <strong>Order ID:</strong> <a href="#">2345</a><br>
                <strong>Order Type:</strong> <a href="#">Breakfast</a><br>
                <strong>Time Ordered:</strong> Jul 25, 2012 11:09<br>
                <strong>Delivery Time:</strong> Jul 25, 2012 11:09<br>
                <strong>Status:</strong> <span class="label label-success">Delivered</span><br><br>
                <span>
                <a class="btn btn-success" href="#">
                    <i class="halflings-icon white ok"></i>                                            
                  </a>
                  <a class="btn btn-info" href="#">
                    <i class="halflings-icon white question-sign"></i>                                            
                  </a>
                  <a class="btn btn-danger" href="#">
                    <i class="halflings-icon white trash"></i> 
                  </a>
                  </span>
              </li>

              <li>
                <strong>Restuarant:</strong> <a href="#">ID: 2345, Name: Maa U</a><br>
                <strong>Order ID:</strong> <a href="#">2345</a><br>
                <strong>Order Type:</strong> <a href="#">Breakfast</a><br>
                <strong>Time Ordered:</strong> Jul 25, 2012 11:09<br>
                <strong>Delivery Time:</strong> Jul 25, 2012 11:09<br>
                <strong>Status:</strong> <span class="label label-success">Delivered</span><br><br>
                <span>
                <a class="btn btn-success" href="#">
                    <i class="halflings-icon white ok"></i>                                            
                  </a>
                  <a class="btn btn-info" href="#">
                    <i class="halflings-icon white question-sign"></i>                                            
                  </a>
                  <a class="btn btn-danger" href="#">
                    <i class="halflings-icon white trash"></i> 
                  </a>
                  </span>
              </li>

            </ul>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="searchModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header modal-header" data-original-title>
            <h2><i class="icon-zoom-in white"></i><span class="break"></span>Filter Foods/ Restaurants</h2>
          </div>
      {{-- </div> --}}
      <div class="box-content modal-body">
            <form class="form-horizontal">

              <fieldset>
                <div class="control-group">
                <label class="control-label" for="selectError">Search By:</label>
                <div class="controls">
                  <select id="selectError" data-rel="chosen">
                  <option value="1">Restaurant</option>
                  <option value="2">Foods</option>
                  <option value="3">Popular Foods</option>
                  <option value="4">Popular Restaurants</option>
                  <option value="5">Top Restaurants</option>
                  <option value="6">Top Foods</option>
                  </select>
                </div>
                </div>

                  <div class="control-group">
                <label class="control-label" for="selectError">Search By Food Type:</label>
                <div class="controls">
                  <select id="selectError1" data-rel="chosen">
                  <option value="0">Any</option>
                  <option value="1">Break Fast</option>
                  <option value="2">Lunch</option>
                  <option value="3">Supper</option>
                  </select>
                </div>
                </div>


                 <div class="control-group">
                <label class="control-label" for="prependedInput">Search By Price</label>
                <div class="controls">
                  <div class="input-prepend">
                  <span class="add-on">Ghc</span><input id="prependedInput" size="16" type="text" name="price" placeholder="20.25" value="Any">
                  </div>
                
                  <input type="checkbox" id="inlineCheckbox1" value="yes">Above
                  
                </div>
                </div>

              </fieldset>
              </form>
          
          </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Search</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="postsModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header modal-header" data-original-title>
            <h2><i class="icon-comments white"></i><span class="break"></span>Posts</h2>
          </div>
      {{-- </div> --}}
      <div class="modal-body">
        <p>List Of posts</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="friendsModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header yellow modal-header" data-original-title>
            <h2><i class="icon-group white"></i><span class="break"></span>All Friends</h2>
            
          </div>
      {{-- </div> --}}
      {{-- <div class="modal-body"> --}}
        <div class="box-content modal-body">
            <ul class="dashboard-list">
            
             {{--  <li>
                <a href="{{ route('profile',[1]) }}">
                  <img class="avatar" alt="" src="img/avatar.jpg">
                </a>
                <strong>Name:</strong> <a href="#">Dennis Ji</a><br>
                <strong>Since:</strong> Jul 25, 2012 11:09<br>
                <strong>Status:</strong> <span class="label label-important">Offline</span> 
                <a href="{{ route('profile',[1]) }}" style="margin: 0;"><i class="icon-envelope yellow"></i></a>                                 
              </li> --}}

            </ul>
          </div>
      {{-- </div> --}}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="favModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header yellow modal-header" data-original-title>
            <h2><i class="icon-group white"></i><span class="break"></span>Favourite Restaurants</h2>
          </div>
      {{-- </div> --}}
      {{-- <div class="modal-body"> --}}
        <div class="box-content modal-body">
            <ul class="dashboard-list">
            @foreach($restaurants as $restaurant)
              <li>
                <a href="{{ route('favourite-restaurant',[$restaurant->id]) }}">
                  <img class="avatar" alt="Dennis Ji" src="{{asset('hand.jpg')}}" style="margin-bottom: 20px;">
                </a>
                <strong>Name:</strong> <a href="#">{{ ucfirst($restaurant->name) }}</a><br>
                <strong>No of Orders you made:</strong> 29<br>
                <strong>Successfull Delivery:</strong> 19<br>
                <strong>Status:</strong> <span class="label label-success">Food Available</span>
                <a href="{{ route('favourite-restaurant',[$restaurant->id]) }}" title="Visit them" style="margin: 0;"><i class="icon-home yellow"></i></a>                                  
              </li>
            @endforeach
            
             {{--  <li>
                <a href="{{ route('favourite-restaurant',[1]) }}">
                  <img class="avatar" alt="" src="{{asset('img/avatar.jpg')}}" style="margin-bottom: 20px;">
                </a>
                <strong>Name:</strong> <a href="#">Brunei Cafetaria</a><br>
                <strong>No of Orders you made:</strong> 29<br>
                <strong>Successfull Delivery:</strong> 19<br>
                <strong>Status:</strong> <span class="label label-important">Food Not Available</span> 
                <a href="{{ route('favourite-restaurant',[1]) }}" title="Visit them" style="margin: 0;"><i class="icon-home yellow"></i></a>                                 
              </li> --}}

            </ul>
          </div>
      {{-- </div> --}}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="suggestionsModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header pink modal-header" data-original-title>
            <h2><i class="icon-group white"></i><span class="break"></span>Make Suggestions</h2>
          </div>
      {{-- </div> --}}
      {{-- <div class="modal-body"> --}}
        <div class="box-content modal-body">
            <ul class="dashboard-list">
              <li>
                <a href="#">
                  <img class="avatar" alt="Dennis Ji" src="{{asset('img/avatar.jpg')}}" style="margin-bottom: 20px;">
                </a>
                <strong>Name:</strong> <a href="#">Akuzi Restaurant</a><br>
                <strong>No of Orders you made:</strong> 29<br>
                <strong>Successfull Delivery:</strong> 19<br>
                <strong>Status:</strong> <span class="label label-success">Food Available</span>
                <a href="#" style="margin: 0;"><i class="icon-envelope yellow"></i></a>                                  
              </li>

              <li>
                <a href="#">
                  <img class="avatar" alt="" src="{{asset('img/avatar.jpg')}}" style="margin-bottom: 20px;">
                </a>
                <strong>Name:</strong> <a href="#">Brunei Cafetaria</a><br>
                <strong>No of Orders you made:</strong> 29<br>
                <strong>Successfull Delivery:</strong> 19<br>
                <strong>Status:</strong> <span class="label label-important">Food Not Available</span> 
                <a href="#" style="margin: 0;"><i class="icon-envelope yellow"></i></a>                                 
              </li>

            </ul>
          </div>
      {{-- </div> --}}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="messagesModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header green modal-header" data-original-title>
            <h2><i class="icon-envelope white"></i><span class="break"></span>Messages</h2>
            
          </div>
      {{-- </div> --}}
      {{-- <div class="modal-body">
        <p>List Of messages</p>
      </div> --}}
      <div class="box-content modal-body">
            <ul class="dashboard-list metro">

            <a href="#">
              <li class="green">
                {{-- <a href="#"> --}}
                  <img class="avatar" alt="Dennis Ji" src="img/avatar.jpg">
                {{-- </a> --}}
                <strong>Dennis Ji</strong><br>
                <span>Hello how are you doing? Check out for this meal</span><br>
                <strong>Status:</strong> Online             
              </li>
            </a>
          
            <a href="#">
              <li class="red">
                {{-- <a href="#"> --}}
                  <img class="avatar" alt="Dennis Ji" src="img/avatar.jpg">
                {{-- </a> --}}
                <strong>Dennis Ji</strong><br>
                <span>Hello how are you doing? Check out for this meal</span><br>
                <strong>Status:</strong> Offline             
              </li>
            </a>
            
            </ul>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="faqModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header yellow modal-header" data-original-title>
            <h2><i class="icon-envelope white"></i><span class="break"></span>Frequently Asked Questions</h2>
            
          </div>
      {{-- </div> --}}
      {{-- <div class="modal-body">
        <p>List Of messages</p>
      </div> --}}
      <div class="box-content modal-body">
            <ul class="dashboard-list metro">

            <a href="#">
              <li class="green">
                {{-- <a href="#"> --}}
                  {{-- <img class="avatar" alt="Dennis Ji" src="img/avatar.jpg"> --}}
                {{-- </a> --}}
                <strong>How do make orders</strong><br>
                <span>Hello how are you doing? Check out for this meal</span><br>             
              </li>
            </a>
          
            <a href="#">
              <li class="red">
                {{-- <a href="#"> --}}
                  {{-- <img class="avatar" alt="Dennis Ji" src="img/avatar.jpg"> --}}
                {{-- </a> --}}
                <strong>Where should I go?</strong><br>
                <span>Hello how are you doing? Check out for this meal</span><br>             
              </li>
            </a>
            
            </ul>
          </div>
      <div class="modal-footer yellow">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="notificationsModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
        <div class="box-header pink modal-header" data-original-title>
            <h2><i class="icon-bell white"></i><span class="break"></span>Notifications</h2>
            
          </div>
      {{-- </div> --}}
      <div class="modal-body">
        <p>List Of notifications</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close<i class="icon-cancel"></i></button>
      </div>
    </div>

  </div>
</div>

