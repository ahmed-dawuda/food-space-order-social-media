<br><br><br>
@if(isset($mark))
	Mark(Select) Notifications to delete
	<form id="mark_form" action="{{route('clear-notif')}}" method="get">
@else
{{-- @if(count()) --}}
	Notification(s) <a href="" style="color: #00BFFF;" id="seeAllNotification"><i class="fa fa-long-arrow-right">See all notification(s)</i></a>.
@endif

<ul id="notifications">
@if(count($notifications))
	@foreach($notifications as $notification)
	@if($notification->reason == 'is following you')
			<li style="background: #00BFFF;">
			@if(isset($mark))
				<input type="checkbox" value="{{$notification->id}}" name="{{'note'.$notification->id}}">
			@endif
			<i class="fa fa-flag"></i> 
				<a href="{{route('profile',[$notification->user->id])}}" >{{ucfirst($notification->user->username)}}</a> is following You. 
				@if(!Auth::user()->friendOf($notification->user->id))
				 	<a href="{{route('customer-follow',[$notification->user->id])}}" class="follow"><i class="fa fa-street-view"></i> Follow Back</a>
				@endif
			</li>
	@elseif($notification->reason == 'accepted your suggestion')
			<li style="background: #3CB371;">
			@if(isset($mark))
				<input type="checkbox" value="{{$notification->id}}" name="{{'note'.$notification->id}}">
			@endif
			<i class="fa fa-flag"></i> 
				<a href="#" >Akuzi</a> accepted your suggestion 	
			</li>
	@elseif($notification->reason == 'rejected your order because the time you gave wouldn\'t be possible')
			<li style="background: #f9a4a9;">
			@if(isset($mark))
				<input type="checkbox" value="{{$notification->id}}" name="{{'note'.$notification->id}}">
			@endif
			<i class="fa fa-flag"></i> 
				<a href="#" >Brunei Cafetaria</a> rejected your order because the time you gave wouldn't be possible. 	
			</li>
	@elseif($notification->reason == 'rejected your order because location is unknown. sorry')
			<li style="background: #f9a4a9;">
			@if(isset($mark))
				<input type="checkbox" value="{{$notification->id}}" name="{{'note'.$notification->id}}">
			@endif
			<i class="fa fa-flag"></i> 
				<a href="#" >Maa U</a> Rejected your order with ID: 5223242 because your location is unknown. sorry
			</li>
	@endif
	@endforeach
	@if(isset($mark))
		<li style="color: blue;">
			<a href="#" id="clearSelected"><i class="fa fa-trash" style="color: red; margin-top: 5px;"></i> Clear Selected</a>	
		</li>
	@else
		<li style="color: blue;">
			<a href="#" id="clearNotification"><i class="fa fa-trash" style="color: red; margin-top: 5px;"></i> Clear Notifications</a>	
		</li>
	@endif
			
@else
<li>
	No new notification
</li>
@endif
</ul>
@if(isset($mark))
{{-- <input type="hidden" name="_token" value="{{Session::token()}}"> --}}
	</form>
@endif