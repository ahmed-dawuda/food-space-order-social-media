<script id="orderTemplate" type="text/x-jquery-tmpl">
    <li>
                <strong>Restuarant:</strong> <a href="#">${res_id}</a><br>
                <strong>Order ID:</strong> <a href="#">${ord_id}</a><br>
                <strong>Order Type:</strong> <a href="#">${ord_type}</a><br>
                <strong>Time Ordered:</strong> ${ord_time}<br>
                <strong>Delivery Time:</strong> ${del_time}<br>
                <strong>Status:</strong> <span class="label label-success">${status}</span><br><br>
                <span>
                <a class="btn btn-success" href="#">
                    <i class="halflings-icon white ok"></i>                                            
                  </a>
                  <a class="btn btn-info" href="#">
                    <i class="halflings-icon white question-sign"></i>                                            
                  </a>
                  <a class="btn btn-danger" href="#">
                    <i class="halflings-icon white trash"></i> 
                  </a>
                  </span>
              </li> 
  </script>

  <script id="friendTemplate" type="text/x-jquery-tmpl">
    {{-- <li>friends template</li>  --}}
    <li>
      <a href="{{URL::to('profile')}}/${id}">
        <img class="avatar" alt="" src="{{asset('img/avatar.jpg')}}">
      </a>
      <strong>Name:</strong> <a href="#">${username}</a><br>
      <strong>phone:</strong> ${phone}<br>
      <strong>Status:</strong> <span class="label label-important">Offline</span> 
      <a href="{{ URL::to('profile')}}/${id}/chat" style="margin: 0;"><i class="icon-envelope yellow"></i></a>                                 
    </li> 
  </script>


  <script id="messageTemplate" type="text/x-jquery-tmpl">
    <a href="${chat_url}" class="instantChat">
      <li class="hov">
        {{-- <a href="#"> --}}
          <img class="avatar" alt="Dennis Ji" src="{{asset('img/avatar.jpg')}}">
        {{-- </a> --}}
        <strong>${username}</strong><br>
        <strong style="color: ${badge};"><i class="icon-envelope"></i>${current_message}</strong> <br>
        <strong>Status:</strong> <span class="label label-success">${privacy}</span>                                  
      </li>
    </a>
  </script>

  <script id="chatTemplate" type="text/x-jquery-tmpl">
    <li class="${position}">
      {{-- <img class="avatar" alt="Dennis Ji" src="{{asset('img/avatar.jpg')}}"> --}}
      <span class="message"><span class="arrow"></span>
        {{-- <span class="from">Dennis Ji</span> --}}
        <span class="time">${created_at.date}</span>
        <span class="text">
          ${content}
        </span>
      </span>                                   
    </li>
    
  </script>


  <script id="favTemplate" type="text/x-jquery-tmpl">
    <li>
      <a href="{{URL::to('/')}}/favourite-restaurant/${id}">
        <img class="avatar" alt="Dennis Ji" src="{{asset('hand.jpg')}}" style="margin-bottom: 20px;">
      </a>
      <strong>Name:</strong> <a href="#">${name}</a><br>
      <strong>About:</strong> <a href="#">${home_mesage}</a><br>
      <strong>No of Orders you made:</strong>10<br>
      <strong>Successfull Delivery:</strong>6<br>
  
    </li> 
  </script>



  <script id="faqTemplate" type="text/x-jquery-tmpl">
    <li>
                frequently asked questions template
              </li> 
  </script>



  <script id="topTemplate" type="text/x-jquery-tmpl">
     <li>
      <a href="{{URL::to('/')}}/favourite-restaurant/${id}">
        <img class="avatar" alt="Dennis Ji" src="{{asset('hand.jpg')}}" style="margin-bottom: 20px;">
      </a>
      <strong>Name:</strong> <a href="#">${name}</a><br>
      <strong>About:</strong> <a href="#">${home_mesage}</a><br>
      <strong>No of Orders you made:</strong>10<br>
      <strong>Rating:</strong><i class="icon-star star"></i><i class="icon-star star"></i><i class="icon-star star"></i> <br>
  
    </li> 
  </script>



<script id="suggestionTemplate" type="text/x-jquery-tmpl">
    <li>
         Suggestion feature is underconstruction...Please visit the restaurant homepage, click the call icon.
    </li> 
  </script>



  <script id="trendTemplate" type="text/x-jquery-tmpl">
    <li>
      Trends Feature not yet available. Goto Timeline > Account Settings > Trending
    </li> 
  </script>



   <script id="aboutTemplate" type="text/x-jquery-tmpl">
    <li>
      About feature is not yet available
    </li> 
  </script>



   <script id="foodTemplate" type="text/x-jquery-tmpl">
    <li>
        Foods feature is not yet available
    </li> 
  </script>

  <script id="postTemplate" type="text/x-jquery-tmpl">
   
  </script>



   <script id="notificationTemplate" type="text/x-jquery-tmpl">
    <li> 
      <i class="icon-arrow-up green" ></i>                               
      <a href="${profile_url}"><strong>${follower}</strong></a>
      ${reason}  <a href="${followback_url}" ${followback}><strong> Follow back</strong></a>                                  
  </li>
              
  </script>