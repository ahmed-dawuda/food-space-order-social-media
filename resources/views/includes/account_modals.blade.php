

{{-- NewsLetter Modal --}}
<div id="newsModal" class="modal fade myspecs" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="box-header modal-header">
        <h2><i class="icon-email white"></i><span class="break"></span>Subsribe</h2>
      </div>
      <div class="box-content modal-body">
		<form class="form-horizontal">
			<fieldset>
			  <div class="control-group">
				<label class="control-label" for="focusedInput">Email:</label>
				<div class="controls">
				  <input class="input-xlarge focused" id="focusedInput" type="email" placeholder="Enter email" name="email">
				</div>
			  </div>
			</fieldset>
		  </form>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Subscribe Now</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

