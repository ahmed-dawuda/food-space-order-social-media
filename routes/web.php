<?php

/* USERS GET REQUESTS */
Route::get('/',['as'=>'user-welcome','uses'=>'DefaultController@welcomeUser']);
Route::get('/user-logout',['as'=>'user-logout','uses'=>'UserController@logout']);
Route::get('/message',['uses'=>'UserController@message']);


/* CUSTOMER GET REQUESTS */
Route::get('/customer/home',['as'=>'customer-home','uses'=>'CustomerController@customerHome']);
Route::get('/profile/{id}/{message?}',['as'=>'profile','uses'=>'CustomerController@showProfile'])->middleware('profile');
Route::get('/tmplt/{id}',['as'=>'tmplt_req','uses'=>'CustomerController@template']);
Route::get('/post/{id}',['uses'=>'CustomerController@getPostByID']);
Route::get('/customer/update/edit-profile/{edit?}',['as'=>'customer-update-profile','uses'=>'CustomerController@customerUpdate']);
Route::get('/account/delete',['as'=>'delete-account','uses'=>'CustomerController@deleteAccount']);
Route::get('/customer/follow/customer/{user_id}/{stop?}',['as'=>'customer-follow','uses'=>'CustomerController@follow']);
Route::get('/customer/vote/post/{post_id}/{votedown?}',['as'=>'vote','uses'=>'CustomerController@vote']);
Route::get('/customer/delete/post/{id}',['as'=>'customer-delete-post','uses'=>'CustomerController@deletePost']);
Route::get('/customer/profile/trending',['as'=>'trending','uses'=>'CustomerController@trending']);
Route::get('/customer/profile/top',['as'=>'top','uses'=>'CustomerController@top']);
Route::get('/customer/profile/my-messages',['as'=>'my-messages','uses'=>'CustomerController@myMessages']);
Route::get('/customer/profile/my-posts',['as'=>'my-posts','uses'=>'CustomerController@myPosts']);
Route::get('/customer/profile/news-feed',['as'=>'news-feed','uses'=>'CustomerController@newsFeed']);
Route::get('/customer/profile/notifications',['as'=>'notifications','uses'=>'CustomerController@notifications']);
Route::get('/customer/profile/mark-notif',['as'=>'mark-notif','uses'=>'CustomerController@markNotif']);
Route::get('/customer/notifications/clear/{all?}',['as'=>'clear-notif','uses'=>'CustomerController@clearAll']);
Route::get('/customer/chat/screen',['as'=>'chat_screen','uses'=>'CustomerController@chatScreen']);
Route::get('/customer/retrieve/chats',['as'=>'chats','uses'=>'CustomerController@chats']);
Route::get('/customer/retrieve/chat/{friend_id}/messages',['as'=>'retrieve_messages','uses'=>'CustomerController@chatMessages']);
Route::get('/customer/send/chat/message',['as'=>'send_message','uses'=>'CustomerController@sendMessage']);
Route::get('/customer/profile/live-chat/with/friend',['as'=>'live-chat','uses'=>'CustomerController@liveChat']);
Route::get('/customer/profile/conversation/{user_id}/{friend_id}',['as'=>'getChats','uses'=>'CustomerController@getChats']);

/* ADMINS GET REQUESTS */
Route::get('/admin',['as'=>'admin-home','uses'=>'AdminController@adminHome'])->middleware('profile');
Route::get('/admin/profile',['as'=>'admin-profile','uses'=>'AdminController@profile']);
Route::get('/admin/orders',['as'=>'admin-orders','uses'=>'AdminController@orders'])->middleware('profile');
Route::get('/admin/foods',['as'=>'admin-foods','uses'=>'AdminController@foods'])->middleware('profile');
Route::get('/admin/notifications',['as'=>'admin-notifications','uses'=>'AdminController@notifications'])->middleware('profile');
Route::get('/admin/messages',['as'=>'admin-messages','uses'=>'AdminController@messages'])->middleware('profile');
Route::get('/admin/working/{close?}',['as'=>'admin-working','uses'=>'AdminController@working']);
Route::get('/admin/reject/order/{user_id}/{order_id}/{reason}',['as'=>'reject-order','uses'=>'AdminController@rejectOrder']);
Route::get('/admin/settings/{type}/{phase}',['as'=>'admin-settings','uses'=>'AdminController@settings']);
Route::get('/admin/add/food',['as'=>'admin-add-food','uses'=>'AdminController@addFood']);
Route::get('/admin/view/order/{id}',['as'=>'view-order','uses'=>'AdminController@viewOrder']);
Route::get('/admin/sort/order/{key}/{id?}',['as'=>'sort-order','uses'=>'AdminController@sortOrder']);
Route::get('/admin/view/by-group/{id}/{key?}',['as'=>'view-group','uses'=>'AdminController@viewGroup']);


/* RESTAURANT GET REQUESTS */
Route::get('/favourite-restaurant/{id}',['as'=>'favourite-restaurant','uses'=>'RestaurantController@showByID']);
Route::get('/fovourite-restaurant/next/{id}',['as'=>'next-restaurant','uses'=>'RestaurantController@nextRestaurant']);
Route::get('/favourite-restaurant/previous/{id}',['as'=>'previous-restaurant','uses'=>'RestaurantController@previousRestaurant']);
Route::get('/order/{id}',['uses'=>'RestaurantController@order']);
Route::get('/follow/restaurant/{id}',['as'=>'follow','uses'=>'RestaurantController@follow']);



/* POST REQUESTS */
Route::post('login',['as'=>'user-login','uses'=>'UserController@login']);
Route::post('register',['as'=>'user-register','uses'=>'UserController@register']);
Route::post('/order',['uses'=>'UserController@order']);
Route::post('/suggestion',['as'=>'suggest','uses'=>'RestaurantController@suggestion']);
Route::post('/customer/create/edit',['as'=>'create-profile','uses'=>'CustomerController@createProfile']);
// Route::post('/customer/delete/marked',['as'=>'delete_marked','uses'=>'CustomerController@deleteMarked']);
Route::post('/submit/post',['as'=>'submit-post','uses'=>'CustomerController@submitPost']);
Route::post('/admin/profile/save',['as'=>'admin-save','uses'=>'AdminController@updateProfile']);


