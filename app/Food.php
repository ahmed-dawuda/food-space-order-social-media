<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
	public $timestamps = false;

    public function restaurant(){
    	return $this->belongsTo('App\Restaurant');
    }

    public function prices(){
    	return $this->hasMany('App\Price');
    }
}
