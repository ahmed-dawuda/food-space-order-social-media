<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Restaurant;

use App\Food;

use App\Price;

class RestaurantController extends Controller
{
    public function showByID($id){
    	$restaurant = Restaurant::find($id);
    	return view('restaurant.account',['restaurant'=>$restaurant]);
    }

    public function nextRestaurant($id){
    	$restaurant = Restaurant::where('id','>',$id)->first();
    	if(isset($restaurant)){
    		return view('restaurant.account',['restaurant'=>$restaurant]);
    	}else{
    		$restaurant = Restaurant::find($id);
    		return view('restaurant.account',['restaurant'=>$restaurant]);
    	}
    	
    }

    public function previousRestaurant($id){

    	$restaurant = Restaurant::where('id','<',$id)->last();

    	if(isset($restaurant)){
    		return view('restaurant.account',['restaurant'=>$restaurant]);
    	}else{
    		$restaurant = Restaurant::find($id);
    		return view('restaurant.account',['restaurant'=>$restaurant]);
    	}
   
	}

    public function order($id){
        $food = Food::find($id);
        $prices = explode("/",$food->pricing);
        return ['food'=>$food,'prices'=>$prices];
    }

    public function suggestion(Request $request){
        return $request;
    }

    public function follow($id){
        return $id;
    }

}