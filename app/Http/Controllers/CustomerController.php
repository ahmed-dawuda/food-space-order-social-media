<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use App\User;

use App\Restaurant;

use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use App\Order;

use App\Post;

use App\Profile;

use App\Location;

use App\Message;

use App\Food;

use App\Notification;

use App\CustomLibs\Chat;

use App\CustomLibs\ChatMessage;

use Carbon\Carbon;

use App\CustomLibs\TestNotification;


class CustomerController extends Controller
{
    public function customerHome(){
    	return view('customer.test');
    }

    public function showProfile($id,$message = null){
       
    	$isMessage = 0;

    	if(isset($message))
    		$isMessage = 1;
    	else $isMessage = 0;

    	$user = User::find($id);
        
    	return view('customer.profile',['user_profile'=>$user,'message'=>$isMessage]);
    }

    public function template($id){
        
        $data = '';
    
        switch ($id) {
            case 'fav':
                $data = Restaurant::all();
                break;     
            case 'orders':
                $data = Auth::user()->orders;
                break;
            case 'friends':
                $data = Auth::user()->friends;
                break;
            case 'top':
                $data = Restaurant::all();
                break;
            case 'suggestions':
                $data = 'suggestions';
                break;
            case 'trends':
                $data = Post::all();
                break;
            case 'messages':
                // $data = Auth::user()->messages;
                $data = $this->chats();
                break;
            case 'notifications':
                $notes = Auth::user()->notifications;
                $data = array();
                foreach($notes as $note){
                    $temp = new TestNotification($note);
                    $data[] = $temp->getData();
                }
                break;
            case 'faq':
                $data = Auth::user();
                break;
            case 'foods':
                $data = Food::all();
                break;

            default:
                return 'about';
        }

        return $data;
    }

    public function getPostByID($id){
        $comments = Post::find($id)->comments;
        // return "all comments related to post with id = ".$id;
        $data = array();
        foreach($comments as $comment){
            $data[] = array('user'=>$comment->user->username,'created'=>$comment->created_at,'body'=>$comment->body);
        }
        return array('user'=>'Ahmed','created'=>'5th Jul 2014','body'=>'This post is the most funny of all');
    }

    public function customerUpdate($edit = null){
        if(isset($edit)){
            return view('customer.edit-profile',['profile'=>Auth::user()->profile,'location'=>Auth::user()->location]);
        }
        return view('customer.edit-profile');
    }

    public function createProfile(Request $request){

        // $rules = array(
        //     'full_name'=>'required',
        //     'date_of_birth'=>'required',
        //     );

        // $validator = Validator::make($request->all(), $rules);

        // if ($validator->fails()) {
        //      return response()->json(array('success' => false,'errors' => $validator->getMessageBag()->toArray()), 422);
        // }


        $profile = '';
        $location = '';
        if(Auth::user()->profile_updated == true){
            $profile = Auth::user()->profile;
            $location = Auth::user()->location;
        }else{
            $profile = new Profile();
            $location = new Location();
        }
        
        $profile->full_name = $request['full_name'];

        $profile->status =  $request['status'] == ''? $profile->status : $request['status'] ;

        $profile->date_of_birth = $request['date_of_birth'];

        $profile->gender = $request['gender'] == 0?'Unspecified' : $request['gender'] == 1? 'M' : 'F';

        $profile->work_place_name = isset($request['work']) ? $request['work']:'Unspecified';

        $profile->position_at_work = isset($request['position']) ? $request['position']:'Unspecified';

        $profile->about_work = isset($request['about_work']) ? $request['about_work']:$profile->about_work;


        $location->country = $request['country'];
        $location->street = $request['street'];
        $location->state = $request['state'];
        $location->city = $request['city'];
        $location->zip_code = $request['zip'];

        $profile->user_id = Auth::user()->id;
        $location->user_id = Auth::user()->id;
        $location->save();
        $profile->save();
        
        Auth::user()->profile_updated = true;
        Auth::user()->save();
        
        return 1;
    }

    public function deleteAccount(Request $request){

        if($request->ajax()){
            Auth::user()->deleteUser();
            return 1;
        }else{
            return '<a href="'.route('user-welcome').'">Confirm Delete</a>';
        }
    }

    public function follow(Request $request, $user_id, $stop = null){

        if($request->ajax()){
            if(isset($stop)){
                Auth::user()->removeFriend($user_id);
                return "Stopped Following";
            }else{
                Auth::user()->addFriend($user_id);
                return "Started Following";
            }
        }
        return redirect()->back();
    }

    public function vote(Request $request, $post_id, $votedown = null){
        $return = '';
        if(isset($votedown)){
            Auth::user()->voteDown($post_id);
            $return = -1;
        }else{
            Auth::user()->voteUp($post_id);
            $return = 1;
        }

        if($request->ajax()){
            return ['return'=>$return,'votes'=>Auth::user()->votes($post_id)];
        }else{
            return redirect()->back();
        }
    }

    public function deletePost($id){
        Auth::user()->deleteUserPost($id);
        return "deleted post with id = ".$id;
    }

    public function trending(){
        return "trends";
    }

    public function myMessages(){
        return view('includes.messages');
    }

    public function myPosts(){
        $posts = Auth::user()->posts;
        return view('includes.myPost_tmpls',['posts'=>$posts]);
    }

    public function newsFeed(){
        return "news feed";
    }

    public function top(){
        return view('includes.top_rests',['restaurants'=>Restaurant::all()]);
    }

    public function notifications(){
        
        $notifications = Auth::user()->notifications;
        return view('includes.notifications',['notifications'=>$notifications]);  
    }

    public function markNotif(){
        return view('includes.notifications',['notifications'=>Auth::user()->notifications,'mark'=>1]);
    }


    public function clearAll(Request $request,$id = null){
        if(isset($id)){
            Auth::user()->notifications()->delete();
            return "all notifications deleted";
        }else{
            foreach($request->toArray() as $key => $value){
                // echo "deleting notification: ".$value." ";
                Notification::destroy($value);
            }
            return "selected notifications have been deleted";
        }
    }

    public function chatScreen(){
        return view('customer.chatscreen');
    }

    public function chats(){
        $friendIDs = Auth::user()->usersIDs();
        $friends = User::whereIn('id', $friendIDs)->get();
        $chats = array();

        foreach($friends as $friend){
            $chat = new Chat($friend);
            $chats[] = $chat->getData();   
        }

        for($pass =0; $pass < count($chats); $pass++){
            for($i = 0; $i < count($chats) -1; $i++){
                if($chats[$i+1]['created'] > $chats[$i]['created']){
                    $temp = $chats[$i+1];
                    $chats[$i+1] = $chats[$i];
                    $chats[$i] = $temp;
                }
            }
        }
        return $chats;
       
    }

    public function chatMessages($friend_id){

        $messages = Auth::user()->retriveChatMessagesWith($friend_id);
        $user = User::find($friend_id);
        $chat = new ChatMessage($user,$messages);
        return $chat->getData();
            
    }

    public function sendMessage(){

    }

    public function liveChat(Request $request){
        $chats = DB::table('chats')->get();
  
        $chatID = $chats->where('user_1', Auth::user()->id)->where('user_2',$request->user_id)->count();
        $chat = $chats->where('user_1', Auth::user()->id)->where('user_2',$request->user_id)->pluck('id');

        if($chatID == 0){            
            $chatID = $chats->where('user_1',$request->user_id)->where('user_2',Auth::user()->id)->count();
            $chat = $chats->where('user_1',$request->user_id)->where('user_2',Auth::user()->id)->pluck('id');
        }

        // return "This is the 2nd try. count() = ".$chatID;

        if($chatID == 0){

            DB::table('chats')->insert([
                'user_1'=>Auth::user()->id,
                'user_2'=>$request->user_id,
                'created_at'=> Carbon::now(),
            ]);

            $chat = DB::table('chats')->where('user_1',Auth::user()->id)->where('user_2',$request->user_id)->pluck('id');
            // return  "Chat is still empty";
        }

        $message = new Message();
        $message->user_id = Auth::user()->id;
        $message->reciever_id = $request->user_id;
        $message->body = $request->textbody;
        $message->chat_id = $chat[0];
        $message->save();

         // return $request->textbody;
        return $message;

    }

    public function getChats(Request $request, $user_id, $friend_id){
        $messages = Auth::user()->retriveChatMessagesWith($friend_id);
        $return = array();
        foreach($messages as $message){
            $return[] = [
                'message'=>$message->body,
                'time'=>$message->created_at,
                'style'=> $message->user_id == Auth::user()->id ? 'btrr' : 'btlr',
            ];
        }
        return $return;
    }

    public function submitPost(Request $request){
        return $request->post_text;
    }
}
