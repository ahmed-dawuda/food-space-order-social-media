<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// use Mail;

use Validator;

use App\User;

// use App\Profile;

// use App\Portfolio;

use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;

class DefaultController extends Controller
{
    public function welcomeUser(){

    	if(Auth::check()){

            if(Auth::user()->admin == true)

                return redirect()->route('admin-home');

            return redirect()->route('customer-home');

        }
        
    	return view('reglog');
    }
    
}
