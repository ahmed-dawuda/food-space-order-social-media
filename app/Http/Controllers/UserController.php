<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// use Mail;

use Validator;

use App\User;

use App\Order;

// use App\Profile;

// use App\Portfolio;

use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
	//login
    public function login(Request $request){

    	$rules = array(
            'mobile'=>'required|numeric|min:10',
            'pasword'=>'required|min:5|alpha_num',
            );

    	$validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $remember = $request['remember'] == 1 ? true:false;

    	if(Auth::attempt(['phone'=>$request['mobile'],'password'=>$request['pasword']],$remember)){

    		if(Auth::user()->admin == true){
    			return redirect()->route('admin-home');
    		}else{
    			return redirect()->route('customer-home');
    		}
    		
    	}else{
    		return redirect()->back()->with('failed','You Entered wrong credentials');
    	}

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('user-welcome');
    }

    public function register(Request $request){
    
    	$rules = array(
            'username'=>'required|alpha_dash|min:3',
            'phone'=>'required|numeric|min:10',
            'password'=>'required|min:5|alpha_num',
            );

    	$validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $admin = $request['restaurant'] == 1 ? true:false;

        $user = new User();
        $user->phone = $request['phone'];
        $user->password = bcrypt($request['password']);
        $user->username = $request['username'];
        $user->admin = $admin;
        $user->save();

        $remember = $request['remember'] == 1 ? true:false;

        if(Auth::attempt(['phone'=>$request['phone'],'password'=>$request['password']], $remember)){

            if(Auth::user()->admin == true){
                return redirect()->route('admin-profile');
            }else{
                return redirect()->route('customer-update-profile');
            }
            
        }else{
            return redirect()->back()->with('failed','There is a problem when tying to log you in automatically! Please login manually');
        }

    }

    public function order(Request $request){
        $orderID = rand(1111111,9999999);
        $order = new Order();
        $order->orderID = $orderID;
        $order->user_id = Auth::user()->id;
        $order->food_id = $request['food_id'];
        $order->restaurant_id = $request['restaurant_id'];
        // $order->order_time = '00:00:00';
        $order->quantity = $request['quantity'];
        $order->status = 'pending';
        $order->price = $request['price'];
        $order->delivery_time = $request['delivery_time'];
        $order->save();
        return $order;
    }

    public function message(Request $request){
        return $request['textbody'];
    }
}
