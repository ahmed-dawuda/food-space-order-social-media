<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// use Mail;

use Validator;

use App\User;

use App\Chef;

use App\Restaurant;
// use App\Profile;

// use App\Portfolio;
use App\CustomLibs\SingleOrder;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;

use App\Order;

class AdminController extends Controller
{

    public function adminHome(){
        return view('admins.home');
    }

    public function profile(){
        $hasRestaurant = Auth::user()->restaurant;
        $not_set = is_null($hasRestaurant);
        $true = !$not_set;
        $chef_is_null = false;
        $chefs = array();
        if($not_set){
            $chef_is_null = true;
        }else{
            $chefs = Auth::user()->restaurant->chefs;
        }

        return view('admins.profile',['not_set'=>$not_set,'true'=>$true,'chefs'=>$chefs,'chef_is_null'=>$chef_is_null]);
    }


    public function orders(){
        $orders = Auth::user()->restaurant->orders()->orderBy('orderID')->get();
        $daily_orders = array();
        foreach($orders as $order){
            $single_order = new SingleOrder($order);
            $daily_orders[] = $single_order->getData();
        }
        // return var_dump($daily_orders);
    	return view('admins.orders',['orders'=>$daily_orders]);   
    }

    public function foods(){
        $orders = Auth::user()->restaurant->orders;
        $daily_orders = array();
        foreach($orders as $order){
            $single_order = new SingleOrder($order);
            $daily_orders[] = $single_order->getData();
        }
        
    	return view('admins.foods');
    }

    public function notifications(){
    	return view('admins.notifications');
    }

    public function messages(){
    	return view('admins.messages');
    }

    public function updateProfile(Request $request){
        // return $request->input();
        
        $chef_1 = explode('-', $request->chef_1);
        $chef_2 = explode('-', $request->chef_2);
        $chef_3 = explode('-', $request->chef_3);

        if(!is_null(Auth::user()->restaurant)){

            Auth::user()->restaurant->updateRestaurant($request);

        }else{

            // return "creating new profile entry";
            $restaurant = new Restaurant();
            $restaurant->managers_name = $request->username;
            $restaurant->name = $request->name;
            $restaurant->email = $request->email;
            $restaurant->about_us = $request->about_us;
            $restaurant->home_message = $request->home_message;
            $restaurant->why_us = $request->why_us;
            $restaurant->other_us = $request->other_us;
            $restaurant->country = $request->country;
            $restaurant->state = $request->state;
            $restaurant->city = $request->city;
            $restaurant->district = $request->district;
            $restaurant->street = $request->street;
            $restaurant->postal = $request->postal;
            $restaurant->profile_updated = true;
            $restaurant->user_id = Auth::user()->id;
            $restaurant->save();
            $restaurant->createChefs($chef_1, $chef_2, $chef_3);
            Auth::user()->profile_updated = true;
            Auth::user()->update();
        }     
        return redirect()->back();
    }

    public function working(Request $request, $close = null){
        if($request->ajax()){

            $restaurant = Auth::user()->restaurant;

            if(isset($close)){
                $restaurant->working = false;
            }else{
                $restaurant->working = true;
            }

            $restaurant->update();
        }

        return $close;
    }

    public function rejectOrder(Request $request,$user_id, $order_id, $reason){
        
        return "user with ID: ".$user_id." with the order of ID: ".$order_id." is been rejected because ".$reason;
    }

    public function settings(Request $request, $type, $phase){
        
    }

    public function addFood(Request $request){
        $rules = array('food_name'=>'required','ingredients'=>'required','prices'=>'required');
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
             return response()->json(array('success' => false,'errors' => $validator->getMessageBag()->toArray()), 200);
        }

        Auth::user()->restaurant->addFood($request);
        return response()->json(array('success' => true,'errors' => ''), 200);
    }

    public function viewOrder(Request $request, $id){
        if($request->ajax()){
            $order = new SingleOrder(Order::find($id));
            return $order->getData();
        }
    }

    public function sortOrder($key, $id = 'all'){
        if($id == 'all'){
            $orders = Auth::user()->restaurant->orders()->orderBy($key)->get();
        }else{
            $orders = Auth::user()->restaurant->orders()->orderBy($key)->where('status','LIKE',$id)->get();
        }
        
        $daily_orders = array();
        foreach($orders as $order){
            $single_order = new SingleOrder($order);
            $daily_orders[] = $single_order->getData();
        }
        return view('admins.order_tmpl',['orders'=>$daily_orders]);
    }

    public function viewGroup($id, $key = 'created_at'){
        $orders = '';
        if($id == "all"){
            $orders = Auth::user()->restaurant->orders()->orderBy($key)->get();
        }else{
            $orders = Auth::user()->restaurant->orders()->where('status','LIKE',$id)->orderBy($key)->get();
        }

        foreach($orders as $order){
            $single_order = new SingleOrder($order);
            $daily_orders[] = $single_order->getData();
        }
        return view('admins.order_tmpl',['orders'=>$daily_orders]);
    }
}
