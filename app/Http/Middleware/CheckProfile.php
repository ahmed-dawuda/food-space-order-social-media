<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->profile_updated == false){
            if(Auth::user()->admin == false){
                return redirect()->route('customer-update-profile');
            }else{
                
                if(is_null(Auth::user()->resturant))
                    return redirect()->route('admin-profile');
            }  
        }
        return $next($request);
    }
}
