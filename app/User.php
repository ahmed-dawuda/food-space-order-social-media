<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function location(){
        return $this->hasOne('App\Location');
    }

    public function posts(){
        return $this->hasMany('App\Post')->orderBy('created_at', 'desc');
    }

    public function restaurant(){
        return $this->hasOne('App\Restaurant');
    }

    public function orders(){
        return $this->hasMany('App\Order')->orderBy('created_at', 'desc');
    }

    public function notifications(){
        return $this->hasMany('App\Notification','destination_id')->orderBy('created_at', 'desc');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

     public function deleteUser()
    {
        // delete all related photos 
        $this->profile()->delete();
        $this->location()->delete();
        $this->posts()->delete();
        $this->comments()->delete();
        
        return parent::delete();
    }

    public function messages(){
        return $this->hasMany('App\Message')->orderBy('created_at','desc');
    }

    public function friends(){
        return $this->belongsToMany('App\User', 'friends_users','user_id','friend_id')->withTimestamps();
    }

    public function followedBy(){
        return $this->belongsToMany('App\User', 'friends_users','friend_id','user_id')->withTimestamps()->wherePivot('followed_back', true);
    }

    public function friendOf($id){
        return DB::table('friends_users')
                    ->where('user_id',$this->id)
                    ->where('friend_id',$id)
                    ->count();
    }

    public function addFriend($id)
    {
        $this->friends()->attach($id);
        $notification = new Notification();
        $notification->source_id = $this->id;
        $notification->destination_id = $id;
        $notification->reason = 'is following you';
        $notification->save();
    }

    public function removeFriend($id)
    {
        $this->friends()->detach($id);
        // DB::table('notifications')->where('destination_id',$id)->where('reason','LIKE','is following you')->delete();
    }

    public function friendsPosts(){
        $friends = $this->friends;
        $ids = array();
        foreach($friends as $friend){
            $ids[] = $friend->id;
        }
        $posts = DB::table('posts')->whereIn('user_id', $ids)->orWhere('user_id',$this->id)
                ->orderBy('created_at', 'desc')
                ->get();
        return $posts;
    }

    public function voteUp($post_id){
        DB::table('users_posts')->insert([
            'user_id'=> $this->id,
            'post_id'=> $post_id,
        ]);
    }

    public function voteDown($post_id){
        DB::table('users_posts')->where('user_id',$this->id)->where('post_id',$post_id)->delete();
    }

    public function voted($post_id){
        return DB::table('users_posts')
                    ->where('user_id',$this->id)
                    ->where('post_id',$post_id)
                    ->count();
    }

    public function votes($post_id){
        return DB::table('users_posts')
                    ->where('post_id',$post_id)
                    ->count();
    }

    public function deleteUserPost($post_id){
        Post::find($post_id)->delete();
        DB::table('users_posts')->where('post_id',$post_id)->delete();
    }

    public function retriveChatMessagesWith($friend_id){
        $chats = DB::table('chats')
                        ->where('user_1',$this->id)
                        ->where('user_2',$friend_id)
                        ->orWhere('user_1',$friend_id)->where('user_2',$this->id)->get();
        $chatIDs = array();
        foreach($chats as $chat){
            $chatIDs[] = $chat->id;
        }
       $messages = Message::whereIn('chat_id',$chatIDs)->orderBy('created_at')->get();
        return $messages;
    }

    public function usersIDs(){
         
        $friends = $this->friends;
        $IDs = array();
        foreach ($friends as $friend) {
            if($this->hasConversationWith($friend->id) == 1){
                $IDs[] = $friend->id;
            } 
        }
        return $IDs;
    }

    public function hasConversationWith($user_id){
        $chats = DB::table('chats')->get();
        $condition1 = $chats->where('user_1',$this->id)->where('user_2',$user_id)->count();
        $condition2 = $chats->where('user_1',$user_id)->where('user_2',$this->id)->count();
        if($condition1 || $condition2) return 1;
        return 0;
    }
}
