<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function user(){
    	return $this->belongsTo('App\User','source_id');
    }

    // public function getRestaurantName(){
    // 	$restaurant = Restaurant::where('user_id',$this->user->id)->get();
    // 	return $restaurant->name;
    // }
}
