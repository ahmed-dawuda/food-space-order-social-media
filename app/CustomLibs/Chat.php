<?php
	namespace App\CustomLibs;
	use App\User;
	use Illuminate\Support\Facades\Auth;

	class Chat{
		public $username;
		public $current_message;
		public $privacy;
		public $user_id;
		public $profile_url;
		public $chat_url;
		public $badge;
		public $created;

		public function __construct(User $user){
			$this->user_id = $user->id;
			$this->username = $user->username;
			$this->privacy = 'online';


			$message = Auth::user()->retriveChatMessagesWith($user->id)->last();
			if($message->user->id == Auth::user()->id){
				$this->badge = 'grey';
			}else{
				$this->badge = '#00BFFF';
			}

			$this->current_message = $message->body;
			$this->created = $message->created_at->toDateTimeString();
			$this->profile_url = route('profile',[$user->id]);
			$this->chat_url = route('retrieve_messages',[$user->id]);
		}

		public function getData(){
			return array(
				'username'=>$this->username,
				'current_message'=>$this->current_message,
				'privacy'=>$this->privacy,
				'user_id'=>$this->user_id,
				'profile_url'=>$this->profile_url,
				'chat_url'=>$this->chat_url,
				'badge'=>$this->badge,
				'created'=>$this->created,
			);
		}
	}
?>
