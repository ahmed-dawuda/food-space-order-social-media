<?php
	namespace App\CustomLibs; 
	use App\Order;
	use App\Food;
	use App\User;
	/**
	* 
	*/
	class SingleOrder{
		public $id;
		public $orderID;
		public $food_name;
		public $delivery_time;
		public $ordered_at;
		public $price;
		public $quantity;
		public $user_name;
		public $handle;
		public $user_country;
		public $user_state;
		public $user_city;
		// public $user_district;
		public $user_street;
		// public $user_hseno;
		public $order_destination;
		public $status;
		public $status_color;
		
		public function __construct(Order $order){
			$sender = User::find($order->user_id);
			$this->id = $order->id;
			$this->orderID = $order->orderID;
			$this->food_name = ucfirst(Food::find($order->food_id)->name);
			$this->delivery_time = $order->delivery_time." hour(s) from time orderred";
			$this->ordered_at = $order->created_at->toDateTimeString();
			$this->price = $order->price;
			$this->quantity = $order->quantity;
			$this->user_name = ucfirst($sender->profile->full_name);
			$this->handle = ucfirst($sender->username);
			$this->user_country = ucfirst($sender->location->country);
			$this->user_state = ucfirst($sender->location->state);
			$this->user_city = ucfirst($sender->location->city);
			// $this->user_district = $sender->location->district;
			$this->user_street = ucfirst($sender->location->street);
			$this->order_destination = ucfirst($order->destination);
			$this->status = ucfirst($order->status);
			if($order->status == 'pending'){
				$this->status_color = 'warning';
			}elseif($order->status == 'Rejected'){
				$this->status_color = 'danger';
			}else{
				$this->status_color = 'success';
			}
		}

		public function getData(){
			return array(
				'id'=>$this->id,
				'orderID'=>$this->orderID,
				'food_name'=>$this->food_name,
				'delivery_time'=>$this->delivery_time,
				'ordered_at'=>$this->ordered_at,
				'price'=>$this->price,
				'quantity'=>$this->quantity,
				'user_name'=>$this->user_name,
				'handle'=>$this->handle,
				'user_country'=>$this->user_country,
				'user_street'=>$this->user_street,
				'user_state'=>$this->user_state,
				'user_city'=>$this->user_city,
				// 'user_district'=>$this->user_district,
				'order_destination'=>$this->order_destination,
				'status'=>$this->status,
				'status_color'=>$this->status_color,
				);
		}
	}
?>