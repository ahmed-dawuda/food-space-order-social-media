<?php
	namespace App\CustomLibs;

	use App\CustomLibs\SingleChatMessage;

	use App\User;
	/**
	* 
	*/
	class ChatMessage
	{
		public $chat_name;
		public $submit_url;
		public $messages;
		
		function __construct(User $user, $messages)
		{
			$this->chat_name = ucfirst($user->username);
			$this->submit_url = route('send_message',[$user->id]);
			$this->messages = array();
			foreach($messages as $message){
				$chat = new SingleChatMessage($message);
				$this->messages[] = $chat->getData();
			}
		}

		public function getData(){
			return array(
				'chat_name'=>$this->chat_name,
				'submit_url'=>$this->submit_url,
				'messages'=>$this->messages,
			);
		}
	}
?>