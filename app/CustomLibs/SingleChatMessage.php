<?php
	namespace App\CustomLibs;

	use App\Message;

	use Illuminate\Support\Facades\Auth;

	/**
	* 
	*/
	class SingleChatMessage
	{
		public $created_at;
		public $position;
		public $content; 
		
		function __construct(Message $message)
		{
			$this->created_at = $message->created_at;
			$this->position = Auth::user()->id == $message->user->id ? 'right' : 'left';
			$this->content = $message->body;
		}

		public function getData(){
			return array(
				'created_at'=>$this->created_at,
				'position'=>$this->position,
				'content'=>$this->content,
			);
		}
	}

?>