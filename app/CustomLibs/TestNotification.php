<?php
	namespace App\CustomLibs;
	use Illuminate\Support\Facades\Auth;
	use App\Notification;
	use App\User;

	/**
	* 
	*/
	class TestNotification{
		public $type;
		public $follower;
		public $reason;
		public $followback_url;
		public $profile_url;
		public $followback;
		
		public function __construct(Notification $notification)
		{
			$this->reason = $notification->reason;
			$this->follower = ucfirst(User::find($notification->source_id)->username);
			$this->followback_url = route('customer-follow',[$notification->source_id]);
			$this->profile_url = route('profile',[$notification->source_id]);
			if(Auth::user()->friendOf($notification->source_id)){
				$this->followback = 'style=display:none;';
			}else{
				$this->followback = '';
			}
		}

		public function getData(){
			return array(
				'reason'=>$this->reason,
				'follower'=>$this->follower,
				'followback_url'=>$this->followback_url,
				'profile_url'=>$this->profile_url,
				'followback'=>$this->followback,
			);
		}
	}
?>