<?php

namespace App;
use App\Chef;
use App\Food;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    // public $timestamps = false;

    public function orders(){
        
        return $this->hasMany('App\Order');
    }
    
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function location(){
    	return $this->hasOne('App\Location');
    }

    public function foods(){
    	return $this->hasMany('App\Food');
    }

    public function openHours(){
    	return $this->hasMany('App\Open_hour');
    }

    public function chefs(){
    	return $this->hasMany('App\Chef');
    }

    public function updateChefs($chef1, $chef2, $chef3){
        $chefs = $this->chefs;
        $chefs[0]->full_name = $chef1[0];
        $chefs[0]->role = $chef1[1];
        $chefs[0]->update();

        $chefs[1]->full_name = $chef2[0];
        $chefs[1]->role = $chef2[1];
        $chefs[1]->update();

        $chefs[2]->full_name = $chef3[0];
        $chefs[2]->role = $chef3[1];
        $chefs[2]->update();
        return;
    }

    public function createChefs($chef_1, $chef_2, $chef_3){
        $chef = new Chef();
        $chef->full_name = $chef_1[0];
        $chef->role = $chef_1[1];
        $chef->restaurant_id = $this->id;
        $chef->save();

        $chef = new Chef();
        $chef->full_name = $chef_2[0];
        $chef->role = $chef_2[1];
        $chef->restaurant_id = $this->id;
        $chef->save();

        $chef = new Chef();
        $chef->full_name = $chef_3[0];
        $chef->role = $chef_3[1];
        $chef->restaurant_id = $this->id;
        $chef->save();
        return;
    }


    public function updateRestaurant(Request $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->about_us = $request->about_us;
        $this->home_message = $request->home_message;
        $this->why_us = $request->why_us;
        $this->other_us = $request->other_us;
        $this->country = $request->country;
        $this->state = $request->state;
        $this->city = $request->city;
        $this->district = $request->district;
        $this->street = $request->street;
        $this->postal = $request->postal;
        $this->profile_updated = true;
        $this->update();
    }

    public function addFood(Request $request){
        $food = new Food();
        $food->name = $request->food_name;
        $food->period = $request->type;
        $food->pricing = $request->prices;
        $food->ingredients = $request->ingredients;
        $food->restaurant_id = $this->id;
        $food->save();
    }
}
