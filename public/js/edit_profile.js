$(document).ready(function(){
	var appended = false;
	// warn(appended)
	$('a').click(function(){
		NProgress.start();
	 	NProgress.inc()
		// NProgress.done();
	})
	
	$('#profile_edit_form').on('submit', function(event){
		event.preventDefault()
		console.log($('#date_of_birth').val())
		if($('#status').val() != ''){
			$('#status_here').text($('#status').val())
		}else{
			$('#status_here').text('Say something... Update your status')
		}

		var allClear = validateForm();
		console.log(allClear)
		if(!allClear){
			return false;
		}
		
		

		if(!appended){

			var formData = {
			full_name: $('#full_name').val(), date_of_birth: $('#date_of_birth').val(), 
			gender: $('#gender').val() == 0 ? "Unspecified" : $('#gender').val() == 1? "Male" : "Female" ,
			country: $('#country').val(), city: $('#city').val(), state: $('#state').val(), street: $('#street').val(), work: $('#work').val(),
			position: $('#position').val(), zip: $('#zip').val()
		}
		console.log(formData)
			$('#profile_preview').empty()
			$( "#profileTemplate" ).tmpl( formData ).appendTo( "#profile_preview")
			appended = true

			$('#preview_button').html('<i class="fa fa-arrow-right"></i> Save Changes')
			
			$('#change_button').show()

			$('#profile_edit_form input').not('input[type="file"]').attr('disabled','disabled')
			$('#gender').attr('disabled','disabled')

			$('html, body').animate({
			        scrollTop: $("#profile_preview").offset().top
			    }, 1000);

			event.preventDefault()

		}else{
			var warning_message = []
			// console.log('redirecting to homepage')
			var url = $('#domain').attr('href') + '/customer/create/edit'
			// alert(url)
			var details = {
			full_name: $('#full_name').val(), date_of_birth: $('#date_of_birth').val(), 
			gender: $('#gender').val() == 0 ? "Unspecified" : $('#gender').val() == 1? "Male" : "Female" ,
			country: $('#country').val(), city: $('#city').val(), state: $('#state').val(), street: $('#street').val(), work: $('#work').val(),
			position: $('#position').val(), zip: $('#zip').val(), _token: $('input[name="_token"]').val(),
			status: $('#status').val(), about_work: $('#about_work').val(), start_date: $('#start_date').val()
		}
			$('#preview_button').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading...')
			// alert(details.status)
			$.post( url,details, function(result) {
				console.log(result)
				
				if($('input[name="edit"]').val() == 1){
					console.log(domain+'/profile/' +$('input[name="user_id"]').val())
					window.location = domain + '/profile/' + $('input[name="user_id"]').val()
				}else{
					window.location = domain + '/customer/home';
					console.log(domain+'/customer/home')
				}
			})
			event.preventDefault()
		}
		
	})

	$('#change_button').on('click',function(event){
		event.preventDefault()
		console.log('Refreshing page')
		$('#profile_edit_form input').not('input[type="file"]').removeAttr('disabled')
		$('#gender').removeAttr('disabled')
		$('#preview_button').html('<i class="fa fa-eye"></i> Preview')
		appended = false
		// $('#profile_preview').empty()
		$(this).hide()
		$('html, body').animate({
			        scrollTop: $("#blog-full-width").offset().top
			    }, 1000);
	})

	function readURL(input,previewLocation) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $(previewLocation).fadeOut(500,function(){
	      		$(this).attr('src', e.target.result).fadeIn(500);
	      });

	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}


	$("#profile_picture").change(function() {
	  readURL(this,'#profile_picture_here');
	});

	$("#cover_picture").change(function() {
	  readURL(this,'#cover_picture_here');
	});

	function warn(input){
		swal({
		  title: 'Won\'t you provide a status message?',
		  text: "",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#00BFFF',
		  cancelButtonColor: '#E74C3C',
		  confirmButtonText: 'Don\'t worry, continue'
		}).then(function () {
		  
		})
	}

})