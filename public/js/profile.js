$(document).ready(function(){

	var friendshiplink = $('#friendship').attr('href')

	$('#delete-account').on('click',function(event){
		event.preventDefault()
		swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#00BFFF',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {
			var url = $('#domain').val() + '/account/delete'
			$.get( url, function(result) {
				if(result){
					swal(
					    'Delete Successful',
					    'Your Account has been deleted. Good bye',
					    'success'
					  )
				}
				window.setTimeout(function(){
					window.location = $('#domain').val()
				},3000)
			})
		})
	})

	$('#commentForm').submit(function(event){
		event.preventDefault()

		if($('#textbody').val() == ""){
			return false
		}
		console.log($(this).serialize())
		// var url = domain + '/message'
		// $.get( url,$(this).serialize(), function(result) {
		// 	console.log(result)
		// 	var current_message = [{message: result, time: new Date(new Date().getTime()).toLocaleTimeString()}]
		// 	$( "#sendTemplate" ).tmpl( current_message ).appendTo( "#chat" )
		// 	$('#chatModal, #chat').animate({
		// 	        scrollTop: $("#last").offset().top
		// 	    }, 1000);

		// })

		$('#textbody').val('').focus()

	})

	$('#chatForm').submit(function(event){
		event.preventDefault()

		if($('#tbody').val() == ""){
			return false
		}
		
		console.log($(this).attr('action'))
		
		$.get( $(this).attr('action'),$(this).serialize(), function(result) {
			console.log(result)
			
			var current_message = [{style: 'btrr', message: result.body, time: {date: new Date(new Date().getTime()).toLocaleTimeString()}}]
			$( "#sendTemplate" ).tmpl( current_message ).appendTo( "#chat" )
			// $('#chatModal, #chat').animate({
			//         scrollTop: $("#last").offset().top
			//     }, 500);

		})

		$('#tbody').val('').focus()

	})

	$('#mainBody').on('click','.comment',function(event){
		event.preventDefault()

		var url = $(this).attr('href')

		// $.get( url, function(result) {
			// console.log(result)
			// $( "#commentTemplate" ).tmpl( result ).appendTo( ".comments_here" )
			$('#commentModal').modal('show')
		// })
	})


	$('#mainBody').on('click','.unfollow',function(event){
		event.preventDefault()
		var link = $(this).attr('href');
		var followLink = link.replace("/stop",'')
		console.log(link)

		//making ajax request to unfollow the clicked user
		$.get( link,function(result) {
			console.log(result)
			var allLinks = $('a[href="'+link+'"]')

			allLinks.each(function(){
				$(this).attr("href",followLink)
				$(this).fadeOut(200,function(event){
					$(this).text('Follow').fadeIn(200)
				}).addClass('follow').removeClass('unfollow')
			})

			var friendship = [{iden: 'profile_unfollow', clas: 'follow', followORnot: 'Follow', yesORno: 'No', icon: 'times',follow: followLink}]
			$('#whetherfollowing').html("")
			$( "#followTemplate" ).tmpl( friendship ).appendTo( "#whetherfollowing")
		})		
	})

	$('#mainBody').on('click','.follow',function(event){
		event.preventDefault()
		var link = $(this).attr('href');
		var unfollowLink = link + "/stop"
		console.log(link)
		//making request to follow user
		$.get( link,function(result) {
			console.log(result)
			var allLinks = $('a[href="'+link+'"]')

			allLinks.each(function(){
				$(this).attr("href",unfollowLink)
				$(this).fadeOut(200,function(event){
					$(this).text('Unfollow').fadeIn(200)
				}).addClass('unfollow').removeClass('follow')
			})

			var friendship = [{iden: 'profile_follow', clas: 'unfollow', followORnot: 'Unollow', yesORno: 'Yes', icon: 'plus',follow: unfollowLink}]
			$('#whetherfollowing').html("")
			$( "#followTemplate" ).tmpl( friendship ).appendTo( "#whetherfollowing")
		})
		
	})

	$('#mainBody').on('click','.votedown',function(event){
		event.preventDefault()
		var element = $(this)
		console.log(element.attr('href'))
		var link = element.attr('href').replace("/votedown",'')
		console.log('new href: '+link)
		$.get( element.attr('href'),function(result) {
			console.log(result)
			element.html('VoteUp <i class="fa fa-level-up"></i>').removeClass('votedown').addClass('voteup').attr('href',link);
			console.log(result.votes)
			element.parent().find('.post_votes').text(result.votes);
		})
	})

	$('#mainBody').on('click','.voteup',function(event){
		event.preventDefault()
		var element = $(this)
		var link = element.attr('href') + "/votedown"
		console.log(element.attr('href'))
		console.log('new href: '+link)
		$.get( element.attr('href'),function(result) {
			console.log(result)
			element.html('VoteDown <i class="fa fa-level-down"></i>').removeClass('voteup').addClass('votedown').attr('href',link);
			element.parent().find('.post_votes').text(result.votes);
		})
	})

	$('#mainBody').on('click','.deletePost',function(event){
		event.preventDefault()
		var element = $(this);
		console.log(element.attr('href'))
		var parentArticle = element.parent().parent().parent().parent();

		$.get( element.attr('href'),function(result) {
			console.log(result)
			parentArticle.slideUp(1000,function(){
				parentArticle.remove()
			})
		})
	})

	$('.dropdown-menu').on('click','a:not(.not)',function(event){
		NProgress.start();
     	NProgress.inc()
		event.preventDefault()
		var element = $(this)
		console.log(element.attr('href'))
		$.get( element.attr('href'),function(result) {
			// console.log(result)
			// $('#somenews').html(result)
			$('#posts').fadeOut(500,function(event){
				$(this).html(result).fadeIn(500)
			})
			NProgress.done()
		})
	})

	$('#mainBody').on('click','.mes',function(event){
		event.preventDefault()
		console.log($(this).attr('href'))
		console.log($('.formHolder').attr('class'))
		// $('.formHolder').append('')
		// $( "#onlineTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo(".formHolder")
		var nextP = $(this).next('p');
		console.log(nextP.text())
		$('.formHolder.online.form-group').not(nextP).hide()
		$('.formHolder.offline.form-group').not(nextP).hide()
		nextP.html(`<form action=`+ $(this).attr('href') +`>
			<textarea class="form-control" placeholder="Type a reply"></textarea>
			<button type="button" class="btn modblue chat"><i class="fa fa-paper-plane-o"></i></button>
			</form>`).show()
		// nextP.find('form').attr('action')
	})

	$('#mainBody').on('click','#clearNotification',function(event){
		event.preventDefault()
		console.log('all or Selected')
		$(this).parent().append(`: 
			<a href="#all" id="all">All</a> OR <a href="#selected" id="selected">Selected</a>
			`)
	})

	$('#mainBody').on('click','#all',function(event){
		event.preventDefault()
		console.log("deleting all notifications")
		$.get($('#domain').val() + "/customer/notifications/clear/all",function(result){
			console.log(result)
			$('#notifications').slideUp(500,function(event){
				$(this).html("")
				$('.fa.fa-long-arrow-right').text("No notification")
			})
		})
	
	})

	$('#mainBody').on('click','#selected',function(event){
		event.preventDefault()
		var element = $(this)
		$.get( $('#domain').val() + "/customer/profile/mark-notif",function(result) {
			$('#posts').html(result)
			console.log("displaying a form to mark you selected notifications")
		})
	})

	$('#mainBody').on('click','#clearSelected',function(event){
		event.preventDefault()
		var formData = $('#mark_form').serialize()
		console.log("initial form data: "+formData)
		console.log("submiting marked content")
		$.get( $('#mark_form').attr('action'), formData, function(result) {
			console.log(result)
			$('#totrigger').trigger('click')
		})
		// console.log("submiting marked content")
	})

	$(document).on('click','#chatFriend',function(event){
		event.preventDefault()
		console.log($(this).attr('href'))
		//getting chats between these two users
		$.get($(this).attr('href'), function(result){
			console.log(result)
			$( "#sendTemplate" ).tmpl( result ).appendTo( "#chat" )
			// $('#chatModal, #chat').animate({
			//         scrollTop: $("#last").offset().top
			//     }, 500);
		})
		$('#chatModal').modal('show')
	})

	$('.close').click(function(event){
		$('.modal-body').empty()
	})
})