function validateInput(element,rule) {
	// alert('im working! hurray')
	// console.log(element.parent().find('span').text())
	    var input = element;
	    var spantext = input.parent().find('span').text().split(',')[0];
	    var condition = false;
	    var message = "";

	    switch(rule){
	    	case 'length':
		    	var argument = input.attr('data-validate-value').split(':');
		    	switch(argument[2]){
		    		case '0':
		    			condition = input.val().length < argument[0];
		    			message = 'minimum: '+ argument[0]+' character(s)';
		    		break;

		    		case '1':
		    			condition = input.val().length > argument[1];
		    			message =  + 'maximum: '+argument[1]+' characters';
		    		break;

		    		default:
		    			condition = input.val().length > argument[1] || input.val().length < argument[0];
		    			message = 'minimum: '+ argument[0]+' maximum: '+argument[1]+' characters';
		    	}
	    	break;

	    	case 'required':

	    		message = 'is required';
	    		var argument = input.attr('data-validate-value').split(':');
	    		switch(argument[0]){
	    			case 'alphanum_':
	    				condition = !isValid(input.val()) || input.val() == '';
	    				// console.log(condition)
	    				message += ' and must only be alphanumeric and underscore';
	    			break;

	    			case 'alphanum':
	    				condition = !(/^[a-zA-Z0-9]/.test( input.val() )) || input.val() == ''
	    				message += ' and must only be alphanumeric'
	    			break;

	    			case 'alpha':
	    				condition = !(/^[a-zA-Z]/.test(input.val())) || input.val() == ''
	    				message += ' and must be only alphabets';
	    			break;

	    			case 'num':
	    				condition = isNaN(input.val()) || input.val() == '';
	    				message += ' and must only be numbers';
	    			break;
	    			case 'date':
	    				condition = input.val() == '' || InvalidDate(input.val());
	    				message += ' and must be of the format dd-mm-yyy'

			    }
	    	break;
	    	case 'alphanum_':
				condition = !isValid(input.val());
				// console.log(condition)
				message += 'must only be alphanumeric and underscore';
			break;

			case 'alphanum':
				condition = !(/^[a-zA-Z0-9]/.test( input.val() ))
				message += 'must only be alphanumeric'
			break;

			case 'alpha':
				condition = !(/^[a-zA-Z]/.test(input.val()))
				message += 'must be only alphabets';
			break;

			case 'num':
				condition = isNaN(input.val())
				message += 'must only be numbers';
			break;
			case 'text':
				condition = !(/^[a-z\d\-_\s]+$/i.test(input.val())) && input.val() != '';
				// alert(condition)
				message = 'must contain alphanumeric and - or space';
			break;
	
	    }

	    if(condition){
	    	flagErrorAndGetSpan(input).html(spantext+',<em class="message"> '+message+'</em>')
	    	return false;
	    }else{
	    	input.parent().removeClass('has-error').find('span').text(spantext)
	    	return true;
	    }

	   
	}

	function flagErrorAndGetSpan(input){
		return input.parent().addClass('has-error').find('span');
	}

	function isValid(str) { return /^\w*$/.test(str); }

	function validateForm(){
		var condition = true;

		$('input[data-validate="length"]').each(function(){
			var a = validateInput($(this),'length');
			condition = condition && a;
		})

		$('input[data-validate="required"]').each(function(){
			var a = validateInput($(this),'required');
			condition = condition && a;
		})

		$('input[data-validate="alphanum_"]').each(function(){
			var a = validateInput($(this),'alphanum_');
			condition = condition && a;
		})

		$('input[data-validate="alphanum"]').each(function(){
			var a = validateInput($(this),'alphanum');
			condition = condition && a;
		})

		$('input[data-validate="num"]').each(function(){
			var a = validateInput($(this),'num');
			condition = condition && a;
		})

		$('input[data-validate="alpha"]').each(function(){
			var a = validateInput($(this),'alpha');
			condition = condition && a;
		})

		$('input[data-validate="text"]').each(function(){
			var a = validateInput($(this),'text');
			condition = condition && a;
		})
		// console.log(condition);
		return condition;
	}

	function InvalidDate(date){
		var data = date.split('-');
		if(isNaN(data[0]) || isNaN(data[1]) || isNaN(data[2])) return true;
		return false;
	}