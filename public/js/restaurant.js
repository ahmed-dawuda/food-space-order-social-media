$(document).ready(function(){

	var domain = $('#domain').attr('href')

	$('#menu a').click(function(event){
		event.preventDefault()
		$('#price').empty()
		var id = $(this).attr('href')
		url = domain + '/order/' + id
		// alert(url)
		$.get( url, function(result) {
			  	console.log(result)
			  	$('#orderForm')[0].reset()
			  	$('#name').val(result.food.name)
			  	$('#type').val(result.food.period)
			  	$('#food_id').val(result.food.id)
			  	var time = new Date(new Date().getTime()).toLocaleTimeString()
			  	// console.log(time)
			  	$('#current_time').val(time)
			  	// alert(result.prices.length)
			  	console.log(result.prices)
			  	for(var i = 0; i < result.prices.length; i++){
			  		$('#price').append('<option value="'+result.prices[i] +'">GHS '+ result.prices[i] +'</option>')
			  	}
			  	
			  	$('#orderModal').modal('show')
			});
	})

	$('#orderForm').submit(function(event){
		event.preventDefault()

		if($('#quantity').val() == ""){
			alert('Please quantity is required to be a number')
			return false
		}

		if($('#delivery_time').val() == ""){
			alert('Please Enter hours for delivery')
			return false
		}

		// console.log('Placing order')
		var url = domain + '/order'
		$.post( url, $('#orderForm').serialize() , function(result) {
			  	console.log(result)
			  	$('#orderModal').modal('hide')
			});
		$('#price').empty()
	})

	$('#chatForm').submit(function(event){
		event.preventDefault()

		if($('#textbody').val() == ""){
			return false
		}

		var url = domain + '/message'
		$.get( url,$(this).serialize(), function(result) {
			console.log(result)
			var current_message = [{message: result, time: new Date(new Date().getTime()).toLocaleTimeString()}]
			// var time = new Date(new Date().getTime()).toLocaleTimeString()
			$( "#sendTemplate" ).tmpl( current_message ).appendTo( "#chat" )
			$('#chatModal, #chat').animate({
			        scrollTop: $("#last").offset().top
			    }, 1000);

		})
		$('#textbody').val('').focus()

	})

	$('#contactForm').submit(function(event){
		event.preventDefault()
		var url = $(this).attr('action')
		console.log(url)
		$('#submit').val('Sending Suggestion...')
		$.post( url,$(this).serialize(), function(result) {
			console.log(result)
			$('#contactForm')[0].reset()
			$('#submit').val('Suggest')
		})
	})

	$('#follow').click(function(event){
		event.preventDefault()
		var url = domain + '/follow/restaurant/' + $('#rest_id').val()
		console.log("following "+url)
		$.get( url, function(result) {
			console.log(result)
			// $('#thumbsup').fadeOut(2000)
		})
	})

	$('#tag').on('click',function(event){
		console.log('click')

		

		$('#tagModal').modal('show')
	})

	$('#multiselectForm').submit(function(event){
		event.preventDefault()
	})

})