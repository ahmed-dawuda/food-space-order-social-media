$(document).ready(function(event){
	// $('#add_food_form').hide()
	function readURL(input,previewLocation) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $(previewLocation).fadeOut(500,function(){
	      		$(this).attr('src', e.target.result).fadeIn(500);
	      });

	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#logo").change(function() {
	  readURL(this,'#logo_here');
	});

	$(document).on('click','#add_more',function(event){
		$('#add_food_form').show();
		$(this).html('<i class="ti-close"></i> Cancel').attr('id','cancel')
	})

	$(document).on('click','#cancel',function(event){
		$('#add_food_form').hide()
		$(this).html('<i class="ti-plus"></i> Add more').attr('id','add_more')
	})

	$(document).on('submit','#add_food_form',function(event){
		event.preventDefault()
		// console.log($(this).serialize())
		$(this).find('#submit').html('Adding food <i class="fa fa-spinner fa-spin"></i>')
		var form = $(this)
		$.get($(this).attr('action'),$(this).serialize(),function(result){
			console.log(result)
			if(result.success){
				console.log("success")
				$.notify({
	                icon: 'ti-check',
	                message: " Food added successfuly"

	            },{
	                type: 'success',
	                timer: 1000
	            });
				form[0].reset()
			}else{
				$.notify({
	                icon: 'ti-info',
	                message: " You've left some field empty."

	            },{
	                type: 'danger',
	                timer: 5000
	            });
			}
			
			form.find('#submit').html('<i class="fa fa-plus"></i> Add food')
			
		})
	})
})