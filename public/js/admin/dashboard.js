$(document).ready(function(event){

	$(document).on('click','#close',function(event){
                event.preventDefault()
                var element = $(this);
                var url = $(this).attr('href')
                console.log($(this).attr('href'))
                $.get(url,function(result){
                    console.log(result)
                    element.attr('id','open');
                    element.html('<i class="ti-alarm-clock"></i> Currently Closed - Open Work');
                    element.attr('href',url.replace("/close",''))
                    $.notify({
                        icon: 'ti-info',
                        message: " You're closed. People can not view your site until you're opened, Bye..All the best."

                    },{
                        type: 'warning',
                        timer: 1000
                    });
                        })
                
            })

            $(document).on('click','#open',function(event){
                event.preventDefault()
                var element = $(this);
                var url = $(this).attr('href')
                console.log($(this).attr('href'))
                $.get(url,function(result){
                    console.log(result)
                    element.attr('id','close');
                    element.html('<i class="ti-alarm-clock" style="color:green;"></i> Currently Opened - Close Work');
                    element.attr('href',url + "/close");
                    $.notify({
                        icon: 'ti-check',
                        message: " You're opened. People can view your site and place order(s) until you're closed, Good Luck."

                    },{
                        type: 'success',
                        timer: 1000
                    });
                });
                
            })

            $(document).on('click','.dash',function(event){
                event.preventDefault()
                var modal = $('#dashboardModal')
                console.log($(this).attr('href'))
                modal.find('h4').html($(this).html()).append(' - More')
                modal.find('#proceed').attr('href',$(this).attr('href').replace('/1',"/2"))
                modal.modal('show')
            })

       $(document).on('click','#proceed',function(event){
       		event.preventDefault()
       		// alert('click')
       		console.log($(this).attr('href'))
       })

       $(document).on('click','.sidebar-wrapper a',function(event){
            NProgress.start();
       })
})