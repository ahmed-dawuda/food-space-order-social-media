$(document).ready(function(event) {
 
	function readURL(input,previewLocation) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $(previewLocation).fadeOut(500,function(){
	      		$(this).attr('src', e.target.result).fadeIn(500);
	      });

	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#logo").change(function() {
	  readURL(this,'#logo_here');
	});

	$("#cover").change(function() {
	  readURL(this,'#cover_here');
	});

	$("#chef1").change(function() {
	  readURL(this,'#chef1_here');
	});

	$("#chef2").change(function() {
	  readURL(this,'#chef2_here');
	});

	$("#chef3").change(function() {
	  readURL(this,'#chef3_here');
	});

	$('#mainBody').on('submit','#main_form',function(event){
		// event.preventDefault()

		// if(!validateForm()){
		// 	return false;
		// }
		// var chef_formData = $('#chef_form').serialize();
		// console.log($('#chef_form').attr('action'))
		// $.post($('#chef_form').attr('action'),chef_formData,function(result){
		// 	console.log(result)
		// })
		// console.log($('#chef_form,#main_form').serialize());
		$('input[name="chef_1"]').val($('input[name="chef1_name"]').val())
		$('input[name="chef_2"]').val($('input[name="chef2_name"]').val())
		$('input[name="chef_3"]').val($('input[name="chef3_name"]').val())
		$('#saveInfo').html(`Saving info <i class="fa fa-spinner fa-spin"></i>`)
	})

	var ex = 0;
	$('#mainBody').on('click','#except',function(event){
		event.preventDefault()
		ex++;
		$('#open_hours').append(`
				<div class="row">
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label>Specify day</label>
	                    <input type="text" class="form-control border-input" placeholder=".e.g Monday" name="ex`+ex+`" data-validate="required" data-validate-value="any">
	                </div>
	            </div>
	            <div class="col-md-3">
	                <div class="form-group">
	                    <label>Start</label>
	                    <input type="time" class="form-control border-input" name="start`+ex+`" data-validate="required" data-validate-value="any">
	                </div>
	            </div>
	            <div class="col-md-3">
	                <div class="form-group">
	                    <label>End</label>
	                    <input type="time" class="form-control border-input" name="end`+ex+`" data-validate="required" data-validate-value="any">
	                </div>
	            </div>
	            <div class="col-md-2">
	           	<a class="btn btn-info" href="#" id="remove"><i class="ti-close"></i></a>
	            </div>
	        </div>
			`)
	})

	$('#mainBody').on('click','#remove',function(event){
		event.preventDefault()
		$(this).parent().parent().remove()
	})

	$('#mainBody').on('click','#editInfo',function(event){
		console.log("editInfo")
		$('input[disabled="disabled"]').removeAttr('disabled');
		$('textarea').removeAttr('disabled')
		$('#saveInfo').removeAttr('disabled')
		$('#except').removeAttr('disabled')
		$(this).attr('disabled','disabled')
	})

	function validateForm(){
		var returnvalue = true;
		$('input[data-validate="required"]').each(function(event){
			var rule = $(this).attr('data-validate-value')
			var message = " is required";
			var condition = false;

			switch(rule){
				case 'name':
				condition = $(this).val() == '' || !(/^\w*$/.test($(this).val()))
				message += 'and must be alphanum and underscores only'
				break;
				default:
				condition = $(this).val() == ''

			}
			if(condition){
				var det = showError($(this),message)
				returnvalue = returnvalue && det
			}
			else{
				var det = removeError($(this))
				returnvalue = returnvalue && det
			}
		})

		return returnvalue;
	}

	function showError(input,message){
		var text = input.parent().find('label').text().split(':')
		input.parent().addClass('has-error').find('label').text(text[0] + ': '+message)
		return false;
	}

	function removeError(input){
		var text = input.parent().find('label').text();
		var input_name = text.split(':');
		input.parent().removeClass('has-error').find('label').text(input_name[0])
		return true;
	}
});