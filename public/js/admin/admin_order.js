$(document).ready(function(event){

	$(document).on('click','.view',function(event){
		event.preventDefault()
		NProgress.start();
		// console.log($(this).attr('href'))
		// NProgress.inc()
		$.get($(this).attr('href'),function(result){
			console.log(result)
			var modal = $('#orderModal');
			modal.find('#order_id').text(result.orderID)
			modal.find('#order_food').text(result.food_name)
			modal.find('#order_del_time').text(result.delivery_time)
			modal.find('#handle').text(result.handle)
			modal.find('#order_quantity').text(result.quantity)
			modal.find('#order_price').text(result.price)
			modal.find('#order_at').text(result.ordered_at)
			// modal.find('#').text(result)
			modal.find('.user_name').each(function(){
				$(this).text(result.user_name)
			})
			modal.find('#country').html('Country - <b>'+result.user_country+'</b>')
			modal.find('#state').html('State - <b>'+result.user_state+'</b>')
			modal.find('#city').html('City - <b>'+result.user_city+'</b>')
			modal.find('#street').html('Street - <b>'+result.user_street+'</b>')
			modal.find('#current_loc').html(result.order_destination)
			modal.modal('show')
			NProgress.done();
		})
		
	})

	$(document).on('click','.dropdown-menu a',function(event){
		event.preventDefault()
		// console.log($(this).parent().parent().parent().find('.btn.btn-info.delete').attr('class'))
		// return false;
		console.log($(this).text())
		//make $.get() request to the server
		var reason = $(this).text()
		$.notify({
                icon: 'ti-info',
                message: " You rejected order with ID: 42345 with the reason being that: "+reason

            },{
                type: 'warning',
                timer: 1000
            });

		// console.log($(this).parent().parent().parent().parent().find('.status').text())
		var parentRow = $(this).parent().parent().parent().parent()
		$(this).parent().parent().parent().find('.btn.btn-info.delete').removeAttr('disabled')
		parentRow.find('.status')
				.removeClass('text-warning').removeClass('text-success')
				.addClass('text-danger').html('Canceled <i class="fa fa-pause"></i>')
				.attr('title',reason)

		parentRow.find('.delivered').attr('disabled','disabled')
		parentRow.find('.reject').attr('disabled','disabled')
		// parentRow.remove()
		// console.log(parentRow.html())
		var rowContent = parentRow.html()
		parentRow.parent().append('<tr>'+ rowContent +'</tr>')
		parentRow.remove()

	})

	$(document).on('click','.delete',function(event){
		event.preventDefault()
		// var orerID = $(this).parent().parent().find('.fa-eye')
		$(this).parent().parent().fadeOut()
		$.notify({
                icon: 'ti-check',
                message: " Order delete successful"

            },{
                type: 'success',
                timer: 1000
            });
	})

	$(document).on('change','#sort',function(event){
		console.log($(this).val())
		var sort = $('#show_only').val().split("/").pop();
		console.log($(this).val()+"/"+sort)
		$.get($(this).val()+"/"+sort, function(result){
			$('#orders').find('tbody').html(result)
		})
		// $('#orders').find('tbody').html()
	})

	$(document).on('change','#show_only',function(event){
		// alert($('#sort').val().split("/").pop())
		var sort = $('#sort').val().split("/").pop();
		console.log($(this).val())
		console.log($(this).val()+"/"+sort)
		// if($(this).val() == 0) return false;
		$.get($(this).val()+"/"+sort, function(result){
			$('#orders').find('tbody').html(result)
		})
		
	})

	$(document).on('click','.delivered', function(event){
		event.preventDefault();
		console.log('delivered')
		$(this).parent().parent()
				.find('.status')
				.removeClass('text-warning')
				.addClass('text-success').html('Delivered <i class="fa fa-check"></i>')
		$(this).attr('disabled','disabled')
		$(this).parent().find('.reject').attr('disabled','disabled')
		$.notify({
                icon: 'ti-check',
                message: " Order has been delivered"

            },{
                type: 'success',
                timer: 1000
            });
	})

})