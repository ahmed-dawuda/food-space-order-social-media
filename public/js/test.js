$(document).ready(function(){

	// $('#logout').click(function(event){
	// 	event.preventDefault()
	// 	$("#text_input").cleditor();
	// })

	var orders = [
		{ color: 'red', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'green', res_id: "ID: 2345, Name: Brunei Cafe", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'red', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'green', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'red', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'red', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" },
		{ color: 'green', res_id: "ID: 2345, Name: Maa U", ord_id: "2345", ord_type: "Breakfast", ord_time: "Jul 25, 2012 11:09", del_time: "Jul 25, 2012 11:09", status: "Delivered" }
	
		];

	var favs = [
	{id: "5", name: "Maa U", ord_num: "34", success: "9", status: "Food Available"},
	{id: "2", name: "Brunei Cafe", ord_num: "14", success: "12", status: "Food Unavailable"},
	{id: "1", name: "High Meal", ord_num: "3", success: "3", status: "Food Available"}
	];

	$('#test-links').on('click','a:not(#posts)',function(event){

		$('.dashboard-list').html("")
		NProgress.start();
	 NProgress.inc()
	
		var link = $(this).attr('id')
		// alert(link)
		var header = ''
		switch(link){
			case 'fav': 
				header = "Favourite Restaurant"
				break;
			case 'friends': 
				header = "Friends"
				break;
			case 'top':
				header = "Top Restaurants"
				break;
			case 'orders':
				header = "Orders"
				break;
			case 'suggestions':
				header = "Choose Restaurant to Send Suggestion"
				break;
			case 'trends':
				header = "Trends"
				break;
			case 'posts':
				header = "Post Something"
				break;
			case 'messages':
				header = "Messages and Chats"
				break;
			case 'notifications':
				header = 'View New Notifications'
				break;
			case 'faq':
				header = "Frequently Asked Questions"
				break;
			case 'foods':
				header = "My Favourite Foods"
				break;
			default:
				header = "About US"

		}
		
		if(link != 'search' && link != 'timeline' && link != 'logout'){
			
			event.preventDefault()
			var testModal = $('#testModal')
			testModal.find('h2').text(header)
			testModal.find('.modal-footer').html(`
				<button type="button" class="btn btn-default" data-dismiss="modal" id="buttonColor">Close</button>
				`)
			var url = $('#searchForm').attr('action') + '/tmplt/' + link
			// var data = '';
			$.get( url, function( result ) {
				console.log(result)
			  	appendData(link,result)
			  	testModal.modal('show')
			});
			// console.log(data)			
		}
		NProgress.done();
	})

	function appendData(link,data){
		switch(link){
			case 'fav': 
				removeOtherClasses('yellow');
				$('#headerColor').addClass('yellow');
				$('#buttonColor').addClass('yellow');
				$( "#favTemplate" ).tmpl( data ).appendTo( ".dashboard-list" );
				break;
			case 'friends': 
				removeOtherClasses('yellow');
				$('#headerColor').addClass('yellow');
				$('#buttonColor').addClass('yellow');
				$( "#friendTemplate" ).tmpl( data ).appendTo( ".dashboard-list" );
				break;
			case 'top':
				removeOtherClasses('red');
				$('#headerColor').addClass('red');
				$('#buttonColor').addClass('red');
				$( "#topTemplate" ).tmpl( data ).appendTo( ".dashboard-list" );
				break;
			case 'orders':
				removeOtherClasses('blue');
				$('#headerColor').addClass('blue');
				$('#buttonColor').addClass('blue');
				$( "#orderTemplate" ).tmpl( orders ).appendTo( ".dashboard-list" );
				break;
			case 'suggestions':
				removeOtherClasses('pink');
				$('#headerColor').addClass('pink');
				$('#buttonColor').addClass('pink');
				$( "#suggestionTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo( ".dashboard-list" );
				break;
			case 'trends':
				removeOtherClasses('green');
				$('#headerColor').addClass('green');
				$('#buttonColor').addClass('green');
				$( "#trendTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo( ".dashboard-list" );
				break;
			// case 'posts':
			// 	$( "#postTemplate" ).tmpl( orders ).appendTo( ".dashboard-list" );
			// 	break;
			case 'messages':
				removeOtherClasses('green');
				$('.dashboard-list').addClass('metro');
				$('#headerColor').addClass('green');
				$('#buttonColor').addClass('green');
				$( "#messageTemplate" ).tmpl( data ).appendTo( ".dashboard-list" );
				break;
			case 'notifications':
				removeOtherClasses('pink');
				$('#headerColor').addClass('pink');
				$('#buttonColor').addClass('pink');
				$( "#notificationTemplate" ).tmpl( data ).appendTo( ".dashboard-list" );
				break;
			case 'faq':
				removeOtherClasses('yellow');
				$('#headerColor').addClass('yellow');
				$('#buttonColor').addClass('yellow');
				$( "#faqTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo( ".dashboard-list" );
				break;
			case 'foods':
				removeOtherClasses('green');
				$('#headerColor').addClass('green');
				$('#buttonColor').addClass('green');
				$( "#foodTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo( ".dashboard-list" );
				break;
			case 'posts':
				removeOtherClasses('blue');
				$('#headerColor').addClass('blue');
				$('#buttonColor').addClass('blue');
				$( "#postTemplate" ).tmpl( [{name: "ahmed"}] ).appendTo( ".dashboard-list" );
				break;
			default:
				removeOtherClasses('pink');
				$('#headerColor').addClass('pink');
				$('#buttonColor').addClass('pink');
				$( "#aboutTemplate" ).tmpl( [{name:"ahmed"}] ).appendTo( ".dashboard-list" );

		}
	}

	function removeOtherClasses(desiredclass){
		switch(desiredclass){
			case 'pink':
				$('#headerColor').removeClass("yellow blue red green");
				$('#buttonColor').removeClass("yellow blue red green");
			break;
			case 'yellow':
				$('#headerColor').removeClass("pink green red blue");
				$('#buttonColor').removeClass("pink green red blue");
			break;
			case 'green':
				$('#headerColor').removeClass("pink yello red blue");
				$('#buttonColor').removeClass("pink yello red blue");
			break;
			case 'red':
				$('#headerColor').removeClass("pink green yello blue");
				$('#buttonColor').removeClass("pink green yello blue");
			break;
			case 'blue':
				$('#headerColor').removeClass("pink green red yellow");
				$('#buttonColor').removeClass("pink green red yellow");
			break;
		}
	}

	$('#modal_content').on('click','a.instantChat',function(event){
		event.preventDefault()
		var element = $(this)
		console.log(element.attr('href'))

		var modal = $('#testModal')

		$.get(element.attr('href'),function(result){
			console.log(result)
			modal.find('.modal-footer').html("").append(`
			<form method="get" action="`+result.submit_url+`" id="chatForm">
				<div class="chat-form">
					<textarea name="message" placeholder="Type message here..."></textarea>
					<input class="btn btn-info" type="submit" value="Send" id="sendButton">
					<button type="button" class="btn btn-default green" data-dismiss="modal" id="buttonColor">Close</button>
				</div>
			</form>	
			`)
			modal.find('ul').addClass('chat').html("")
			$( "#chatTemplate" ).tmpl( result.messages ).appendTo(modal.find('ul'));
			modal.find('h2').html('<a href="#" style="color:white;" class="backIcon"><i class="icon-backward"></i> '+result.chat_name+' <i class="icon-user"></i></a>')
		})
	})

	$('#modal_content').on("click","a.backIcon",function(event){
		event.preventDefault()
		console.log($('#chatForm').serialize())
		$(".dashboard-list.metro.chat").html("")

		$('#chatForm').remove()
		$('#testModal').find('ul').removeClass("chat")
		$('#testModal').find('h2').html('Messages and Chats')
		var url = $('#searchForm').attr('action') + '/tmplt/' + 'messages'
		$.get( url, function( result ) {
				console.log(result)
			  	appendData("messages",result)
				$('#testModal').find('.modal-footer').html('<button type="button" class="btn btn-default green" data-dismiss="modal" id="buttonColor">Close</button>')
			});
	})

	$(document).on('submit','#chatForm',function(event){
		event.preventDefault()
		console.log('sending the message')
		console.log($(this).find('textarea').val())
		$(this).find('textarea').val("")
	})

	$('#test-links').on('click','#posts',function(event){
		event.preventDefault()
		console.log('post modal')

		$("#text_input").cleditor({width: 540, heigth: 340});
		$('#postModal').modal('show')
	})

	function readURL(input,previewLocation) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $(previewLocation).fadeOut(500,function(){
	      		$(this).attr('src', e.target.result).fadeIn(500);
	      });

	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#file-input").change(function() {
	  readURL(this,'#postPhoto');
	  $("#postPhoto").parent().show()
	});

	$(document).on('submit','#post_form',function(event){
		event.preventDefault()
		// alert('submit')
		console.log($(this).serialize())
		$("#postPhoto").attr('src','').hide()
		$('#file-input').val('')
		// $('#postModal').modal('hide')
	})
})